package com.threadr.android.app;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import com.threadr.android.app.Utils.PreferenceHelper;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by lilandra on 2/13/14.
 */
public class ActivePollsFragment extends BaseFragment {

    private static String TAG = "ActivePollsFragment";

    private ListView lv_alerts;
    private ActivePollsListViewAdapter adapter;
    private List<PollInfo> alerts = new ArrayList<PollInfo>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View V = inflater.inflate(R.layout.active_polls_view, container, false);

        // setup alert list

        lv_alerts = (ListView) V.findViewById(R.id.lv_alerts);

        adapter = new ActivePollsListViewAdapter(main_tab_activity, alerts);

        lv_alerts.setAdapter(adapter);

        lv_alerts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View view, int pos, long id) {

                PollInfo selected_entry = alerts.get(pos);

                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.ALERT_ID, selected_entry.id);

                PollDetailFragment fragment = new PollDetailFragment();
                fragment.setArguments(bundle);

                main_tab_activity.pushFragments(AppConstants.TAB_POLLS, fragment, true, true);
            }
        });

        return V;
    }

    @Override
    public void onResume() {
        super.onResume();

        alerts.clear();

        new LoadData().execute();
    }

    private void loadData() {

        String access_token = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_ACCESS_TOKEN, "UNKNOWN");

        try {
            Map<String, Object> result = ThreadrAPIAdapter.doGetActiveAlertList(access_token);

            Object groups_data = result.get("data");

            for (Map<String, Object> alert : (ArrayList<Map<String, Object>>) groups_data) {

                PollInfo poll_info = new PollInfo();

                poll_info.id = alert.get("alertId").toString();
                poll_info.description = alert.get("description").toString();
                poll_info.post_date = alert.get("timestamp").toString();
                poll_info.expiration_date = alert.get("expirationDate").toString();
                poll_info.comments_count = Integer.parseInt(alert.get("commentsCount").toString());
                poll_info.location_name = alert.get("locationName").toString();
                poll_info.latitude = Double.parseDouble(alert.get("latitude").toString());
                poll_info.longitude = Double.parseDouble(alert.get("longitude").toString());

                Map<String, Object> user_info = (Map<String, Object>) alert.get("userInfo");
                poll_info.sender_user_id = user_info.get("userId").toString();
                poll_info.sender_first_name = user_info.get("firstName").toString();
                poll_info.sender_last_name = user_info.get("lastName").toString();
                poll_info.sender_image_url = user_info.get("profileImage").toString();

                Object contacts = alert.get("contacts");
                for (Map<String, Object> contact : (ArrayList<Map<String, Object>>) contacts) {
                    PollInfo.ContactInfo alert_contact = new PollInfo.ContactInfo();
                    alert_contact.first_name = contact.get("firstName").toString();
                    poll_info.contacts.add(alert_contact);
                }

                Object assets = alert.get("assets");
                for (Map<String, Object> asset : (ArrayList<Map<String, Object>>) assets) {
                    PollInfo.AssetInfo asset_info = new PollInfo.AssetInfo();
                    asset_info.id = asset.get("assetId").toString();
                    asset_info.url = asset.get("filename").toString();
                    asset_info.type = asset.get("type").toString();
                    asset_info.description = asset.get("choice").toString();
                    asset_info.count = Integer.parseInt(asset.get("count").toString());

                    poll_info.assets.add(asset_info);
                }

                Log.i(TAG, poll_info.description);
                SharedData.retrieved_polls.polls.add(poll_info);

                alerts.add(poll_info);
            }
        } catch (Exception e) {
            Log.i(TAG + " loadAlerts", e.toString());
        }
    }

    private void updateDisplay() {
        adapter.notifyDataSetChanged();
    }

    class LoadData extends AsyncTask<Void, Void, Void> {

        protected ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(main_tab_activity, "", "Loading data", true, false);
        }

        @Override
        protected Void doInBackground(Void... params) {
            ActivePollsFragment.this.loadData();
            return null;
        }

        @Override
        protected void onPostExecute(Void ret) {
            ActivePollsFragment.this.updateDisplay();
            progressDialog.dismiss();
        }
    }

    private boolean isExpired(String expiration_date) {
        boolean result = false;

        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));

            Date future_date = formatter.parse(expiration_date);
            Date now_date = new Date();

            long diffInMillies = future_date.getTime() - now_date.getTime();

            if (diffInMillies < 0)
                result = true;

        } catch (Exception e) {
        }

        return result;
    }
}