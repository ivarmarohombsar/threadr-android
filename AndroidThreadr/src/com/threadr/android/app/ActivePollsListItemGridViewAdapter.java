package com.threadr.android.app;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by lilandra on 2/13/14.
 */
public class ActivePollsListItemGridViewAdapter extends BaseAdapter {

    private final String TAG = "ActivePollsListItemGridViewAdapter";

    private final Context mContext;
    private final List<PollInfo.AssetInfo> mAlertAssets;
    private int pax_voted = 0;


    public ActivePollsListItemGridViewAdapter(Context c, List list, int pax_voted) {
        mContext = c;
        mAlertAssets = list;
        this.pax_voted = pax_voted;
    }

    @Override
    public int getCount() {
        return mAlertAssets.size();
    }

    @Override
    public Object getItem(int position) {
        return mAlertAssets.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View itemView, ViewGroup parent) {
        // reference to convertView
        View v = itemView;

        if (mAlertAssets != null && !mAlertAssets.isEmpty()) {

            // get the selected entry
            PollInfo.AssetInfo entry = mAlertAssets.get(position);

            Log.i(TAG, entry.url);
            Log.i(TAG, entry.type);

            if (v == null) {
                LayoutInflater inflater = LayoutInflater.from(mContext);

                if (entry.type.equals(AppConstants.ASSET_IMAGE) || entry.type.equals(AppConstants.ASSET_AUDIO))
                    v = inflater.inflate(R.layout.active_poll_list_asset_item_audio_or_image, parent, false);
                else if (entry.type.equals(AppConstants.ASSET_VIDEO))
                    v = inflater.inflate(R.layout.active_poll_list_asset_item_video, parent, false);
                else
                    v = inflater.inflate(R.layout.active_poll_list_asset_item_text, parent, false);
            }

            // set the pic
            if (!entry.type.equals(AppConstants.ASSET_TEXT)) {
                ImageView iv_asset = (ImageView) v.findViewById(R.id.iv_asset);

                if (entry.type.equals(AppConstants.ASSET_IMAGE))
                    Picasso.with(mContext).load(entry.url).fit().into(iv_asset);
                else if (entry.type.equals(AppConstants.ASSET_VIDEO)) {
                    int i = entry.url.lastIndexOf(".");
                    String remote_image = entry.url.substring(0, i) + "_thumbnail.jpg";
                    Picasso.with(mContext).load(remote_image).error(R.drawable.with_video).fit().into(iv_asset);
                } else if (entry.type.equals(AppConstants.ASSET_AUDIO))
                    Picasso.with(mContext).load(R.drawable.with_audio).fit().into(iv_asset);

                iv_asset.setClickable(false);
                iv_asset.setFocusable(false);
                iv_asset.setFocusableInTouchMode(false);
            } else {
                TextView tv_asset = (TextView) v.findViewById(R.id.tv_asset);
                tv_asset.setText(entry.description);
            }

            // set the percentage

            TextView tv_percentage = (TextView) v.findViewById(R.id.tv_percentage);

            double count_percentage = 0.0;
            if (entry.count != 0)
                count_percentage = (entry.count / (pax_voted * 1.0)) * 100;
            String str_count_percentage = String.format("%.2f", count_percentage) + "%";
            tv_percentage.setText(str_count_percentage);

        }

        v.setClickable(false);
        v.setFocusable(false);
        v.setFocusableInTouchMode(false);

        return v;
    }
}

