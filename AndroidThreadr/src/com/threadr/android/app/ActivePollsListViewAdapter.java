package com.threadr.android.app;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.ArcShape;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import com.threadr.android.app.Extensions.NonScrollableGridView;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by lilandra on 1/18/14.
 */
public class ActivePollsListViewAdapter extends BaseAdapter {

    private String TAG = "ActivePollsListViewAdapter";

    private final Context mContext;
    private final List mListAlertInfo;

    public ActivePollsListViewAdapter(Context c, List list) {
        mContext = c;
        mListAlertInfo = list;
    }

    @Override
    public int getCount() {
        return mListAlertInfo.size();
    }

    @Override
    public Object getItem(int position) {
        return mListAlertInfo.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    static class ViewHolder {
        public ImageView iv_sender;
        public TextView tv_sender_name;
        public TextView tv_description;
        public TextView tv_posting_date;
        public TextView tv_location_name;
        public TextView tv_short_remaining_time;
        public TextView tv_comments;
        public TextView tv_recipients;
        public NonScrollableGridView gv_assets;

        public ImageView arcImageView;
        public ImageView grayArcImageView;
        public ImageView innerWhiteImageView;
    }

    @Override
    public View getView(int position, View itemView, ViewGroup parent) {

        View rowView = itemView;

        if (rowView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            rowView = inflater.inflate(R.layout.active_polls_list_item, parent, false);

            ViewHolder viewHolder = new ViewHolder();

            viewHolder.iv_sender = (ImageView) rowView.findViewById(R.id.sender_icon);
            viewHolder.tv_sender_name = (TextView) rowView.findViewById(R.id.tv_sender_name);
            viewHolder.tv_description = (TextView) rowView.findViewById(R.id.tv_description);
            viewHolder.tv_posting_date = (TextView) rowView.findViewById(R.id.tv_posting_date);
            viewHolder.tv_location_name = (TextView) rowView.findViewById(R.id.tv_location_name);
            viewHolder.tv_short_remaining_time = (TextView) rowView.findViewById(R.id.tv_short_remaining_time);
            viewHolder.tv_comments = (TextView) rowView.findViewById(R.id.tv_comments);
            viewHolder.tv_recipients = (TextView) rowView.findViewById(R.id.tv_recipients);
            viewHolder.gv_assets = (NonScrollableGridView) rowView.findViewById(R.id.gv_assets);

            viewHolder.arcImageView = (ImageView) rowView.findViewById(R.id.arcImageView);
            viewHolder.grayArcImageView = (ImageView) rowView.findViewById(R.id.grayArcImageView);
            viewHolder.innerWhiteImageView = (ImageView) rowView.findViewById(R.id.innerWhiteImageView);

            rowView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder) rowView.getTag();

        // get the selected entry
        PollInfo entry = (PollInfo) mListAlertInfo.get(position);

        // set data to display

        float getTimeRemainingPercentage = getTimeRemainingPercentage(entry.expiration_date, entry.post_date);
//        Log.i("Arthur", "getTimeRemainingPercentage = "+ getTimeRemainingPercentage);

        float angle = (1 - getTimeRemainingPercentage) * 360;

        ShapeDrawable grayDrawable = new ShapeDrawable(new ArcShape(270, angle));
        grayDrawable.getPaint().setColor(Color.LTGRAY);
        grayDrawable.setIntrinsicHeight(100);
        grayDrawable.setIntrinsicWidth(100);
        holder.grayArcImageView.setImageDrawable(grayDrawable);

        ShapeDrawable whiteDrawable = new ShapeDrawable(new ArcShape(270, 360));
        whiteDrawable.getPaint().setColor(Color.WHITE);
        whiteDrawable.setIntrinsicHeight(100);
        whiteDrawable.setIntrinsicWidth(100);
        holder.innerWhiteImageView.setImageDrawable(whiteDrawable);

        if (!entry.sender_image_url.equals(""))
            Picasso.with(mContext).load(entry.sender_image_url).fit().into(holder.iv_sender);
        else
            Picasso.with(mContext).load(R.drawable.silhouette).fit().into(holder.iv_sender);

        int pax_voted = 0;

        for (PollInfo.AssetInfo asset : entry.assets) {
            pax_voted += asset.count;
        }

        ActivePollsListItemGridViewAdapter adapter = new ActivePollsListItemGridViewAdapter(mContext, entry.assets, pax_voted);
        holder.gv_assets.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        holder.gv_assets.setEnabled(false);
        holder.gv_assets.setClickable(false);
        holder.gv_assets.setFocusable(false);
        holder.gv_assets.setFocusableInTouchMode(false);

        String full_name = entry.sender_first_name + " " + entry.sender_last_name;
        holder.tv_sender_name.setText(full_name);

        holder.tv_posting_date.setText("Posted on " + convertISODate(entry.post_date));
        holder.tv_location_name.setText("From: " + entry.location_name);
        holder.tv_short_remaining_time.setText(getShortTimeDifference(entry.expiration_date));
        holder.tv_description.setText(entry.description);

        holder.tv_comments.setText(String.valueOf(entry.comments_count) + " comments");

        String recipients = "";

        int num_contacts = entry.contacts.size();

        if (num_contacts == 1)
            recipients = "With " + entry.contacts.get(0).first_name;
        else if (num_contacts == 2)
            recipients = "With " + entry.contacts.get(0).first_name + " & " + entry.contacts.get(1).first_name;
        else if (num_contacts > 2)
            recipients = "With " + entry.contacts.get(0).first_name + "," + entry.contacts.get(1).first_name + " + " + String.valueOf(num_contacts - 2) + " others";

        holder.tv_recipients.setText(recipients);

        // return view
        return rowView;
    }

    public String convertISODate(String iso_date) {
        String result = "";

        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date temp_date = formatter.parse(iso_date);

            Calendar cal = Calendar.getInstance();
            cal.setTime(temp_date);

            Locale current_locale = mContext.getResources().getConfiguration().locale;

            String month_name = cal.getDisplayName(Calendar.MONTH, Calendar.SHORT, current_locale);
            String day = Integer.toString(cal.get(Calendar.DAY_OF_MONTH));
            String year = Integer.toString(cal.get(Calendar.YEAR));

            result = month_name + " " + day + ", " + year;
        } catch (Exception e) {
        }

        return result;
    }

    public String getDetailedTimeDifference(String iso_date) {
        String result = "";

        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));

            Date future_date = formatter.parse(iso_date);
            Date now_date = new Date();

            TimeUnit timeUnit = TimeUnit.SECONDS;

            long diffInMillies = future_date.getTime() - now_date.getTime();
            long s = timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);

            long days = s / (24 * 60 * 60);
            long rest = s - (days * 24 * 60 * 60);
            long std = rest / (60 * 60);
            long rest1 = rest - (std * 60 * 60);
            long min = rest1 / 60;
            long sec = s % 60;

            if (days > 0)
                result += days + " days ";
            if (std > 0)
                result += Long.toString(std) + " hours ";
            if (min > 0)
                result += Long.toString(min) + " minutes";

        } catch (Exception e) {
        }

        return result;
    }

    public String getShortTimeDifference(String iso_date) {
        String result = "";

        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));

            Date future_date = formatter.parse(iso_date);
            Date now_date = new Date();

            TimeUnit timeUnit = TimeUnit.SECONDS;

            long diffInMillies = future_date.getTime() - now_date.getTime();
            long s = timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);

            long days = s / (24 * 60 * 60);
            long rest = s - (days * 24 * 60 * 60);
            long std = rest / (60 * 60);
            long rest1 = rest - (std * 60 * 60);
            long min = rest1 / 60;
            long sec = s % 60;

            if (days > 0)
                result += Long.toString(days) + "d";
            else if (std > 0)
                result += Long.toString(std) + "h";
            else if (min > 0)
                result += Long.toString(min) + "m";

        } catch (Exception e) {
        }

        return result;
    }

    public float getTimeRemainingPercentage(String endtDate, String creationDate) {
        float result = 100.00f;

        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));

            Date end_date = formatter.parse(endtDate);
            Date posted_date = formatter.parse(creationDate);
            Date now_date = new Date();


            TimeUnit timeUnit = TimeUnit.SECONDS;

            long totalDurationMillis = end_date.getTime() - posted_date.getTime();
            long totalSeconds = timeUnit.convert(totalDurationMillis, TimeUnit.MILLISECONDS);

            long diffInMillies = end_date.getTime() - now_date.getTime();
            long currentSeconds = timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);

            Log.i("Arthur", "totalSeconds = " + totalSeconds + " currentSeconds = " + currentSeconds);

            if (currentSeconds < totalDurationMillis) {
                float remainingSecs = (float) currentSeconds / (float) totalSeconds;
                return remainingSecs;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
}

