package com.threadr.android.app;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.squareup.picasso.Picasso;
import net.sourceforge.lame.Lame;

import java.io.File;
import java.util.UUID;

/**
 * Created by lilandra on 2/19/14.
 */
public class AddAudioOptionFragment extends BaseFragment {

    private static final String TAG = "AddAudioOptionFragment";

    private String wav_url = "";
    private String mp3_url = "";

    private EditText et_description;
    private ImageView iv_play;
    private ImageView iv_rec_stop;
    private TextView tv_rec_status;

    private final int MAX_DURATION = 30;

    //private MediaRecorder media_recorder;
    private ExtAudioRecorder audio_recorder;

    private boolean is_recording = false;
    private Thread timer_thread;

    private Handler handler;

    private boolean is_loaded = false;
    private boolean is_edit_mode = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //media_recorder = new MediaRecorder();
        audio_recorder = ExtAudioRecorder.getInstanse(false);

        try {
            String seed = UUID.randomUUID().toString();
            String wav_name = String.format("threadr_%s.wav", seed);
            String mp3_name = String.format("threadr_%s.mp3", seed);
            wav_url = main_tab_activity.getApplicationContext().getFilesDir().getCanonicalPath() + "/" + wav_name;
            mp3_url = main_tab_activity.getApplicationContext().getFilesDir().getCanonicalPath() + "/" + mp3_name;
        } catch (Exception e) {
        }

        View V = inflater.inflate(R.layout.add_audio_option_view, container, false);


        et_description = (EditText) V.findViewById(R.id.et_description);
        tv_rec_status = (TextView) V.findViewById(R.id.tv_rec_status);

        iv_rec_stop = (ImageView) V.findViewById(R.id.iv_rec_stop);
        iv_rec_stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (is_recording) {

                    is_recording = false;

                    //media_recorder.stop();
                    audio_recorder.stop();
                    audio_recorder.release();

                    Picasso.with(getActivity()).load(R.drawable.begin_record).fit().centerCrop().into(iv_rec_stop);

                    iv_play.setEnabled(true);
                } else {
                    try {
                        //media_recorder.reset();

                        //media_recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                        //media_recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
                        //media_recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
                        //media_recorder.setOutputFile(wav_url);

                        //media_recorder.prepare();
                        //media_recorder.start();

                        audio_recorder = ExtAudioRecorder.getInstanse(false);

                        audio_recorder.setOutputFile(wav_url);
                        audio_recorder.prepare();
                        audio_recorder.start();

                        is_recording = true;

                        timer_thread = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                int num_sec = 0;
                                while (num_sec <= MAX_DURATION) {

                                    try {

                                        if (is_recording) {

                                            Message msgObj = handler.obtainMessage();
                                            Bundle b = new Bundle();
                                            b.putString("message", Integer.toString(num_sec));
                                            msgObj.setData(b);
                                            handler.sendMessage(msgObj);

                                            Thread.sleep(1000);

                                            num_sec += 1;
                                        }

                                    } catch (Exception e) {
                                        Log.i(TAG, e.toString());
                                    }
                                }
                            }
                        });
                        timer_thread.start();
                        Picasso.with(getActivity()).load(R.drawable.stop_record).fit().centerCrop().into(iv_rec_stop);

                        iv_play.setEnabled(false);

                    } catch (Exception e) {
                        Log.i(TAG, e.toString());
                    }
                }
            }
        });

        iv_play = (ImageView) V.findViewById(R.id.iv_play);
        iv_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!is_recording) {

                    Log.i(TAG, wav_url);

                    MediaPlayer m = new MediaPlayer();

                    try {
                        m.setDataSource(wav_url);
                        m.prepare();
                        m.start();
                    } catch (Exception e) {
                        Log.i(TAG, e.toString());
                    }
                }
            }
        });

        Button bt_add_option = (Button) V.findViewById(R.id.bt_add_option);
        bt_add_option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (new File(wav_url).exists()) {

                    // now we convert to mp3
                    Encoder lame = new Encoder(new File(wav_url), new File(mp3_url));

                    try {

                        lame.initialize();
                        lame.setPreset(Lame.LAME_PRESET_STANDARD);
                        lame.encode();
                        PollInfo.AssetInfo asset = new PollInfo.AssetInfo();

                        asset.description = et_description.getText().toString();
                        asset.url = mp3_url;
                        asset.type = AppConstants.ASSET_AUDIO;

                        if (asset.description.trim().equals(""))
                            Toast.makeText(main_tab_activity, "Missing description", Toast.LENGTH_SHORT).show();
                        else {
                            if (is_edit_mode)
                                SharedData.alert_ammendment_payload.assets.add(asset);
                            else
                                SharedData.alert_payload.assets.add(asset);

                            main_tab_activity.onBackPressed();
                        }
                    } catch (Exception e) {
                        Log.i(TAG, "ENCODING ERROR");
                    }
                }
            }
        });

        handler = new Handler() {
            public void handleMessage(Message msg) {
                String message = msg.getData().getString("message");

                if (Integer.parseInt(message) == 30) {
                    try {
                        //media_recorder.stop();
                        audio_recorder.stop();
                        audio_recorder.release();

                        Picasso.with(getActivity()).load(R.drawable.begin_record).fit().centerCrop().into(iv_rec_stop);
                        iv_play.setEnabled(true);
                    } catch (Exception e) {
                    }

                    is_recording = false;
                }

                String status = "Audio Recording: " + getStringDuration(Integer.parseInt(message));
                tv_rec_status.setText(status);
            }
        };

        return V;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!is_loaded) {
            if (arguments != null) {
                String edit_mode = arguments.getString(AppConstants.ADD_OPTION_MODE, "");
                if (!edit_mode.equals(""))
                    is_edit_mode = true;
                else
                    is_edit_mode = false;
            }
            is_loaded = true;
        }
    }

    String getStringDuration(int totalSecs) {
        String result;

        int hours = totalSecs / 3600;
        int minutes = (totalSecs % 3600) / 60;
        int seconds = totalSecs % 60;

        result = hours + ":" + minutes + ":" + seconds;
        return result;
    }

}