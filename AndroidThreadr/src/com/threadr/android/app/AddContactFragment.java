package com.threadr.android.app;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.threadr.android.app.Utils.PreferenceHelper;

import java.util.Map;

/**
 * Created by lilandra on 1/5/14.
 */
public class AddContactFragment extends BaseFragment {

    private static final String TAG = "AddContactFragment";

    private EditText et_name;
    private EditText et_email;
    private EditText et_phone_number;
    private TextView tv_error_description;
    private LinearLayout ll_error;

    private String name = "";
    private String email = "";
    private String phone_number = "";
    private String group_id = AppConstants.ALL_CONTACTS_GROUP_ID;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View V = inflater.inflate(R.layout.add_contact_view, container, false);

        // this adjust the keyboard to not cover the edit text
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        et_name = (EditText) V.findViewById(R.id.et_name);
        et_email = (EditText) V.findViewById(R.id.et_email);
        et_phone_number = (EditText) V.findViewById(R.id.et_phone_number);

        tv_error_description = (TextView) V.findViewById(R.id.tv_error_description);
        ll_error = (LinearLayout) V.findViewById(R.id.ll_error);

        Button bt_add_contact = (Button) V.findViewById(R.id.bt_add_contact);
        bt_add_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // hide keyboard
                InputMethodManager inputManager = (InputMethodManager) main_tab_activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(main_tab_activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                name = et_name.getText().toString().trim();
                email = et_email.getText().toString().trim();
                phone_number = et_phone_number.getText().toString().trim();

                Log.i(TAG, name);
                Log.i(TAG, email);
                Log.i(TAG, phone_number);

                if (name.equals("") || email.equals("") || phone_number.equals("")) {

                    ll_error.setVisibility(View.VISIBLE);

                    if (name.equals(""))
                        tv_error_description.setText("Name is required");
                    else if (phone_number.equals(""))
                        tv_error_description.setText("Phone number is required");
                    else
                        tv_error_description.setText("Email is required");

                } else {
                    ll_error.setVisibility(View.INVISIBLE);
                    new CreateContact(name, email, phone_number, group_id).execute();
                }
            }
        });

        ll_error.setVisibility(View.INVISIBLE);

        return V;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (arguments != null) {
            name = arguments.getString(AppConstants.CONTACT_NAME);
            email = arguments.getString(AppConstants.CONTACT_EMAIL);
            phone_number = arguments.getString(AppConstants.CONTACT_PHONE_NUMBER);
            group_id = arguments.getString(AppConstants.CONTACT_GROUP_ID);
        }

        updateDisplay();
    }

    private void updateDisplay() {
        et_name.setText(name);
        et_email.setText(email);
        et_phone_number.setText(phone_number);
    }

    class CreateContact extends AsyncTask<Void, Void, Void> {

        protected ProgressDialog progressDialog;

        private String name = "";
        private String email = "";
        private String phone_number = "";
        private String group_id = "";
        private boolean is_ok = false;
        private boolean is_existing_email = false;

        public CreateContact(String name, String email, String phone_number, String group_id) {
            this.name = name;
            this.email = email;
            this.phone_number = phone_number;
            this.group_id = group_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(main_tab_activity, "", "Creating contact ...", true, false);
        }

        @Override
        protected Void doInBackground(Void... params) {

            String access_token = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_ACCESS_TOKEN, "UNKNOWN");
            Map<String, Object> result = ThreadrAPIAdapter.doCreateContact(access_token, email, name, "", phone_number, group_id);

            if (result != null && result.get("result").toString().equals(AppConstants.API_RESULT_OK))
                is_ok = true;
            else if (result != null && result.get("errorMessage").toString().equals("Contact already exist."))
                is_existing_email = true;


            return null;
        }

        @Override
        protected void onPostExecute(Void ret) {
            progressDialog.dismiss();

            if (is_ok) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                LayoutInflater inflater = getActivity().getLayoutInflater();

                View add_contact_status_generic_view = inflater.inflate(R.layout.add_contact_status_generic_view, null);
                builder.setView(add_contact_status_generic_view);

                final AlertDialog dialog = builder.create();

                TextView tv_description = (TextView) add_contact_status_generic_view.findViewById(R.id.tv_description);

                tv_description.setText("Successfully added contact");

                TextView tv_continue = (TextView) add_contact_status_generic_view.findViewById(R.id.tv_continue);
                tv_continue.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        main_tab_activity.onBackPressed();
                    }
                });
                dialog.show();
            } else {
                if (is_existing_email) {
                    ll_error.setVisibility(View.VISIBLE);
                    tv_error_description.setText("There is already a contact with that email");
                }

                Toast.makeText(main_tab_activity, "Unable to complete.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}