package com.threadr.android.app;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.threadr.android.app.Utils.PreferenceHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by lilandra on 2/28/14.
 */
public class AddContactFromFacebook extends BaseFragment {

    private static final String TAG = "AddContactFromFacebook";

    private FacebookContactListViewAdapter adapter;
    private final List<FacebookContactInfo> contacts = new ArrayList<FacebookContactInfo>();

    private String facebook_token;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View V = inflater.inflate(R.layout.facebook_contact_view, container, false);

        // setup contact list

        ListView lv_contacts = (ListView) V.findViewById(R.id.lv_contacts);

        adapter = new FacebookContactListViewAdapter(main_tab_activity, contacts);

        lv_contacts.setAdapter(adapter);

        /*
        lv_contacts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View view, int pos, long id) {

                Toast.makeText(main_tab_activity, "FIRED", Toast.LENGTH_SHORT).show();
            }
        });
         */


        // setup others

        TextView tv_add_all_facebook_contacts = (TextView) V.findViewById(R.id.tv_add_all_facebook_contacts);

        tv_add_all_facebook_contacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new CreateContacts().execute();
            }
        });

        return V;
    }

    @Override
    public void onResume() {
        super.onResume();

        //Before we do anything, first we check if we have a token
        facebook_token = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_FACEBOOK_ACCESS_TOKEN, null);

        if (facebook_token != null) {
            contacts.clear();
            new LoadData().execute();
        } else {
            //We invoke facebook and get a token
            TabMain activity = (TabMain) getActivity();
            if (activity != null)
                activity.getFacebookToken();
        }
    }

    private void loadData() {

        try {
            List<String> current_contacts_emails = new ArrayList<String>();

            // we exploit the fact that all users belong to All Contacts group
            String access_token = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_ACCESS_TOKEN, "UNKNOWN");

            Map<String, Object> all_contacts_result = ThreadrAPIAdapter.doGetAllGroupContacts(access_token, AppConstants.ALL_CONTACTS_GROUP_ID);

            if (all_contacts_result != null) {

                try {
                    Object all_contacts_data = all_contacts_result.get("data");

                    for (Map<String, Object> contact : (ArrayList<Map<String, Object>>) all_contacts_data) {

                        String email = contact.get("email").toString();

                        current_contacts_emails.add(email);
                    }
                } catch (Exception e) {
                }
            }

            Map<String, Object> group_contacts_result = ThreadrAPIAdapter.doGetFacebookContacts(facebook_token);

            if (group_contacts_result != null) {
                Object group_contacts_data = group_contacts_result.get("data");

                if (group_contacts_data != null) {

                    for (Map<String, Object> contact : (ArrayList<Map<String, Object>>) group_contacts_data) {

                        FacebookContactInfo temp_contact = new FacebookContactInfo();

                        temp_contact.user_id = contact.get("userId").toString();
                        temp_contact.first_name = contact.get("firstName").toString();
                        temp_contact.last_name = contact.get("lastName").toString();
                        temp_contact.email = contact.get("email").toString();
                        temp_contact.phone_number = contact.get("phoneNumber").toString();

                        // filter for emails that already exists
                        if (!current_contacts_emails.contains(temp_contact.email))
                            contacts.add(temp_contact);
                    }
                }
            }

        } catch (Exception e) {
        }

    }

    private void updateDisplay() {

        adapter.notifyDataSetChanged();
    }

    class LoadData extends AsyncTask<Void, Void, Void> {

        protected ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(main_tab_activity, "", "Loading data", true, false);
        }

        @Override
        protected Void doInBackground(Void... params) {
            AddContactFromFacebook.this.loadData();
            return null;
        }

        @Override
        protected void onPostExecute(Void ret) {
            AddContactFromFacebook.this.updateDisplay();
            progressDialog.dismiss();
        }
    }

    class CreateContact extends AsyncTask<Void, Void, Void> {

        protected ProgressDialog progressDialog;

        private String name = "";
        private String email = "";
        private String phone_number = "";
        private String group_id = "";
        private boolean is_ok = false;
        private boolean is_existing_email = false;

        public CreateContact(String name, String email, String phone_number, String group_id) {
            this.name = name;
            this.email = email;
            this.phone_number = phone_number;
            this.group_id = group_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(main_tab_activity, "", "Creating contact ...", true, false);
        }

        @Override
        protected Void doInBackground(Void... params) {

            String access_token = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_ACCESS_TOKEN, "UNKNOWN");
            Map<String, Object> result = ThreadrAPIAdapter.doCreateContact(access_token, email, name, "", phone_number, group_id);

            if (result != null && result.get("result").toString().equals(AppConstants.API_RESULT_OK))
                is_ok = true;
            else if (result != null && result.get("errorMessage").toString().equals("Contact already exist."))
                is_existing_email = true;


            return null;
        }

        @Override
        protected void onPostExecute(Void ret) {
            progressDialog.dismiss();

            if (is_ok) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                LayoutInflater inflater = getActivity().getLayoutInflater();

                View add_contact_status_generic_view = inflater.inflate(R.layout.add_contact_status_generic_view, null);
                builder.setView(add_contact_status_generic_view);

                final AlertDialog dialog = builder.create();

                TextView tv_description = (TextView) add_contact_status_generic_view.findViewById(R.id.tv_description);

                tv_description.setText("Successfully added contact");

                TextView tv_continue = (TextView) add_contact_status_generic_view.findViewById(R.id.tv_continue);
                tv_continue.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        main_tab_activity.onBackPressed();
                    }
                });
                dialog.show();
            } else {
                if (is_existing_email)
                    Toast.makeText(main_tab_activity, "There is already a contact with that email", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(main_tab_activity, "Unable to complete.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    class CreateContacts extends AsyncTask<Void, Void, Void> {

        protected ProgressDialog progressDialog;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(main_tab_activity, "", "Creating contacts ...", true, false);
        }

        @Override
        protected Void doInBackground(Void... params) {

            String access_token = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_ACCESS_TOKEN, "UNKNOWN");

            for (FacebookContactInfo contact : contacts) {

                if (contact.is_selected) {
                    try {
                        Map<String, Object> result = ThreadrAPIAdapter.doCreateContact(access_token, contact.email, contact.first_name, contact.last_name, contact.phone_number, AppConstants.ALL_CONTACTS_GROUP_ID);

                    } catch (Exception e) {
                    }
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void ret) {
            progressDialog.dismiss();

            contacts.clear();
            new LoadData().execute();
        }
    }

}