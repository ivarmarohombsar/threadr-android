package com.threadr.android.app;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import java.util.*;

/**
 * Created by lilandra on 1/5/14.
 */
public class AddContactFromPhoneBookFragment extends BaseFragment {

    private static String TAG = "AddContactFromPhoneBookFragment";

    private ContactInfoListViewAdapter adapter;
    private final List<ContactInfo> contacts = new ArrayList<ContactInfo>();
    private final List<ContactInfo> contacts_temp = new ArrayList<ContactInfo>();

    private String group_id = AppConstants.ALL_CONTACTS_GROUP_ID;

    private boolean is_loaded = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View V = inflater.inflate(R.layout.add_contact_from_phone_book_view, container, false);

        // setup contacts list
        ListView lv_contacts = (ListView) V.findViewById(R.id.contacts);

        adapter = new ContactInfoListViewAdapter(main_tab_activity, contacts);

        lv_contacts.setAdapter(adapter);

        lv_contacts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View view, int pos, long id) {

                // hide keyboard
                InputMethodManager inputManager = (InputMethodManager) main_tab_activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(main_tab_activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                ContactInfo selected_entry = contacts.get(pos);

                String name = selected_entry.getName();
                String email = selected_entry.getEmail();
                String phone_number = selected_entry.getPhoneNumber();

                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.CONTACT_NAME, name);
                bundle.putString(AppConstants.CONTACT_EMAIL, email);
                bundle.putString(AppConstants.CONTACT_PHONE_NUMBER, phone_number);
                bundle.putString(AppConstants.CONTACT_GROUP_ID, group_id);

                AddContactFragment fragment = new AddContactFragment();
                fragment.setArguments(bundle);

                main_tab_activity.pushFragments(AppConstants.TAB_CONTACTS, fragment, true, true);
            }
        });

        EditText et_search = (EditText) V.findViewById(R.id.et_search);

        et_search.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String charText = s.toString().toLowerCase();
                contacts.clear();
                if (charText.length() == 0) {
                    contacts.addAll(contacts_temp);
                } else {
                    for (ContactInfo contact : contacts_temp) {
                        if (contact.getName().toLowerCase().contains(charText)) {
                            contacts.add(contact);
                        }
                    }
                }
                adapter.notifyDataSetChanged();
            }

        });

        return V;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!is_loaded) {

            is_loaded = true;

            if (arguments != null)
                group_id = arguments.getString(AppConstants.CONTACT_GROUP_ID);

            contacts.clear();

            new LoadData().execute();
        }
    }

    private void loadData() {

        ContentResolver cr = main_tab_activity.getContentResolver();
        Cursor contact_cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        if (contact_cursor.getCount() > 0) {
            while (contact_cursor.moveToNext()) {
                String id = contact_cursor.getString(contact_cursor.getColumnIndex(ContactsContract.Contacts._ID));
                String name = contact_cursor.getString(contact_cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                String key = contact_cursor.getString(contact_cursor.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY));
                Integer photo_id = contact_cursor.getInt(contact_cursor.getColumnIndex(ContactsContract.Contacts.PHOTO_ID));

                String phone_number = "";
                if (contact_cursor.getInt(contact_cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                    Cursor phones = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                    while (phones.moveToNext()) {
                        phone_number = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    }
                    phones.close();
                }

                if (!phone_number.equals("")) {

                    String email = "";
                    Cursor emailCur = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new String[]{id}, null);
                    while (emailCur.moveToNext()) {
                        email = emailCur.getString(emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                    }
                    emailCur.close();

                    Uri photoUri = ContentUris.withAppendedId(ContactsContract.Data.CONTENT_URI, photo_id);

                    /*
                    Cursor photo_cursor = cr.query(photoUri, new String[]{ContactsContract.CommonDataKinds.Photo.PHOTO}, null, null, null);

                    byte[] photoBytes = null;

                    try {
                        if (photo_cursor.moveToFirst())
                            photoBytes = photo_cursor.getBlob(0);

                    } catch (Exception e) {
                    } finally {
                        photo_cursor.close();
                    }

                    Drawable temp_icon = null;

                    if (photoBytes != null) {
                        Bitmap photo_bitmap = BitmapFactory.decodeByteArray(photoBytes, 0, photoBytes.length);
                        temp_icon = new BitmapDrawable(getResources(), photo_bitmap);
                    }
                    */

                /*
                boolean is_already_selected = false;

                for(ContactInfo temp_info: Common.contact_selections){
                    if(temp_info != null){
                        if(temp_info.getContactKey().equals(key)){
                            is_already_selected = true;
                            break;
                        }
                    }
                }

                if(!is_already_selected){
                    ContactInfo contact = new ContactInfo(name,key,temp_icon);
                    contacts_temp.add(contact);
                }
                */
                    //ContactInfo contact = new ContactInfo(name, email, phone_number, key, temp_icon);
                    ContactInfo contact = new ContactInfo(name, email, phone_number, key, photoUri);
                    contacts_temp.add(contact);
                }

            }
        }

        sortContactsByName();
    }

    private void updateDisplay() {
        for (ContactInfo contact_info : contacts_temp)
            contacts.add(contact_info);

        //Toast.makeText(getActivity(), Integer.toString(contacts.size()), Toast.LENGTH_SHORT).show();

        adapter.notifyDataSetChanged();
    }

    private void sortContactsByName() {
        Map<String, ContactInfo> name_contact_hash = new LinkedHashMap<String, ContactInfo>();
        for (ContactInfo contact_info : contacts_temp) {
            name_contact_hash.put(contact_info.getName(), contact_info);
        }

        contacts_temp.clear();

        TreeMap<String, ContactInfo> result = new TreeMap<String, ContactInfo>(String.CASE_INSENSITIVE_ORDER);
        result.putAll(name_contact_hash);

        name_contact_hash = result;

        for (Map.Entry<String, ContactInfo> entry : name_contact_hash.entrySet()) {
            contacts_temp.add(entry.getValue());
        }
    }

    class LoadData extends AsyncTask<Void, Void, Void> {

        protected ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(main_tab_activity, "", "Loading Data", true, false);
        }

        @Override
        protected Void doInBackground(Void... params) {
            AddContactFromPhoneBookFragment.this.loadData();
            return null;
        }

        @Override
        protected void onPostExecute(Void ret) {
            AddContactFromPhoneBookFragment.this.updateDisplay();
            progressDialog.dismiss();
        }
    }
}