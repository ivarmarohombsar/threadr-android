package com.threadr.android.app;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import com.threadr.android.app.Utils.PreferenceHelper;

import java.util.*;

/**
 * Created by lilandra on 1/30/14.
 */
public class AddContactToGroupExistingUserFragment extends Fragment {

    private static String TAG = "AddContactToGroupExistingUserFragment";

    public String group_id = "";
    private boolean is_loaded = false;

    private final List<Contact> target_contacts = new ArrayList<Contact>();
    private final List<Contact> temp_target_contacts = new ArrayList<Contact>();
    public final HashMap<String, Contact> hash_target_contacts = new HashMap<String, Contact>();

    private AddContactToGroupExistingUserListViewAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View V = inflater.inflate(R.layout.add_contact_to_group_existing_user_view, container, false);

        // this adjust the keyboard to not cover the edit text
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        // setup contacts list

        ListView lv_contacts = (ListView) V.findViewById(R.id.lv_contacts);

        adapter = new AddContactToGroupExistingUserListViewAdapter(getActivity(), hash_target_contacts, temp_target_contacts);

        lv_contacts.setAdapter(adapter);

        // set up others

        EditText et_search = (EditText) V.findViewById(R.id.et_search);

//        et_search.addTextChangedListener(new TextWatcher() {
//
//            @Override
//            public void afterTextChanged(Editable s) {
//            }
//
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                String charText = s.toString().toLowerCase();
//                temp_target_contacts.clear();
//                if (charText.length() == 0) {
//                    temp_target_contacts.addAll(target_contacts);
//                } else {
//                    for (AddContactToGroupExistingUserFragment.Contact contact : target_contacts) {
//                        if (contact.name.toLowerCase().contains(charText)) {
//                            temp_target_contacts.add(contact);
//                        }
//                    }
//                }
//                adapter.notifyDataSetChanged();
//            }
//
//        });

        return V;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!is_loaded) {

            is_loaded = true;
            target_contacts.clear();
            temp_target_contacts.clear();
            hash_target_contacts.clear();

            new LoadData(group_id).execute();
        }
    }

    private class LoadData extends AsyncTask<Void, Void, Void> {

        protected ProgressDialog progressDialog;

        private String group_id = "";
        private boolean is_ok = false;

        public LoadData(String group_id) {
            this.group_id = group_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(getActivity(), "", "Loading data", true, false);
        }

        @Override
        protected Void doInBackground(Void... params) {

            HashMap<String, Contact> hash_contacts = new HashMap<String, Contact>();

            String access_token = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_ACCESS_TOKEN, "UNKNOWN");

            try {
                // retrieve all contacts

                // we exploit the fact that all users belong to All Contacts group
                Map<String, Object> all_contacts_result = ThreadrAPIAdapter.doGetAllGroupContacts(access_token, AppConstants.ALL_CONTACTS_GROUP_ID);

                if (all_contacts_result != null) {

                    try {
                        Object all_contacts_data = all_contacts_result.get("data");

                        for (Map<String, Object> contact : (ArrayList<Map<String, Object>>) all_contacts_data) {

                            String contact_id = contact.get("userContactId").toString();
                            String first_name = contact.get("firstName").toString();
                            String last_name = contact.get("lastName").toString();

                            Contact temp_contact = new Contact();

                            temp_contact.id = contact_id;
                            temp_contact.name = first_name + " " + last_name;

                            hash_contacts.put(contact_id, temp_contact);
                        }
                    } catch (Exception e) {
                    }
                }

                // retrieve contacts from current group

                Map<String, Object> current_group_result = ThreadrAPIAdapter.doGetAllGroupContacts(access_token, group_id);

                if (current_group_result != null) {

                    try {

                        Object current_group_data = current_group_result.get("data");

                        for (Map<String, Object> contact : (ArrayList<Map<String, Object>>) current_group_data) {

                            // we remove current contact from the all contacts

                            String contact_id = contact.get("userContactId").toString();

                            if (hash_contacts.containsKey(contact_id))
                                hash_contacts.remove(contact_id);
                        }
                    } catch (Exception e) {
                    }
                }

                // sort the filtered contacts alphabetically

                HashMap<String, Contact> temp_map = new HashMap<String, Contact>();

                int seed = 0;
                for (Contact contact : hash_contacts.values()) {

                    // we make sure to add a seed number to enforce order for contacts with same first and last_name
                    seed += 1;

                    String name = contact.name + Integer.toString(seed);
                    temp_map.put(name.toLowerCase(), contact);
                }

                SortedSet<String> keys = new TreeSet<String>(temp_map.keySet());

                for (String key : keys) {
                    Contact contact = temp_map.get(key);
                    target_contacts.add(contact);
                    hash_target_contacts.put(contact.id, contact);
                }

                is_ok = true;
            } catch (Exception e) {
            }

            temp_target_contacts.addAll(target_contacts);

            return null;
        }

        @Override
        protected void onPostExecute(Void ret) {
            progressDialog.dismiss();

            if (is_ok) {
                adapter.notifyDataSetChanged();
            } else
                Toast.makeText(getActivity(), "Unable to complete.", Toast.LENGTH_SHORT).show();
        }
    }

    public static class Contact {
        public String name = "";
        public String id = "";
        public boolean is_selected = false;
    }
}