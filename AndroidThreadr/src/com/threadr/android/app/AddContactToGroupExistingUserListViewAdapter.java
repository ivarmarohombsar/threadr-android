package com.threadr.android.app;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

/**
 * Created by lilandra on 2/1/14.
 */
public class AddContactToGroupExistingUserListViewAdapter extends BaseAdapter {
    private String TAG = "AddContactToGroupExistingUserListViewAdapter";

    private final Context mContext;
    private final HashMap<String, AddContactToGroupExistingUserFragment.Contact> original_contacts;
    private final List<AddContactToGroupExistingUserFragment.Contact> temp_contacts;


    public AddContactToGroupExistingUserListViewAdapter(Context c, HashMap<String, AddContactToGroupExistingUserFragment.Contact> original, List temporary) {
        mContext = c;
        original_contacts = original;
        temp_contacts = temporary;
    }

    @Override
    public int getCount() {
        return temp_contacts.size();
    }

    @Override
    public Object getItem(int position) {
        return temp_contacts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View itemView, ViewGroup parent) {
        // reference to convertView
        View v = itemView;

        // inflate new layout if null
        if (v == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            v = inflater.inflate(R.layout.add_existing_contact_list_item, parent, false);
        }

        if (temp_contacts != null && !temp_contacts.isEmpty()) {
            // get the selected entry
            final AddContactToGroupExistingUserFragment.Contact contact = temp_contacts.get(position);

            TextView tv_contact_name = (TextView) v.findViewById(R.id.tv_contact_name);
            CheckBox cb_selected = (CheckBox) v.findViewById(R.id.cb_selected);

            // set data to display
            tv_contact_name.setText(contact.name);
            cb_selected.setChecked(contact.is_selected);
            cb_selected.setTag(contact.id);

            // set listener
            cb_selected.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean is_selected = ((CheckBox) v).isChecked();
                    String id = v.getTag().toString();

                    AddContactToGroupExistingUserFragment.Contact temp_contact = original_contacts.get(id);
                    temp_contact.is_selected = is_selected;
                }
            });
        }

        // return view
        return v;
    }
}


