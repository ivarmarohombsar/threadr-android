package com.threadr.android.app;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.threadr.android.app.Utils.PreferenceHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lilandra on 1/29/14.
 */
public class AddContactToGroupMainFragment extends BaseFragment {

    private static String TAG = "AddContactToGroupMainFragment";

    private final int TAB_EXISTING_USER = 1;
    private final int TAB_NEW_USER = 2;

    private FragmentManager fm;
    private AddContactToGroupExistingUserFragment fragment_add_existing_user;
    private AddContactToGroupNewUserFragment fragment_add_new_user;

    private TextView tv_add_existing_user;
    private TextView tv_add_new_user;

    private String group_name = "";
    private String group_id = "";

    private int current_mode = -1;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View V = inflater.inflate(R.layout.add_contact_to_group_main_view, container, false);

        group_name = arguments.getString(AppConstants.CONTACT_GROUP_NAME);
        group_id = arguments.getString(AppConstants.CONTACT_GROUP_ID);

        fm = main_tab_activity.getSupportFragmentManager();

        fragment_add_existing_user = new AddContactToGroupExistingUserFragment();
        fragment_add_existing_user.group_id = group_id;

        fragment_add_new_user = new AddContactToGroupNewUserFragment();

        TextView tv_contact_group_name = (TextView) V.findViewById(R.id.tv_contact_group_name);
        tv_contact_group_name.setText(group_name);

        tv_add_existing_user = (TextView) V.findViewById(R.id.tv_add_existing_user);
        tv_add_new_user = (TextView) V.findViewById(R.id.tv_add_new_user);

        tv_add_existing_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                current_mode = TAB_EXISTING_USER;

                tv_add_existing_user.setTextColor(Color.WHITE);
                tv_add_existing_user.setBackgroundColor(Color.RED);
                tv_add_new_user.setTextColor(Color.RED);
                tv_add_new_user.setBackgroundColor(Color.WHITE);

                FragmentTransaction transaction = fm.beginTransaction();
                transaction.replace(R.id.contentFragment, fragment_add_existing_user);
                transaction.commit();
            }
        });

        tv_add_new_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                current_mode = TAB_NEW_USER;

                tv_add_existing_user.setTextColor(Color.RED);
                tv_add_existing_user.setBackgroundColor(Color.WHITE);
                tv_add_new_user.setTextColor(Color.WHITE);
                tv_add_new_user.setBackgroundColor(Color.RED);

                FragmentTransaction transaction = fm.beginTransaction();
                transaction.replace(R.id.contentFragment, fragment_add_new_user);
                transaction.commit();
            }
        });

        Button bt_add_contact = (Button) V.findViewById(R.id.bt_add_contact);
        bt_add_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch (current_mode) {
                    case TAB_EXISTING_USER: {
                        new AddExistingContacts(fragment_add_existing_user.hash_target_contacts, group_id).execute();
                    }
                    break;
                    case TAB_NEW_USER: {
                        final String name = fragment_add_new_user.et_name.getText().toString().trim();
                        final String email = fragment_add_new_user.et_email.getText().toString().trim();
                        final String phone_number = fragment_add_new_user.et_phone_number.getText().toString().trim();

                        if (name.equals("") || email.equals("") || phone_number.equals("")) {

                            Message msgObj = fragment_add_new_user.error_message_handler.obtainMessage();
                            Bundle b = new Bundle();

                            if (name.equals(""))
                                b.putString(AppConstants.ERROR_MESSAGE, "Name is required");
                            else if (phone_number.equals(""))
                                b.putString(AppConstants.ERROR_MESSAGE, "Phone number is required");
                            else
                                b.putString(AppConstants.ERROR_MESSAGE, "Email is required");

                            msgObj.setData(b);
                            fragment_add_new_user.error_message_handler.dispatchMessage(msgObj);


                        } else
                            new CreateContact(name, email, phone_number, group_id).execute();
                    }
                    break;
                }
            }
        });

        tv_add_existing_user.performClick();

        return V;
    }

    class CreateContact extends AsyncTask<Void, Void, Void> {

        protected ProgressDialog progressDialog;

        private String name = "";
        private String email = "";
        private String phone_number = "";
        private String group_id = "";
        private boolean is_ok = false;

        public CreateContact(String name, String email, String phone_number, String group_id) {
            this.name = name;
            this.email = email;
            this.phone_number = phone_number;
            this.group_id = group_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(main_tab_activity, "", "Creating contact ...", true, false);
        }

        @Override
        protected Void doInBackground(Void... params) {

            String access_token = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_ACCESS_TOKEN, "UNKNOWN");
            Map<String, Object> result = ThreadrAPIAdapter.doCreateContact(access_token, email, name, "", phone_number, group_id);

            if (result != null && result.get("result").toString().equals(AppConstants.API_RESULT_OK))
                is_ok = true;

            return null;
        }

        @Override
        protected void onPostExecute(Void ret) {
            progressDialog.dismiss();

            if (is_ok) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                LayoutInflater inflater = getActivity().getLayoutInflater();

                View add_contact_status_generic_view = inflater.inflate(R.layout.add_contact_status_generic_view, null);
                builder.setView(add_contact_status_generic_view);

                final AlertDialog dialog = builder.create();

                TextView tv_description = (TextView) add_contact_status_generic_view.findViewById(R.id.tv_description);

                tv_description.setText("Successfully added contact");

                TextView tv_continue = (TextView) add_contact_status_generic_view.findViewById(R.id.tv_continue);
                tv_continue.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        main_tab_activity.onBackPressed();
                    }
                });
                dialog.show();
            } else
                Toast.makeText(main_tab_activity, "Unable to complete.", Toast.LENGTH_SHORT).show();
        }
    }

    class AddExistingContacts extends AsyncTask<Void, Void, Void> {

        protected ProgressDialog progressDialog;

        private HashMap<String, AddContactToGroupExistingUserFragment.Contact> contacts;
        private String group_id = "";
        private boolean is_ok = false;

        public AddExistingContacts(HashMap<String, AddContactToGroupExistingUserFragment.Contact> contacts, String group_id) {
            this.contacts = contacts;
            this.group_id = group_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(main_tab_activity, "", "Adding existing contacts ...", true, false);
        }

        @Override
        protected Void doInBackground(Void... params) {

            String access_token = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_ACCESS_TOKEN, "UNKNOWN");

            List<String> contact_ids = new ArrayList<String>();
            for (AddContactToGroupExistingUserFragment.Contact contact : contacts.values())
                if (contact.is_selected) {
                    Log.i(TAG, contact.id);
                    contact_ids.add(contact.id);
                }

            Map<String, Object> result = ThreadrAPIAdapter.doAddExistingContactToGroup(access_token, contact_ids, group_id);

            if (result != null && result.get("result").toString().equals(AppConstants.API_RESULT_OK))
                is_ok = true;

            return null;
        }

        @Override
        protected void onPostExecute(Void ret) {
            progressDialog.dismiss();

            if (is_ok) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                LayoutInflater inflater = getActivity().getLayoutInflater();

                View add_contact_status_generic_view = inflater.inflate(R.layout.add_contact_status_generic_view, null);
                builder.setView(add_contact_status_generic_view);

                final AlertDialog dialog = builder.create();

                TextView tv_description = (TextView) add_contact_status_generic_view.findViewById(R.id.tv_description);

                tv_description.setText("Successfully added contacts");

                TextView tv_continue = (TextView) add_contact_status_generic_view.findViewById(R.id.tv_continue);
                tv_continue.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        main_tab_activity.onBackPressed();
                    }
                });
                dialog.show();
            } else
                Toast.makeText(main_tab_activity, "Unable to complete.", Toast.LENGTH_SHORT).show();
        }
    }
}