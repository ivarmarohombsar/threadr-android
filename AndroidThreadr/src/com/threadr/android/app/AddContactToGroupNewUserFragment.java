package com.threadr.android.app;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by lilandra on 1/7/14.
 */
public class AddContactToGroupNewUserFragment extends Fragment {

    private static String TAG = "AddContactToGroupNewUserFragment";

    public EditText et_name;
    public EditText et_email;
    public EditText et_phone_number;

    private TextView tv_error_description;
    private LinearLayout ll_error;

    public Handler error_message_handler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View V = inflater.inflate(R.layout.add_contact_to_group_new_user_view, container, false);

        // this adjust the keyboard to not cover the edit text
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        et_name = (EditText) V.findViewById(R.id.et_name);
        et_email = (EditText) V.findViewById(R.id.et_email);
        et_phone_number = (EditText) V.findViewById(R.id.et_phone_number);

        tv_error_description = (TextView) V.findViewById(R.id.tv_error_description);
        ll_error = (LinearLayout) V.findViewById(R.id.ll_error);

        error_message_handler = new Handler() {

            public void handleMessage(Message msg) {

                String error_message = msg.getData().getString(AppConstants.ERROR_MESSAGE);

                ll_error.setVisibility(View.VISIBLE);
                tv_error_description.setText(error_message);
            }
        };

        ll_error.setVisibility(View.GONE);

        return V;
    }
}