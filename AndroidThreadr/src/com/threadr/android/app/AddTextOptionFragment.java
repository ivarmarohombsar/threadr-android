package com.threadr.android.app;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by lilandra on 2/19/14.
 */
public class AddTextOptionFragment extends BaseFragment {

    private EditText et_description;
    private boolean is_loaded = false;
    private boolean is_edit_mode = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View V = inflater.inflate(R.layout.add_text_option_view, container, false);

        et_description = (EditText) V.findViewById(R.id.et_description);

        Button bt_add_option = (Button) V.findViewById(R.id.bt_add_option);
        bt_add_option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PollInfo.AssetInfo asset = new PollInfo.AssetInfo();

                asset.description = et_description.getText().toString();
                asset.url = "";
                asset.type = AppConstants.ASSET_TEXT;

                if (asset.description.trim().equals(""))
                    Toast.makeText(main_tab_activity, "Missing description", Toast.LENGTH_SHORT).show();
                else {
                    if (is_edit_mode)
                        SharedData.alert_ammendment_payload.assets.add(asset);
                    else
                        SharedData.alert_payload.assets.add(asset);

                    main_tab_activity.onBackPressed();
                }
            }
        });
        return V;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!is_loaded) {
            if (arguments != null) {
                String edit_mode = arguments.getString(AppConstants.ADD_OPTION_MODE, "");
                if (!edit_mode.equals(""))
                    is_edit_mode = true;
                else
                    is_edit_mode = false;
            }
            is_loaded = true;
        }
    }
}