package com.threadr.android.app;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import com.androidquery.AQuery;

import java.io.File;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created by lilandra on 2/19/14.
 */
public class AddVideoOptionFragment extends BaseFragment {
    private static final String TAG = "AddVideoOptionFragment";

    private static final int SELECT_VIDEO = 0;

    private String asset_url = "";
    private List<String> asset_urls;

    private boolean is_loaded = false;
    private boolean is_edit_mode = false;

    private ImageView iv_asset;
    private EditText et_description;

    private Iterator paths_iterator;

    private AQuery aq;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View V = inflater.inflate(R.layout.add_video_option_view, container, false);

        aq = new AQuery(V);

        et_description = (EditText) V.findViewById(R.id.et_description);
        iv_asset = (ImageView) V.findViewById(R.id.iv_asset);

        iv_asset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Uri theURI = Uri.fromFile(new File(asset_url));
                    Intent intent = new Intent();
                    intent.setAction(android.content.Intent.ACTION_VIEW);
                    intent.setDataAndType(theURI, URLConnection.guessContentTypeFromName(asset_url));
                    startActivity(intent);
                } catch (Exception e) {
                    Toast.makeText(main_tab_activity, "Cannot process the media", Toast.LENGTH_SHORT).show();
                }
            }
        });

        Button bt_add_option = (Button) V.findViewById(R.id.bt_add_option);
        bt_add_option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PollInfo.AssetInfo asset = new PollInfo.AssetInfo();

                asset.description = et_description.getText().toString();
                asset.url = asset_url;
                asset.type = AppConstants.ASSET_VIDEO;

                if (asset.description.trim().equals(""))
                    Toast.makeText(main_tab_activity, "Missing description", Toast.LENGTH_SHORT).show();
                else {
                    if (is_edit_mode)
                        SharedData.alert_ammendment_payload.assets.add(asset);
                    else
                        SharedData.alert_payload.assets.add(asset);

                    if (paths_iterator.hasNext()) {
                        et_description.setText("");
                        asset_url = paths_iterator.next().toString();
                        Bitmap bmThumbnail = ThumbnailUtils.createVideoThumbnail(asset_url, MediaStore.Video.Thumbnails.MINI_KIND);
                        aq.id(iv_asset).image(bmThumbnail);
                    } else
                        main_tab_activity.onBackPressed();
                }
            }
        });

        return V;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!is_loaded) {
            if (arguments != null) {
                //asset_url = arguments.getString(AppConstants.ASSET_PATHS);
                asset_urls = Arrays.asList(arguments.getStringArray(AppConstants.ASSET_PATHS));

                paths_iterator = asset_urls.iterator();
                String edit_mode = arguments.getString(AppConstants.ADD_OPTION_MODE, "");
                if (!edit_mode.equals(""))
                    is_edit_mode = true;
                else
                    is_edit_mode = false;
                updateDisplay();
            }
            is_loaded = true;
        }
    }

    private void updateDisplay() {
        if (paths_iterator.hasNext()) {
            et_description.setText("");
            asset_url = paths_iterator.next().toString();
            Bitmap bmThumbnail = ThumbnailUtils.createVideoThumbnail(asset_url, MediaStore.Video.Thumbnails.MINI_KIND);
            aq.id(iv_asset).image(bmThumbnail);
        }
    }
}