package com.threadr.android.app;

import java.util.Arrays;
import java.util.List;

/**
 * Created by lilandra on 1/5/14.
 */
public class AppConstants {

    public static final String API_RESULT_OK = "1";

    public static final String SHARED_DATA = "shared_date";
    public static final String TAB_CONTACTS = "tab_contacts";
    public static final String TAB_POLLS = "tab_polls";
    public static final String TAB_SETTINGS = "tab_settings";

    public static final String ALL_CONTACTS_GROUP_ID = "0";

    public static final String CONTACT_GROUP_NAME = "contact_group_name";
    public static final String CONTACT_GROUP_ID = "contact_group_id";
    public static final String CONTACT_ID = "contact_id";
    public static final String CONTACT_USER_ID = "contact_user_id";

    public static final String ALERT_ID = "alert_id";
    public static final String CONTACT_NAME = "contact_name";
    public static final String CONTACT_EMAIL = "contact_email";
    public static final String CONTACT_PHONE_NUMBER = "contact_phone_number";
    public static final String PREVIOUS_FRAGMENT = "previous_fragment";

    public static final String ADD_OPTION_MODE = "add option mode";
    public static final String ADD_OPTION_MODE_NORMAL = "add option mode normal";
    public static final String ADD_OPTION_MODE_EDIT = "add option mode edit";

    public static final String ADD_MEDIA_MODE = "add media mode";
    public static final String ADD_MEDIA_MODE_EDIT = "add media mode edit";

    public static final String ERROR_MESSAGE = "error_message";

    public static final String LOC_LATITUDE = "location latitude";
    public static final String LOC_LONGITUDE = "location longitude";
    public static final String LOC_NAME = "location_name";

    // SHARED DATA
    public static final String USER_ACCESS_TOKEN = "user_access_token";
    public static final String USER_FACEBOOK_ACCESS_TOKEN = "user_facebook_access_token";
    public static final String USER_ID = "user_id";
    public static final String USER_FIRST_NAME = "user_first_name";
    public static final String USER_LAST_NAME = "user_last_name";
    public static final String USER_EMAIL = "user_email";
    public static final String USER_EMAIL_NOTIFICATION = "user_email_notification";
    public static final String USER_PHONE_NUMBER = "user_phone_number";
    public static final String USER_PROFILE_IMAGE = "user_profile_image";
    public static final String USER_ALLOW_FACEBOOK_LOGIN = "user_allow_facebook_login";
    public static final String ASSET_PATHS = "asset_url";
    public static final String SKIP_WELCOME = "skip_welcome";

    // ASSET TYPES
    public static final String ASSET_IMAGE = "p";
    public static final String ASSET_VIDEO = "v";
    public static final String ASSET_AUDIO = "a";
    public static final String ASSET_TEXT = "t";

    // GCM

    public static final String REGISTRATION_ID = "registration_id";
    public static final String APP_VERSION = "app_version";

    public static final List<String> fbPermissions = Arrays.asList("basic_info", "email");

    // GALLERY

    public static final String GALLERY_MODE = "gallery_mode";

    // POLL MEDIA / ASSETS

    public static final int SELECT_IMAGE_PREAPI18 = 1;
    public static final int SELECT_VIDEO_PREAPI18 = 2;
    public static final int SELECT_IMAGE_POSTAPI18 = 3;
    public static final int SELECT_VIDEO_POSTAPI18 = 4;
    public static final int SELECT_VIDEO_FROM_CAMERA = 5;
    public static final int SELECT_IMAGE_FROM_CAMERA = 6;
    public static final int SELECT_MEDIA_IMAGE = 7;
    public static final int SELECT_MEDIA_IMAGE_FROM_CAMERA = 8;
    public static final int SELECT_MEDIA_VIDEO = 9;
    public static final int SELECT_MEDIA_AUDIO = 10;

}
