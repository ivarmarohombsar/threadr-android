package com.threadr.android.app;

import android.os.Bundle;
import android.support.v4.app.Fragment;

/**
 * Created by lilandra on 1/5/14.
 */
public class BaseFragment extends Fragment {
    TabMain main_tab_activity;
    Bundle arguments;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        main_tab_activity = (TabMain) this.getActivity();
        arguments = getArguments();
    }

    public void onBackPressed() {
    }

    /*
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
    */


}