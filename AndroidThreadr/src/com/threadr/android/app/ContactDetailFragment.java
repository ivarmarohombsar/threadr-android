package com.threadr.android.app;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.threadr.android.app.Utils.PreferenceHelper;

import java.util.Map;

/**
 * Created by lilandra on 2/26/14.
 */
public class ContactDetailFragment extends BaseFragment {

    private static String TAG = "ContactDetailFragment";

    private String user_contact_id = "";
    private String group_id = "";

    private String name = "";
    private String email = "";
    private String phone_number = "";
    private String image_url = "";

    private TextView tv_name;
    private TextView tv_email;
    private TextView tv_phone_number;
    private ImageView iv_avatar;
    private ImageView iv_edit_contact;
    private ImageView iv_delete_contact;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View V = inflater.inflate(R.layout.contact_details, container, false);


        tv_name = (TextView) V.findViewById(R.id.tv_name);
        tv_email = (TextView) V.findViewById(R.id.tv_email);
        tv_phone_number = (TextView) V.findViewById(R.id.tv_phone_number);

        iv_avatar = (ImageView) V.findViewById(R.id.iv_avatar);

        iv_edit_contact = (ImageView) V.findViewById(R.id.iv_edit_contact);
        iv_edit_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.CONTACT_USER_ID, user_contact_id);

                EditContactFragment fragment = new EditContactFragment();
                fragment.setArguments(bundle);

                main_tab_activity.pushFragments(AppConstants.TAB_CONTACTS, fragment, true, true);
            }
        });

        iv_delete_contact = (ImageView) V.findViewById(R.id.iv_delete_contact);
        iv_delete_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                LayoutInflater inflater = getActivity().getLayoutInflater();

                View add_contact_status_generic_view = inflater.inflate(R.layout.generic_delete_prompt_view, null);
                builder.setView(add_contact_status_generic_view);

                final AlertDialog dialog = builder.create();

                TextView tv_description = (TextView) add_contact_status_generic_view.findViewById(R.id.tv_description);

                tv_description.setText("Are you sure you want to delete this contact ?");

                TextView tv_no = (TextView) add_contact_status_generic_view.findViewById(R.id.tv_no);
                tv_no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                TextView tv_yes = (TextView) add_contact_status_generic_view.findViewById(R.id.tv_yes);
                tv_yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        new DeleteContact(user_contact_id, group_id).execute();
                    }
                });

                dialog.show();
            }
        });

        return V;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (arguments != null) {
            group_id = arguments.getString(AppConstants.CONTACT_GROUP_ID);
            user_contact_id = arguments.getString(AppConstants.CONTACT_USER_ID);
        }

        new LoadData(user_contact_id).execute();
    }

    class LoadData extends AsyncTask<Void, Void, Void> {

        protected ProgressDialog progressDialog;

        private String user_contact_id = "";
        private boolean is_ok = false;

        public LoadData(String user_contact_id) {
            this.user_contact_id = user_contact_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(main_tab_activity, "", "Loading data", true, false);
        }

        @Override
        protected Void doInBackground(Void... params) {
            String access_token = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_ACCESS_TOKEN, "UNKNOWN");
            Map<String, Object> result = null;

            try {
                result = ThreadrAPIAdapter.doGetContactInfo(access_token, user_contact_id);

                if (result != null && result.get("result").toString().equals(AppConstants.API_RESULT_OK)) {
                    Map<String, Object> info = (Map<String, Object>) result.get("data");

                    String first_name = info.get("firstName").toString();
                    String last_name = info.get("lastName").toString();

                    name = String.format("%s %s", first_name, last_name);
                    email = info.get("email").toString();
                    phone_number = info.get("phoneNumber").toString();
                    image_url = "";

                    is_ok = true;
                }
            } catch (Exception e) {
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void ret) {
            progressDialog.dismiss();

            if (is_ok) {
                tv_name.setText(name);
                tv_email.setText(email);
                tv_phone_number.setText(phone_number);
                //Picasso.with(getActivity()).load(image_url).error(R.drawable.silhouette).fit().centerCrop().into(iv_avatar);
            } else
                Toast.makeText(main_tab_activity, "Unable to complete.", Toast.LENGTH_SHORT).show();
        }
    }


    class DeleteContact extends AsyncTask<Void, Void, Void> {

        protected ProgressDialog progressDialog;

        private boolean toGroup = false;
        private final String contact_id;
        private final String group_id;
        private boolean is_ok = false;

        public DeleteContact(String contact_id, String group_id) {
            this.contact_id = contact_id;
            this.group_id = group_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(main_tab_activity, "", "Deleting contacts", true, false);
        }

        @Override
        protected Void doInBackground(Void... params) {
            String access_token = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_ACCESS_TOKEN, "UNKNOWN");

            Map<String, Object> result;

            if (group_id.equals(AppConstants.ALL_CONTACTS_GROUP_ID)) {
                Log.i(TAG, "ALL CONTACTS DETECTED");
                toGroup = false;
            }
            else{
                toGroup = true;
            }

            result = ThreadrAPIAdapter.doDeleteContact(access_token, contact_id, toGroup);

            if (result != null && result.get("result").toString().equals(AppConstants.API_RESULT_OK))
                is_ok = true;

            return null;
        }

        @Override
        protected void onPostExecute(Void ret) {
            progressDialog.dismiss();

            if (is_ok)
                main_tab_activity.onBackPressed();
        }
    }


}