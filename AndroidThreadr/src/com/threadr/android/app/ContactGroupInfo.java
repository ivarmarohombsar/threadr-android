package com.threadr.android.app;

/**
 * Created by lilandra on 1/7/14.
 */
public class ContactGroupInfo {
    private String group_name = "";
    private String group_id = "";
    private int num_contacts = -1;

    public ContactGroupInfo(String group_name_param, String group_id_param, int num_contacts_param) {
        group_name = group_name_param;
        group_id = group_id_param;
        num_contacts = num_contacts_param;
    }

    public String getGroupName() {
        return group_name;
    }

    public String getGroupID() {
        return group_id;
    }

    public int getNumContacts() {
        return num_contacts;
    }
}
