package com.threadr.android.app;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.threadr.android.app.Utils.PreferenceHelper;

import java.util.*;

/**
 * Created by lilandra on 1/7/14.
 */
public class ContactGroupInfoFragment extends BaseFragment {

    private static final String TAG = "ContactGroupInfoFragment";

    private String group_name = "";
    private String group_id = "";

    private ContactGroupInfoListViewAdapter adapter;
    private final List<ThreadrContactInfo> contacts = new ArrayList<ThreadrContactInfo>();
    private final List<ThreadrContactInfo> backup_contacts = new ArrayList<ThreadrContactInfo>();

    private TextView tv_contact_group_name;
    private TextView tv_add_contact_to_group;
    private TextView tv_edit;
    private ImageView iv_save_changes;
    private ImageView iv_cancel_changes;
    private ImageView iv_delete_group;

    private LinearLayout ll_edit_options;

    public boolean in_edit_mode = false;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View V = inflater.inflate(R.layout.contact_group_info_view, container, false);

        ll_edit_options = (LinearLayout) V.findViewById(R.id.ll_edit_options);

        // setup contact list

        ListView lv_contacts = (ListView) V.findViewById(R.id.lv_contacts);

        adapter = new ContactGroupInfoListViewAdapter(main_tab_activity, contacts, this);

        lv_contacts.setAdapter(adapter);

        lv_contacts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View view, int pos, long id) {

                final ThreadrContactInfo entry = contacts.get(pos);

                if (in_edit_mode) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    LayoutInflater inflater = getActivity().getLayoutInflater();

                    View add_contact_status_generic_view = inflater.inflate(R.layout.generic_delete_prompt_view, null);
                    builder.setView(add_contact_status_generic_view);

                    final AlertDialog dialog = builder.create();

                    TextView tv_description = (TextView) add_contact_status_generic_view.findViewById(R.id.tv_description);

                    tv_description.setText("Are you sure you want to delete this contact ?");

                    TextView tv_no = (TextView) add_contact_status_generic_view.findViewById(R.id.tv_no);
                    tv_no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    TextView tv_yes = (TextView) add_contact_status_generic_view.findViewById(R.id.tv_yes);
                    tv_yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();

                            Log.i(TAG, entry.first_name);
                            Log.i(TAG, entry.last_name);
                            Log.i(TAG, entry.user_contact_id);

                            contacts.remove(entry);
                            adapter.notifyDataSetChanged();
                        }
                    });

                    dialog.show();
                } else {
                    Bundle bundle = new Bundle();
                    bundle.putString(AppConstants.CONTACT_GROUP_ID, "");
                    bundle.putString(AppConstants.CONTACT_USER_ID, entry.user_contact_id);

                    ContactDetailFragment fragment = new ContactDetailFragment();
                    fragment.setArguments(bundle);

                    main_tab_activity.pushFragments(AppConstants.TAB_CONTACTS, fragment, true, true);
                }
            }
        });

        // setup others

        tv_add_contact_to_group = (TextView) V.findViewById(R.id.tv_add_contact_to_group);

        tv_add_contact_to_group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*
                if (group_id.equals(AppConstants.ALL_CONTACTS_GROUP_ID)) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                    LayoutInflater inflater = getActivity().getLayoutInflater();

                    View add_contact_selection_view = inflater.inflate(R.layout.add_from_contact_group_selection, null);
                    builder.setView(add_contact_selection_view);

                    final AlertDialog dialog = builder.create();

                    TextView tv_contact_selection = (TextView) add_contact_selection_view.findViewById(R.id.tv_contact_selection);
                    tv_contact_selection.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //Toast.makeText(getActivity(), "CONTACT SELECTED", Toast.LENGTH_SHORT).show();
                            dialog.dismiss();

                            Bundle bundle = new Bundle();
                            bundle.putString(AppConstants.CONTACT_GROUP_NAME, group_name);
                            bundle.putString(AppConstants.CONTACT_GROUP_ID, group_id);


                            AddContactFragment fragment = new AddContactFragment();
                            fragment.setArguments(bundle);

                            main_tab_activity.pushFragments(AppConstants.TAB_CONTACTS, fragment, true, true);

                        }
                    });

                    TextView tv_contact_from_phonebook_selection = (TextView) add_contact_selection_view.findViewById(R.id.tv_contact_from_phonebook_selection);
                    tv_contact_from_phonebook_selection.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //Toast.makeText(getActivity(), "CONTACT FROM PHONEBOOK SELECTED", Toast.LENGTH_SHORT).show();
                            dialog.dismiss();

                            Bundle bundle = new Bundle();
                            bundle.putString(AppConstants.CONTACT_GROUP_ID, group_id);

                            AddContactFromPhoneBookFragment fragment = new AddContactFromPhoneBookFragment();
                            fragment.setArguments(bundle);

                            main_tab_activity.pushFragments(AppConstants.TAB_CONTACTS, fragment, true, true);
                        }
                    });

                    TextView tv_contact_from_facebook_selection = (TextView) add_contact_selection_view.findViewById(R.id.tv_contact_from_facebook_selection);
                    tv_contact_from_facebook_selection.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(getActivity(), "CONTACT FROM FACEBOOK SELECTED", Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }
                    });

                    TextView tv_cancel_selection = (TextView) add_contact_selection_view.findViewById(R.id.tv_cancel_selection);
                    tv_cancel_selection.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //Toast.makeText(getActivity(), "CANCEL SELECTED", Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }
                    });

                    dialog.show();

                } else {

                    Bundle bundle = new Bundle();
                    bundle.putString(AppConstants.CONTACT_GROUP_NAME, group_name);
                    bundle.putString(AppConstants.CONTACT_GROUP_ID, group_id);

                    AddContactToGroupMainFragment fragment = new AddContactToGroupMainFragment();
                    fragment.setArguments(bundle);

                    main_tab_activity.pushFragments(AppConstants.TAB_CONTACTS, fragment, true, true);
                }
                */

                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.CONTACT_GROUP_NAME, group_name);
                bundle.putString(AppConstants.CONTACT_GROUP_ID, group_id);

                AddContactToGroupMainFragment fragment = new AddContactToGroupMainFragment();
                fragment.setArguments(bundle);

                main_tab_activity.pushFragments(AppConstants.TAB_CONTACTS, fragment, true, true);


            }
        });

        tv_edit = (TextView) V.findViewById(R.id.tv_edit);

        tv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                in_edit_mode = !in_edit_mode;

                if (in_edit_mode) {
                    tv_edit.setVisibility(View.GONE);
                    ll_edit_options.setVisibility(View.VISIBLE);
                    tv_add_contact_to_group.setVisibility(View.GONE);
                } else {
                    ll_edit_options.setVisibility(View.GONE);
                }

                adapter.notifyDataSetChanged();
            }
        });

        iv_save_changes = (ImageView) V.findViewById(R.id.iv_save_changes);
        iv_save_changes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DeleteContacts().execute();
            }
        });

        iv_cancel_changes = (ImageView) V.findViewById(R.id.iv_cancel_changes);
        iv_cancel_changes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contacts.clear();
                for (ThreadrContactInfo contact : backup_contacts) {
                    contacts.add(contact);
                }
                adapter.notifyDataSetChanged();

                in_edit_mode = false;
                tv_edit.setVisibility(View.VISIBLE);
                ll_edit_options.setVisibility(View.GONE);
                tv_add_contact_to_group.setVisibility(View.VISIBLE);
            }
        });


        iv_delete_group = (ImageView) V.findViewById(R.id.iv_delete_group);

        iv_delete_group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                LayoutInflater inflater = getActivity().getLayoutInflater();

                View add_contact_status_generic_view = inflater.inflate(R.layout.generic_delete_prompt_view, null);
                builder.setView(add_contact_status_generic_view);

                final AlertDialog dialog = builder.create();

                TextView tv_description = (TextView) add_contact_status_generic_view.findViewById(R.id.tv_description);

                tv_description.setText("Are you sure you want to delete this group ?");

                TextView tv_no = (TextView) add_contact_status_generic_view.findViewById(R.id.tv_no);
                tv_no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                TextView tv_yes = (TextView) add_contact_status_generic_view.findViewById(R.id.tv_yes);
                tv_yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();

                        new DeleteContactGroup(group_id).execute();
                    }
                });

                dialog.show();
            }
        });

        tv_contact_group_name = (TextView) V.findViewById(R.id.tv_contact_group_name);
        //tv_group_contacts_label =  (TextView) V.findViewById(R.id.tv_group_contacts_label);

        return V;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (arguments != null) {
            group_name = arguments.getString(AppConstants.CONTACT_GROUP_NAME);
            group_id = arguments.getString(AppConstants.CONTACT_GROUP_ID);
        }

        in_edit_mode = false;
        tv_edit.setVisibility(View.VISIBLE);
        ll_edit_options.setVisibility(View.GONE);
        tv_add_contact_to_group.setVisibility(View.VISIBLE);

        contacts.clear();
        backup_contacts.clear();

        new LoadData().execute();
    }

    private void loadData() {

        String access_token = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_ACCESS_TOKEN, "UNKNOWN");

        try {
            Map<String, Object> group_contacts_result = ThreadrAPIAdapter.doGetAllGroupContacts(access_token, group_id);

            if (group_contacts_result != null) {
                Object group_contacts_data = group_contacts_result.get("data");

                if (group_contacts_data != null) {

                    for (Map<String, Object> contact : (ArrayList<Map<String, Object>>) group_contacts_data) {

                        ThreadrContactInfo temp_contact = new ThreadrContactInfo();

                        temp_contact.user_contact_id = contact.get("userContactId").toString();
                        temp_contact.contact_id = contact.get("contactId").toString();
                        temp_contact.first_name = contact.get("firstName").toString();
                        temp_contact.last_name = contact.get("lastName").toString();

                        contacts.add(temp_contact);
                        backup_contacts.add(temp_contact);
                    }
                }
            }
        } catch (Exception e) {
        }

    }

    private void updateDisplay() {

        tv_contact_group_name.setText(group_name);
        adapter.notifyDataSetChanged();
    }

    class LoadData extends AsyncTask<Void, Void, Void> {

        protected ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(main_tab_activity, "", "Loading data", true, false);
        }

        @Override
        protected Void doInBackground(Void... params) {
            ContactGroupInfoFragment.this.loadData();
            return null;
        }

        @Override
        protected void onPostExecute(Void ret) {
            ContactGroupInfoFragment.this.updateDisplay();
            progressDialog.dismiss();
        }
    }

    class DeleteContacts extends AsyncTask<Void, Void, Void> {

        protected ProgressDialog progressDialog;

        private final boolean toGroup = false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(main_tab_activity, "", "Deleting contacts", true, false);
        }

        @Override
        protected Void doInBackground(Void... params) {
            String access_token = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_ACCESS_TOKEN, "UNKNOWN");


            Set<String> all_contact_ids = new HashSet<String>();
            Set<String> remaining_contact_ids = new HashSet<String>();

            for (ThreadrContactInfo contact : backup_contacts) {
                all_contact_ids.add(contact.user_contact_id);
            }

            for (ThreadrContactInfo contact : contacts) {
                remaining_contact_ids.add(contact.user_contact_id);
            }

            all_contact_ids.removeAll(remaining_contact_ids); // this mutates all_contact_ids to have only ids to remove

            for (String contact_id : all_contact_ids) {

                Map<String, Object> result;

                result = ThreadrAPIAdapter.doDeleteContact(access_token, contact_id, toGroup);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void ret) {
            progressDialog.dismiss();

            in_edit_mode = false;
            tv_edit.setVisibility(View.VISIBLE);
            ll_edit_options.setVisibility(View.GONE);

            contacts.clear();
            backup_contacts.clear();

            new LoadData().execute();
        }
    }

    class DeleteContactGroup extends AsyncTask<Void, Void, Void> {

        protected ProgressDialog progressDialog;

        private String group_id = "";
        private boolean is_ok = false;

        public DeleteContactGroup(String group_id) {
            this.group_id = group_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(main_tab_activity, "", "Deleting contact group", true, false);
        }

        @Override
        protected Void doInBackground(Void... params) {
            String access_token = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_ACCESS_TOKEN, "UNKNOWN");

            Map<String, Object> result = ThreadrAPIAdapter.doDeleteContactGroup(access_token, group_id);

            if (result != null && result.get("result").toString().equals(AppConstants.API_RESULT_OK))
                is_ok = true;

            return null;
        }

        @Override
        protected void onPostExecute(Void ret) {
            progressDialog.dismiss();

            if (is_ok)
                main_tab_activity.onBackPressed();
            else
                Toast.makeText(main_tab_activity, "Unable to complete.", Toast.LENGTH_SHORT).show();
        }
    }
}