package com.threadr.android.app;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by lilandra on 1/25/14.
 */
public class ContactGroupInfoListViewAdapter extends BaseAdapter {

    private String TAG = "ContactGroupInfoListViewAdapter";

    private final Context mContext;
    private final List<ThreadrContactInfo> mListThreadrContactInfo;
    private final ContactGroupInfoFragment the_parent;

    public ContactGroupInfoListViewAdapter(Context c, List list, ContactGroupInfoFragment parent) {
        mContext = c;
        mListThreadrContactInfo = list;
        the_parent = parent;
    }

    @Override
    public int getCount() {
        return mListThreadrContactInfo.size();
    }

    @Override
    public Object getItem(int position) {
        return mListThreadrContactInfo.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View itemView, ViewGroup parent) {
        // reference to convertView
        View v = itemView;

        // inflate new layout if null
        if (v == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            v = inflater.inflate(R.layout.contact_group_info_list_item, parent, false);
        }

        if (mListThreadrContactInfo != null && !mListThreadrContactInfo.isEmpty()) {
            // get the selected entry
            ThreadrContactInfo entry = mListThreadrContactInfo.get(position);

            // load controls from layout resources

            TextView tv_contact_name = (TextView) v.findViewById(R.id.tv_contact_name);
            ImageView iv_remove = (ImageView) v.findViewById(R.id.iv_remove);

            // set data to display
            String full_name = entry.first_name + " " + entry.last_name;
            tv_contact_name.setText(full_name);

            if (the_parent != null) {
                if (the_parent.in_edit_mode)
                    iv_remove.setVisibility(View.VISIBLE);
                else
                    iv_remove.setVisibility(View.GONE);
            } else
                iv_remove.setVisibility(View.GONE);
        }

        // return view
        return v;
    }
}

