package com.threadr.android.app;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.threadr.android.app.Extensions.ListViewHelper;
import com.threadr.android.app.Utils.PreferenceHelper;
import com.threadr.android.app.Utils.TextFontHelper;

import java.util.*;

/**
 * Created by lilandra on 1/4/14.
 */


public class ContactGroupsFragment extends BaseFragment {

    private static final String TAG = "ContactGroupsFragment";

    private ContactGroupsListViewAdapter adapter;
    private final List<ContactGroupInfo> contact_groups = new ArrayList<ContactGroupInfo>();

    private ContactGroupInfoListViewAdapter contactsAdapter;
    private final List<ThreadrContactInfo> contacts = new ArrayList<ThreadrContactInfo>();

    private ListView lv_contact_contacts;
    private ListView lv_contact_groups;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View V = inflater.inflate(R.layout.contacts_group_view, container, false);

        // setup contact group list

        lv_contact_groups = (ListView) V.findViewById(R.id.lv_contact_groups);
        adapter = new ContactGroupsListViewAdapter(main_tab_activity, contact_groups);
        lv_contact_groups.setAdapter(adapter);
        lv_contact_groups.setOnItemClickListener(groupClickListener);

        lv_contact_contacts = (ListView) V.findViewById(R.id.lv_contact_contacts);
        contactsAdapter = new ContactGroupInfoListViewAdapter(main_tab_activity, contacts, null);
        lv_contact_contacts.setAdapter(contactsAdapter);
        lv_contact_contacts.setOnItemClickListener(contactsClickListener);

        // setup others

        RelativeLayout new_group_layout = (RelativeLayout) V.findViewById(R.id.new_group_layout);
        new_group_layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                LayoutInflater inflater = getActivity().getLayoutInflater();
                final View add_group_view = inflater.inflate(R.layout.add_group_view, null);

                builder.setView(add_group_view);

                final AlertDialog add_group_dialog = builder.create();

                TextView tv_add_group_selection = (TextView) add_group_view.findViewById(R.id.tv_add_group_selection);
                tv_add_group_selection.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        // hide keyboard
                        InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                        EditText et_group_name = (EditText) add_group_view.findViewById(R.id.et_group_name);
                        String group_name = et_group_name.getText().toString().trim();

                        if(group_name.equals("") == false)
                        {
                            new CreateContactGroup(group_name).execute();
                            add_group_dialog.dismiss();
                        }
                    }
                });

                TextView tv_cancel_selection = (TextView) add_group_view.findViewById(R.id.tv_add_group_cancel_selection);
                tv_cancel_selection.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Toast.makeText(getActivity(), "CANCEL ADD GROUP SELECTED", Toast.LENGTH_SHORT).show();
                        add_group_dialog.dismiss();
                    }
                });

                add_group_dialog.show();
            }
        });

        RelativeLayout new_contact_layout = (RelativeLayout) V.findViewById(R.id.new_contact_layout);
        new_contact_layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                main_tab_activity.pushFragments(AppConstants.TAB_CONTACTS, new AddContactFragment(), true, true);
            }
        });

        Button findContactsButton = (Button) V.findViewById(R.id.findContactsButton);
        findContactsButton.setOnClickListener(addContactsClickListener);

        //TextView tv_title = (TextView) V.findViewById(R.id.tv_title);
        //TextFontHelper.setFontAndText(getActivity(), tv_title, null);

        return V;
    }

    @Override
    public void onResume() {
        super.onResume();

        contact_groups.clear();

        //Load Groups
        new LoadData().execute();

    }

    private void loadData() {

        String access_token = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_ACCESS_TOKEN, "UNKNOWN");

        try {

            Map<String, Object> result = ThreadrAPIAdapter.doGetUserGroups(access_token);
            Object groups_data = result.get("data");

            for (Map<String, Object> group : (ArrayList<Map<String, Object>>) groups_data) {

                String group_id = group.get("groupId").toString();
                if (group_id.equals(AppConstants.ALL_CONTACTS_GROUP_ID))
                    continue; //We skip the first group since this is the summary

                String group_name = group.get("groupName").toString();
                int num_contacts = Integer.parseInt(group.get("totalCount").toString());

                Log.i(TAG, group_name);

                ContactGroupInfo temp = new ContactGroupInfo(group_name, group_id, num_contacts);
                contact_groups.add(temp);
            }
        } catch (Exception e) {
        }

        // sort the contact group names
        HashMap<String, ContactGroupInfo> temp_map = new HashMap<String, ContactGroupInfo>();

        int seed = 0;
        for (ContactGroupInfo group : contact_groups) {

            // we make sure to add a seed number to enforce order for contact groups
            seed += 1;

            String name = group.getGroupName() + Integer.toString(seed);
            temp_map.put(name.toLowerCase().trim(), group);
        }

        SortedSet<String> keys = new TreeSet<String>(temp_map.keySet());

        contact_groups.clear();

        for (String key : keys) {
            ContactGroupInfo contact_group = temp_map.get(key);
            contact_groups.add(contact_group);
        }

    }

    private void populateContactsListView() {
        adapter.notifyDataSetChanged();
        ListViewHelper.setListViewHeightBasedOnChildren(lv_contact_groups);
    }

    class LoadData extends AsyncTask<Void, Void, Void> {

        protected ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(main_tab_activity, "", "Loading data", true, false);
        }

        @Override
        protected Void doInBackground(Void... params) {
            ContactGroupsFragment.this.loadData();
            return null;
        }

        @Override
        protected void onPostExecute(Void ret) {
            ContactGroupsFragment.this.populateContactsListView();
            progressDialog.dismiss();

            //Load contacts
            new LoadContacts().execute();
        }
    }

    class CreateContactGroup extends AsyncTask<Void, Void, Void> {

        protected ProgressDialog progressDialog;

        private String group_name = "";
        private boolean is_ok = false;

        public CreateContactGroup(String group_name) {
            this.group_name = group_name;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(main_tab_activity, "", "Creating contact ...", true, false);
        }

        @Override
        protected Void doInBackground(Void... params) {

            String access_token = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_ACCESS_TOKEN, "UNKNOWN");
            Map<String, Object> result = ThreadrAPIAdapter.doCreateContactGroup(access_token, group_name);

            if (result != null && result.get("result").toString().equals(AppConstants.API_RESULT_OK))
                is_ok = true;

            return null;
        }

        @Override
        protected void onPostExecute(Void ret) {
            progressDialog.dismiss();

            if (is_ok) {
                contact_groups.clear();
                new LoadData().execute();
            } else
                Toast.makeText(main_tab_activity, "Unable to complete.", Toast.LENGTH_SHORT).show();
        }
    }

    private AdapterView.OnItemClickListener groupClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView parent, View view, int pos, long id) {
            ContactGroupInfo selected_entry = contact_groups.get(pos);

            Bundle bundle = new Bundle();
            bundle.putString(AppConstants.CONTACT_GROUP_NAME, selected_entry.getGroupName());
            bundle.putString(AppConstants.CONTACT_GROUP_ID, selected_entry.getGroupID());

            ContactGroupInfoFragment fragment = new ContactGroupInfoFragment();
            fragment.setArguments(bundle);

            main_tab_activity.pushFragments(AppConstants.TAB_CONTACTS, fragment, true, true);
        }
    };

    private AdapterView.OnItemClickListener contactsClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView parent, View view, int pos, long id) {
            ThreadrContactInfo entry = contacts.get(pos);

            Bundle bundle = new Bundle();
            bundle.putString(AppConstants.CONTACT_GROUP_ID, AppConstants.ALL_CONTACTS_GROUP_ID);
            bundle.putString(AppConstants.CONTACT_USER_ID, entry.user_contact_id);

            ContactDetailFragment fragment = new ContactDetailFragment();
            fragment.setArguments(bundle);

            main_tab_activity.pushFragments(AppConstants.TAB_CONTACTS, fragment, true, true);
        }
    };

    private OnClickListener addContactsClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            LayoutInflater inflater = getActivity().getLayoutInflater();

            View add_contact_selection_view = inflater.inflate(R.layout.add_from_contact_groups_selection, null);
            builder.setView(add_contact_selection_view);

            final AlertDialog dialog = builder.create();

            TextView tv_contact_from_phonebook_selection = (TextView) add_contact_selection_view.findViewById(R.id.tv_contact_from_phonebook_selection);
            tv_contact_from_phonebook_selection.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Toast.makeText(getActivity(), "CONTACT FROM PHONEBOOK SELECTED", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();

                    main_tab_activity.pushFragments(AppConstants.TAB_CONTACTS, new AddContactFromPhoneBookFragment(), true, true);
                }
            });

            TextView tv_contact_from_facebook_selection = (TextView) add_contact_selection_view.findViewById(R.id.tv_contact_from_facebook_selection);
            tv_contact_from_facebook_selection.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Toast.makeText(getActivity(), "CONTACT FROM FACEBOOK SELECTED", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();

                    main_tab_activity.pushFragments(AppConstants.TAB_CONTACTS, new AddContactFromFacebook(), true, true);
                }
            });

            TextView tv_cancel_selection = (TextView) add_contact_selection_view.findViewById(R.id.tv_cancel_selection);
            tv_cancel_selection.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();
        }
    };

    private class LoadContacts extends AsyncTask<Void, Void, Void> {
        protected ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(main_tab_activity, "", "Loading ...", true, false);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            String access_token = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_ACCESS_TOKEN, "UNKNOWN");

            try {
                Map<String, Object> group_contacts_result = ThreadrAPIAdapter.doGetAllGroupContacts(access_token, AppConstants.ALL_CONTACTS_GROUP_ID);

                if (group_contacts_result != null) {
                    Object group_contacts_data = group_contacts_result.get("data");

                    if (group_contacts_data != null) {

                        contacts.clear();
                        for (Map<String, Object> contact : (ArrayList<Map<String, Object>>) group_contacts_data) {

                            ThreadrContactInfo temp_contact = new ThreadrContactInfo();

                            temp_contact.user_contact_id = contact.get("userContactId").toString();
                            temp_contact.contact_id = contact.get("contactId").toString();
                            temp_contact.first_name = contact.get("firstName").toString();
                            temp_contact.last_name = contact.get("lastName").toString();

                            contacts.add(temp_contact);
                        }
                    }
                }
            } catch (Exception e) {
            }

            // sort the contact names
            HashMap<String, ThreadrContactInfo> temp_map = new HashMap<String, ThreadrContactInfo>();

            int seed = 0;
            for (ThreadrContactInfo contact : contacts) {

                // we make sure to add a seed number to enforce order for contacts with same first and last_name
                seed += 1;

                String name = contact.first_name + contact.last_name + Integer.toString(seed);
                temp_map.put(name.toLowerCase().trim(), contact);
            }

            SortedSet<String> keys = new TreeSet<String>(temp_map.keySet());

            contacts.clear();

            for (String key : keys) {
                ThreadrContactInfo contact = temp_map.get(key);
                contacts.add(contact);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressDialog.dismiss();
            contactsAdapter.notifyDataSetChanged();
            ListViewHelper.setListViewHeightBasedOnChildren(lv_contact_contacts);
        }
    }
}
