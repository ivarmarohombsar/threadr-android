package com.threadr.android.app;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by lilandra on 1/7/14.
 */
public class ContactGroupsListViewAdapter extends BaseAdapter {

    private String TAG = "ContactGroupsListViewAdapter";

    private Context mContext;
    private List mListContactGroupInfo;

    public ContactGroupsListViewAdapter(Context c, List list) {
        mContext = c;
        mListContactGroupInfo = list;
    }

    @Override
    public int getCount() {
        return mListContactGroupInfo.size();
    }

    @Override
    public Object getItem(int position) {
        return mListContactGroupInfo.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View itemView, ViewGroup parent) {
        // reference to convertView
        View v = itemView;

        // inflate new layout if null
        if (v == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            v = inflater.inflate(R.layout.contact_group_list_item, parent, false);
        }

        if (mListContactGroupInfo != null && !mListContactGroupInfo.isEmpty()) {
            // get the selected entry
            ContactGroupInfo entry = (ContactGroupInfo) mListContactGroupInfo.get(position);

            // load controls from layout resources

            TextView tv_contact_name = (TextView) v.findViewById(R.id.tv_contact_group_name);
            TextView tv_num_contacts = (TextView) v.findViewById(R.id.tv_num_contacts);

            // set data to display

            tv_contact_name.setText(entry.getGroupName());
            tv_num_contacts.setText(Integer.toString(entry.getNumContacts()));
        }

        // return view

        return v;
    }
}

