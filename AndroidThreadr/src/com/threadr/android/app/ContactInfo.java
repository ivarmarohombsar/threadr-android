package com.threadr.android.app;

import android.graphics.drawable.Drawable;
import android.net.Uri;

public class ContactInfo {

    private String name = "";
    private String email = "";
    private String phone_number = "";
    private String contact_key = "";
    //private Drawable icon = null;
    private Uri photo_uri = null;

    public ContactInfo(String contact_name_param, String contact_email_param, String contact_phone_number_param, String contact_key_param, Uri photo_uri_param ){//Drawable icon_param) {
        name = contact_name_param;
        email = contact_email_param;
        phone_number = contact_phone_number_param;
        contact_key = contact_key_param;
        //icon = icon_param;
        photo_uri = photo_uri_param;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phone_number;
    }

    public String getContactKey() {
        return contact_key;
    }

    /*public Drawable getIcon() {
        return icon;
    }
    */

    public Uri getPhotoUri() {
        return photo_uri;
    }

}
