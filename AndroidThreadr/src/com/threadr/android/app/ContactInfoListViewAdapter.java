package com.threadr.android.app;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.androidquery.AQuery;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ContactInfoListViewAdapter extends BaseAdapter {

    private String TAG = "ContactInfoListViewAdapter";

    private final Context mContext;
    private final List mListContactInfo;

    private AQuery aq;

    public ContactInfoListViewAdapter(Context c, List list) {
        mContext = c;
        mListContactInfo = list;
    }

    @Override
    public int getCount() {
        return mListContactInfo.size();
    }

    @Override
    public Object getItem(int position) {
        return mListContactInfo.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View itemView, ViewGroup parent) {
        // reference to convertView
        View v = itemView;

        // inflate new layout if null
        if (v == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            v = inflater.inflate(R.layout.contact_list_item, parent, false);
        }

        aq = new AQuery(v);

        if (mListContactInfo != null && !mListContactInfo.isEmpty()) {
            // get the selected entry
            ContactInfo entry = (ContactInfo) mListContactInfo.get(position);

            // load controls from layout resources

            ImageView iv_icon = (ImageView) v.findViewById(R.id.contact_icon);
            TextView tv_contact_name = (TextView) v.findViewById(R.id.contact_name);
            TextView tv_phone_number = (TextView) v.findViewById(R.id.phone_number);

            // set data to display

            if (entry.getPhotoUri() != null) {
                new LoadImage(entry.getPhotoUri(), iv_icon).execute();
            } else
                Picasso.with(mContext).load(R.drawable.silhouette).fit().into(iv_icon);

            tv_contact_name.setText(entry.getName());
            tv_phone_number.setText("phone: " + entry.getPhoneNumber());
        }

        // return view

        return v;
    }

    class LoadImage extends AsyncTask<Void, Void, Void> {

        private Uri photo_uri = null;
        private ImageView iv_view;
        private Drawable temp_icon = null;

        public LoadImage(Uri photo_uri, ImageView iv_view) {
            this.photo_uri = photo_uri;
            this.iv_view = iv_view;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {

                ContentResolver cr = mContext.getContentResolver();

                Cursor photo_cursor = cr.query(photo_uri, new String[]{ContactsContract.CommonDataKinds.Photo.PHOTO}, null, null, null);

                byte[] photoBytes = null;

                try {
                    if (photo_cursor.moveToFirst())
                        photoBytes = photo_cursor.getBlob(0);

                } catch (Exception e) {
                } finally {
                    photo_cursor.close();
                }

                if (photoBytes != null) {
                    Bitmap photo_bitmap = BitmapFactory.decodeByteArray(photoBytes, 0, photoBytes.length);
                    temp_icon = new BitmapDrawable(mContext.getResources(), photo_bitmap);
                }

            } catch (Exception e) {
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void ret) {

            if (temp_icon != null)
                aq.id(iv_view).image(temp_icon);
            else
                Picasso.with(mContext).load(R.drawable.silhouette).fit().into(iv_view);
        }
    }
}

