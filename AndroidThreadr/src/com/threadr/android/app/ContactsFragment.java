package com.threadr.android.app;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.threadr.android.app.Utils.PreferenceHelper;

public class ContactsFragment extends BaseFragment {

    private boolean is_greeted = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View V = inflater.inflate(R.layout.contacts_tab_view, container, false);

        TextView tv_info = (TextView) V.findViewById(R.id.info);
        String info_text = "Threadr is the best way to get advice from your friends. Send your network anything that you need advice on and get instant feedback.\n\n" +
                "So if you need an opinion on which shirt to get while you're trying them on at a store, or which design your client prefers, give Threadr a try!\n\n" +
                "Swipe right to browse to through the guided tour.";
        tv_info.setText(info_text);

        Button bt_skip = (Button) V.findViewById(R.id.bt_skip);

        bt_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                main_tab_activity.pushFragments(AppConstants.TAB_CONTACTS, new ContactGroupsFragment(), true, true);
            }
        });

        return V;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!is_greeted) {

            String tmp_user = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_FIRST_NAME, "UNKNOWN");

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            LayoutInflater inflater = getActivity().getLayoutInflater();

            View v = inflater.inflate(R.layout.contact_welcome, null);

            TextView tv_welcome_name = (TextView) v.findViewById(R.id.tv_welcome_name);
            tv_welcome_name.setText("Welcome, " + tmp_user);


            builder.setView(v);


            builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User clicked OK button
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();

            is_greeted = true;

        }

    }
}