package com.threadr.android.app;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

/**
 * Created by lilandra on 2/18/14.
 */
public class CreatePollAddOptionsListFragment extends BaseFragment {

    private CreatePollAddOptionsListViewAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View V = inflater.inflate(R.layout.create_poll_add_options_list_view, container, false);

        ListView lv_assets = (ListView) V.findViewById(R.id.lv_assets);

        adapter = new CreatePollAddOptionsListViewAdapter(main_tab_activity, SharedData.alert_payload.assets);
        lv_assets.setAdapter(adapter);

        lv_assets.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View view, int pos, long id) {
                SharedData.alert_payload.assets.remove(pos);
                adapter.notifyDataSetChanged();

                if (SharedData.alert_payload.assets.size() == 0) {
                    // routine to get back to default view
                }
            }
        });

        return V;
    }

    @Override
    public void onResume() {
        super.onResume();

        adapter.notifyDataSetChanged();
    }
}