package com.threadr.android.app;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.androidquery.AQuery;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

/**
 * Created by lilandra on 2/18/14.
 */
public class CreatePollAddOptionsListViewAdapter extends BaseAdapter {

    private final String TAG = "CreatePollAddOptionsListViewAdapter";

    private final Context mContext;
    private final List<PollInfo.AssetInfo> mAssets;

    public CreatePollAddOptionsListViewAdapter(Context c, List list) {
        mContext = c;
        mAssets = list;
    }

    @Override
    public int getCount() {
        return mAssets.size();
    }

    @Override
    public Object getItem(int position) {
        return mAssets.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View itemView, ViewGroup parent) {
        View v = itemView;

        // inflate new layout if null
        if (v == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            v = inflater.inflate(R.layout.poll_list_item, parent, false);
        }

        final AQuery aq = new AQuery(v);
        if (mAssets != null && !mAssets.isEmpty()) {
            // get the selected entry
            PollInfo.AssetInfo entry = mAssets.get(position);

            Log.i(TAG, entry.url);
            Log.i(TAG, entry.type);
            Log.i(TAG, entry.description);

            ImageView iv_asset = (ImageView) v.findViewById(R.id.iv_asset);
            TextView tv_description = (TextView) v.findViewById(R.id.tv_description);

            if (entry.type.equals(AppConstants.ASSET_IMAGE)) {
                if (entry.id.equals(""))
                    Picasso.with(mContext).load(new File(entry.url)).fit().centerCrop().into(iv_asset);
                else
                    Picasso.with(mContext).load(entry.url).fit().centerCrop().into(iv_asset);
            } else if (entry.type.equals(AppConstants.ASSET_VIDEO)) {
                if (entry.id.equals("")) {
                    Bitmap bmThumbnail = ThumbnailUtils.createVideoThumbnail(entry.url, MediaStore.Video.Thumbnails.MICRO_KIND);
                    aq.id(iv_asset).image(bmThumbnail);
                } else {
                    int i = entry.url.lastIndexOf(".");
                    String remote_image = entry.url.substring(0, i) + "_thumbnail.png";
                    Picasso.with(mContext).load(remote_image).error(R.drawable.with_video).fit().centerCrop().into(iv_asset);
                }
            } else if (entry.type.equals(AppConstants.ASSET_AUDIO))
                Picasso.with(mContext).load(R.drawable.with_audio).fit().into(iv_asset);
            else
                Picasso.with(mContext).load(R.drawable.just_text).fit().into(iv_asset);

            tv_description.setText(entry.description);
        }

        return v;
    }
}


