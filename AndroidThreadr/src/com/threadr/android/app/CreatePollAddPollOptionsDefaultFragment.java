package com.threadr.android.app;

import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.UUID;

/**
 * Created by lilandra on 2/18/14.
 */
public class CreatePollAddPollOptionsDefaultFragment extends BaseFragment {

    private static final String TAG = "CreatePollAddPollOptionsDefaultFragment";

    private Handler asset_preloading_handler;

    private static final int MAX_DURATION_LENGTH = 30;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View V = inflater.inflate(R.layout.create_poll_add_poll_options_default, container, false);

        ImageView iv_text = (ImageView) V.findViewById(R.id.iv_text);
        iv_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getActivity(), "TEXT ASSET SELECTION", Toast.LENGTH_SHORT).show();
                AddTextOptionFragment fragment = new AddTextOptionFragment();
                main_tab_activity.pushFragments(AppConstants.TAB_POLLS, fragment, true, true);
            }
        });

        ImageView iv_photo = (ImageView) V.findViewById(R.id.iv_photo);
        iv_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getActivity(),"PHOTO ASSET SELECTION",Toast.LENGTH_SHORT).show();

                AlertDialog.Builder image_builder = new AlertDialog.Builder(getActivity());

                LayoutInflater inflater = getActivity().getLayoutInflater();

                View add_image_selection_view = inflater.inflate(R.layout.add_image_selection, null);
                image_builder.setView(add_image_selection_view);

                final AlertDialog image_dialog = image_builder.create();

                TextView tv_image_library_selection = (TextView) add_image_selection_view.findViewById(R.id.tv_image_library_selection);
                tv_image_library_selection.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            intent.addCategory(Intent.CATEGORY_OPENABLE);
                            main_tab_activity.startActivityForResult(Intent.createChooser(intent, "Select Image"), AppConstants.SELECT_IMAGE_POSTAPI18);
                        } else {
                            Intent intent = new Intent(main_tab_activity, GalleryPickerActivity.class);
                            intent.putExtra(AppConstants.GALLERY_MODE, AppConstants.ASSET_IMAGE);
                            main_tab_activity.startActivityForResult(intent, AppConstants.SELECT_IMAGE_PREAPI18);
                        }

                        image_dialog.dismiss();
                    }
                });

                TextView tv_image_camera_selection = (TextView) add_image_selection_view.findViewById(R.id.tv_image_camera_selection);
                tv_image_camera_selection.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                        String unique_id = UUID.randomUUID().toString();

                        File image_file = null;
                        String path = "";

                        try {
                            path = Environment.getExternalStorageDirectory().getAbsolutePath() + String.format("/%s_threadr_photo.png", unique_id);
                            image_file = new File(path);
                        } catch (Exception e) {
                        }

                        if (image_file != null) {
                            Uri outputFileUri = Uri.fromFile(image_file);
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);

                            CreatePollFragment.camera_image_url = path;

                            main_tab_activity.startActivityForResult(intent, AppConstants.SELECT_IMAGE_FROM_CAMERA);
                        }

                        image_dialog.dismiss();
                    }
                });

                TextView tv_cancel_selection = (TextView) add_image_selection_view.findViewById(R.id.tv_cancel_selection);
                tv_cancel_selection.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Toast.makeText(getActivity(), "CANCEL SELECTED", Toast.LENGTH_SHORT).show();
                        image_dialog.dismiss();
                    }
                });

                image_dialog.show();

            }
        });

        ImageView iv_video = (ImageView) V.findViewById(R.id.iv_video);
        iv_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getActivity(), "VIDEO ASSET SELECTION", Toast.LENGTH_SHORT).show();

                AlertDialog.Builder video_builder = new AlertDialog.Builder(getActivity());

                LayoutInflater inflater = getActivity().getLayoutInflater();

                View add_video_selection_view = inflater.inflate(R.layout.add_video_selection, null);
                video_builder.setView(add_video_selection_view);

                final AlertDialog video_dialog = video_builder.create();

                TextView tv_video_library_selection = (TextView) add_video_selection_view.findViewById(R.id.tv_video_library_selection);
                tv_video_library_selection.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                            Intent intent = new Intent();
                            intent.setType("video/*");
                            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            intent.addCategory(Intent.CATEGORY_OPENABLE);
                            intent.putExtra("android.intent.extra.durationLimit", MAX_DURATION_LENGTH);
                            main_tab_activity.startActivityForResult(Intent.createChooser(intent, "Select Video"), AppConstants.SELECT_VIDEO_POSTAPI18);
                        } else {
                            Intent intent = new Intent(main_tab_activity, GalleryPickerActivity.class);
                            intent.putExtra(AppConstants.GALLERY_MODE, AppConstants.ASSET_VIDEO);
                            main_tab_activity.startActivityForResult(intent, AppConstants.SELECT_VIDEO_PREAPI18);
                        }

                        video_dialog.dismiss();
                    }
                });

                TextView tv_video_camera_selection = (TextView) add_video_selection_view.findViewById(R.id.tv_video_camera_selection);
                tv_video_camera_selection.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                        intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, MAX_DURATION_LENGTH);
                        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);
                        main_tab_activity.startActivityForResult(intent, AppConstants.SELECT_VIDEO_FROM_CAMERA);

                        video_dialog.dismiss();
                    }
                });

                TextView tv_cancel_selection = (TextView) add_video_selection_view.findViewById(R.id.tv_cancel_selection);
                tv_cancel_selection.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Toast.makeText(getActivity(), "CANCEL SELECTED", Toast.LENGTH_SHORT).show();
                        video_dialog.dismiss();
                    }
                });

                video_dialog.show();

            }
        });

        ImageView iv_audio = (ImageView) V.findViewById(R.id.iv_audio);
        iv_audio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getActivity(), "AUDIO ASSET SELECTION", Toast.LENGTH_SHORT).show();
                AddAudioOptionFragment fragment = new AddAudioOptionFragment();
                main_tab_activity.pushFragments(AppConstants.TAB_POLLS, fragment, true, true);
            }
        });

        /*
        asset_preloading_handler = new Handler() {

            public void handleMessage(Message msg) {

                String[] paths = msg.getData().getStringArray("paths");
                String asset_type = msg.getData().getString("asset_type");

                if (asset_type.equals(AppConstants.ASSET_IMAGE)) {
                    Bundle bundle = new Bundle();
                    bundle.putStringArray(AppConstants.ASSET_PATHS, paths);

                    AddPhotoOptionFragment fragment = new AddPhotoOptionFragment();
                    fragment.setArguments(bundle);

                    main_tab_activity.pushFragments(AppConstants.TAB_POLLS, fragment, true, true);
                } else if (asset_type.equals(AppConstants.ASSET_VIDEO)) {
                    Bundle bundle = new Bundle();
                    bundle.putStringArray(AppConstants.ASSET_PATHS, paths);

                    AddVideoOptionFragment fragment = new AddVideoOptionFragment();
                    fragment.setArguments(bundle);

                    main_tab_activity.pushFragments(AppConstants.TAB_POLLS, fragment, true, true);
                }
            }
        };  */

        return V;
    }

    /*
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //Toast.makeText(getActivity(), Integer.toString(requestCode), Toast.LENGTH_SHORT).show();

        if (resultCode == Activity.RESULT_OK) {

            String asset_path = "";

            Uri selectedUri = data.getData();

            try {
                asset_path = FileUtils.getPath(getActivity().getApplicationContext(), selectedUri);
            } catch (Exception e) {
                // so its not registered in the media store..thats fine. use the original
                asset_path = selectedUri.getPath();
            }

            switch (requestCode) {
                case SELECT_IMAGE_FROM_CAMERA: {

                    Message msgObj = asset_preloading_handler.obtainMessage();
                    Bundle b = new Bundle();
                    List<String> paths = new ArrayList<String>();
                    paths.add(asset_path);
                    b.putStringArray("paths", paths.toArray(new String[0]));
                    b.putString("asset_type", AppConstants.ASSET_IMAGE);
                    msgObj.setData(b);
                    asset_preloading_handler.sendMessage(msgObj);

                }
                break;
                case SELECT_VIDEO_FROM_CAMERA: {

                    Message msgObj = asset_preloading_handler.obtainMessage();
                    Bundle b = new Bundle();
                    List<String> paths = new ArrayList<String>();
                    paths.add(asset_path);
                    b.putStringArray("paths", paths.toArray(new String[0]));
                    b.putString("asset_type", AppConstants.ASSET_VIDEO);
                    msgObj.setData(b);
                    asset_preloading_handler.sendMessage(msgObj);

                }
                break;
            }
        }
    }
    */

    private String getRealPathFromURI(Uri contentUri) {
        String path = null;
        String[] proj = {MediaStore.MediaColumns.DATA};
        Cursor cursor = main_tab_activity.getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            path = cursor.getString(column_index);
        }
        cursor.close();
        return path;
    }
}