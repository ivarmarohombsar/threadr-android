package com.threadr.android.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.location.*;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.*;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.*;
import com.androidquery.AQuery;
import com.squareup.picasso.Picasso;
import com.threadr.android.app.Utils.FileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by lilandra on 1/15/14.
 */
public class CreatePollFragment extends BaseFragment {

    private static final String TAG = "CreatePollFragment";

    private FragmentManager fm;

    private CreatePollAddPollOptionsDefaultFragment fragment_create_poll_options_default;
    private CreatePollAddOptionsListFragment fragment_create_poll_options_list;

    private static final int MAX_DURATION_LENGTH = 30;

    private EditText et_description;
    private ImageView iv_asset;

    private Handler asset_preloading_handler;

    public static String camera_image_url = "";
    private static String media_camera_image_url = "";

    // GPS RELATED
    private double latitude;
    private double longitude;

    private boolean is_gps_received = false;
    private boolean is_location_listener_added = false;

    private LocationManager location_manager;
    private LocationListener location_listener_low;
    private LocationListener location_listener_high;
    private LocationProvider low_provider;
    private LocationProvider high_provider;

    private int GPS_LOCATION_WAIT = 5000; // 5 secs

    private AQuery aq;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View V = inflater.inflate(R.layout.create_poll_view, container, false);

        aq = new AQuery(V);

        // this adjust the keyboard to not cover the edit text
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        fm = main_tab_activity.getSupportFragmentManager();

        fragment_create_poll_options_default = new CreatePollAddPollOptionsDefaultFragment();
        fragment_create_poll_options_list = new CreatePollAddOptionsListFragment();

        et_description = (EditText) V.findViewById(R.id.et_description);

        iv_asset = (ImageView) V.findViewById(R.id.iv_asset);
        iv_asset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                iv_asset.setImageDrawable(null);

                SharedData.alert_payload.media.url = "";
                SharedData.alert_payload.media.type = "";
            }
        });

        TextView tv_add_media = (TextView) V.findViewById(R.id.tv_add_media);
        tv_add_media.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                LayoutInflater inflater = getActivity().getLayoutInflater();

                View add_new_media_selection_view = inflater.inflate(R.layout.add_new_media_selection, null);
                builder.setView(add_new_media_selection_view);

                final AlertDialog dialog = builder.create();

                TextView tv_media_photo = (TextView) add_new_media_selection_view.findViewById(R.id.tv_media_photo);
                tv_media_photo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Toast.makeText(getActivity(),"PHOTO ASSET SELECTION",Toast.LENGTH_SHORT).show();

                        AlertDialog.Builder image_builder = new AlertDialog.Builder(getActivity());

                        LayoutInflater inflater = getActivity().getLayoutInflater();

                        View add_image_selection_view = inflater.inflate(R.layout.add_image_selection, null);
                        image_builder.setView(add_image_selection_view);

                        final AlertDialog image_dialog = image_builder.create();

                        TextView tv_image_library_selection = (TextView) add_image_selection_view.findViewById(R.id.tv_image_library_selection);
                        tv_image_library_selection.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent();
                                intent.setType("image/*");
                                intent.setAction(Intent.ACTION_GET_CONTENT);
                                intent.addCategory(Intent.CATEGORY_OPENABLE);
                                main_tab_activity.startActivityForResult(Intent.createChooser(intent, "Select Image"), AppConstants.SELECT_MEDIA_IMAGE);

                                image_dialog.dismiss();
                            }
                        });

                        TextView tv_image_camera_selection = (TextView) add_image_selection_view.findViewById(R.id.tv_image_camera_selection);
                        tv_image_camera_selection.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                                String unique_id = UUID.randomUUID().toString();

                                File image_file = null;
                                String path = "";

                                try {
                                    path = Environment.getExternalStorageDirectory().getAbsolutePath() + String.format("/%s_threadr_photo.png", unique_id);
                                    image_file = new File(path);
                                } catch (Exception e) {
                                }

                                if (image_file != null) {
                                    Uri outputFileUri = Uri.fromFile(image_file);
                                    intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);

                                    media_camera_image_url = path;

                                    main_tab_activity.startActivityForResult(intent, AppConstants.SELECT_MEDIA_IMAGE_FROM_CAMERA);
                                }

                                image_dialog.dismiss();
                            }
                        });

                        TextView tv_cancel_selection = (TextView) add_image_selection_view.findViewById(R.id.tv_cancel_selection);
                        tv_cancel_selection.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //Toast.makeText(getActivity(), "CANCEL SELECTED", Toast.LENGTH_SHORT).show();
                                image_dialog.dismiss();
                            }
                        });

                        image_dialog.show();

                        dialog.dismiss();
                    }
                });

                TextView tv_media_video = (TextView) add_new_media_selection_view.findViewById(R.id.tv_media_video);
                tv_media_video.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Toast.makeText(getActivity(),"VIDEO ASSET SELECTION",Toast.LENGTH_SHORT).show();

                        AlertDialog.Builder video_builder = new AlertDialog.Builder(getActivity());

                        LayoutInflater inflater = getActivity().getLayoutInflater();

                        View add_video_selection_view = inflater.inflate(R.layout.add_video_selection, null);
                        video_builder.setView(add_video_selection_view);

                        final AlertDialog video_dialog = video_builder.create();

                        TextView tv_video_library_selection = (TextView) add_video_selection_view.findViewById(R.id.tv_video_library_selection);
                        tv_video_library_selection.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent();
                                intent.setType("video/*");
                                intent.setAction(Intent.ACTION_GET_CONTENT);
                                intent.addCategory(Intent.CATEGORY_OPENABLE);
                                intent.putExtra("android.intent.extra.durationLimit", MAX_DURATION_LENGTH);
                                main_tab_activity.startActivityForResult(Intent.createChooser(intent, "Select Video"), AppConstants.SELECT_MEDIA_VIDEO);

                                video_dialog.dismiss();
                            }
                        });

                        TextView tv_video_camera_selection = (TextView) add_video_selection_view.findViewById(R.id.tv_video_camera_selection);
                        tv_video_camera_selection.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

                                intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, MAX_DURATION_LENGTH);
                                intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
                                main_tab_activity.startActivityForResult(intent, AppConstants.SELECT_MEDIA_VIDEO);

                                video_dialog.dismiss();
                            }
                        });

                        TextView tv_cancel_selection = (TextView) add_video_selection_view.findViewById(R.id.tv_cancel_selection);
                        tv_cancel_selection.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //Toast.makeText(getActivity(), "CANCEL SELECTED", Toast.LENGTH_SHORT).show();
                                video_dialog.dismiss();
                            }
                        });

                        video_dialog.show();

                        dialog.dismiss();

                    }
                });

                TextView tv_media_audio = (TextView) add_new_media_selection_view.findViewById(R.id.tv_media_audio);
                tv_media_audio.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Toast.makeText(getActivity(), "AUDIO ASSET SELECTION", Toast.LENGTH_SHORT).show();

                        AddAudioMediaFragment fragment = new AddAudioMediaFragment();
                        main_tab_activity.pushFragments(AppConstants.TAB_POLLS, fragment, true, true);


                        dialog.dismiss();
                    }
                });

                TextView tv_cancel_selection = (TextView) add_new_media_selection_view.findViewById(R.id.tv_cancel_selection);
                tv_cancel_selection.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Toast.makeText(getActivity(), "CANCEL SELECTED", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });


        TextView tv_add_new_option = (TextView) V.findViewById(R.id.tv_add_new_option);
        tv_add_new_option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                LayoutInflater inflater = getActivity().getLayoutInflater();

                View add_new_option_selection_view = inflater.inflate(R.layout.add_new_option_selection, null);
                builder.setView(add_new_option_selection_view);

                final AlertDialog dialog = builder.create();

                TextView tv_option_text = (TextView) add_new_option_selection_view.findViewById(R.id.tv_option_text);
                tv_option_text.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Toast.makeText(getActivity(), "TEXT ASSET SELECTION", Toast.LENGTH_SHORT).show();

                        AddTextOptionFragment fragment = new AddTextOptionFragment();
                        main_tab_activity.pushFragments(AppConstants.TAB_POLLS, fragment, true, true);

                        dialog.dismiss();
                    }
                });

                TextView tv_option_photo = (TextView) add_new_option_selection_view.findViewById(R.id.tv_option_photo);
                tv_option_photo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Toast.makeText(getActivity(),"PHOTO ASSET SELECTION",Toast.LENGTH_SHORT).show();

                        AlertDialog.Builder image_builder = new AlertDialog.Builder(getActivity());

                        LayoutInflater inflater = getActivity().getLayoutInflater();

                        View add_image_selection_view = inflater.inflate(R.layout.add_image_selection, null);
                        image_builder.setView(add_image_selection_view);

                        final AlertDialog image_dialog = image_builder.create();

                        TextView tv_image_library_selection = (TextView) add_image_selection_view.findViewById(R.id.tv_image_library_selection);
                        tv_image_library_selection.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                                    Intent intent = new Intent();
                                    intent.setType("image/*");
                                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                                    intent.setAction(Intent.ACTION_GET_CONTENT);
                                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                                    main_tab_activity.startActivityForResult(Intent.createChooser(intent, "Select Image"), AppConstants.SELECT_IMAGE_POSTAPI18);
                                } else {

                                    Intent intent = new Intent(main_tab_activity, GalleryPickerActivity.class);
                                    intent.putExtra(AppConstants.GALLERY_MODE, AppConstants.ASSET_IMAGE);
                                    main_tab_activity.startActivityForResult(intent, AppConstants.SELECT_IMAGE_PREAPI18);
                                }

                                image_dialog.dismiss();
                            }
                        });

                        TextView tv_image_camera_selection = (TextView) add_image_selection_view.findViewById(R.id.tv_image_camera_selection);
                        tv_image_camera_selection.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                                String unique_id = UUID.randomUUID().toString();

                                File image_file = null;
                                String path = "";

                                try {
                                    path = Environment.getExternalStorageDirectory().getAbsolutePath() + String.format("/%s_threadr_photo.png", unique_id);
                                    image_file = new File(path);
                                } catch (Exception e) {
                                }

                                if (image_file != null) {
                                    Uri outputFileUri = Uri.fromFile(image_file);
                                    intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);

                                    camera_image_url = path;

                                    main_tab_activity.startActivityForResult(intent, AppConstants.SELECT_IMAGE_FROM_CAMERA);
                                }

                                image_dialog.dismiss();
                            }
                        });

                        TextView tv_cancel_selection = (TextView) add_image_selection_view.findViewById(R.id.tv_cancel_selection);
                        tv_cancel_selection.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //Toast.makeText(getActivity(), "CANCEL SELECTED", Toast.LENGTH_SHORT).show();
                                image_dialog.dismiss();
                            }
                        });

                        image_dialog.show();

                        dialog.dismiss();
                    }
                });

                TextView tv_option_video = (TextView) add_new_option_selection_view.findViewById(R.id.tv_option_video);
                tv_option_video.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Toast.makeText(getActivity(),"VIDEO ASSET SELECTION",Toast.LENGTH_SHORT).show();

                        AlertDialog.Builder video_builder = new AlertDialog.Builder(getActivity());

                        LayoutInflater inflater = getActivity().getLayoutInflater();

                        View add_video_selection_view = inflater.inflate(R.layout.add_video_selection, null);
                        video_builder.setView(add_video_selection_view);

                        final AlertDialog video_dialog = video_builder.create();

                        TextView tv_video_library_selection = (TextView) add_video_selection_view.findViewById(R.id.tv_video_library_selection);
                        tv_video_library_selection.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                                    Intent intent = new Intent();
                                    intent.setType("video/*");
                                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                                    intent.setAction(Intent.ACTION_GET_CONTENT);
                                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                                    intent.putExtra("android.intent.extra.durationLimit", MAX_DURATION_LENGTH);
                                    main_tab_activity.startActivityForResult(Intent.createChooser(intent, "Select Video"), AppConstants.SELECT_VIDEO_POSTAPI18);
                                } else {
                                    Intent intent = new Intent(main_tab_activity, GalleryPickerActivity.class);
                                    intent.putExtra(AppConstants.GALLERY_MODE, AppConstants.ASSET_VIDEO);
                                    main_tab_activity.startActivityForResult(intent, AppConstants.SELECT_VIDEO_PREAPI18);
                                }

                                video_dialog.dismiss();
                            }
                        });

                        TextView tv_video_camera_selection = (TextView) add_video_selection_view.findViewById(R.id.tv_video_camera_selection);
                        tv_video_camera_selection.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                                intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, MAX_DURATION_LENGTH);
                                intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
                                main_tab_activity.startActivityForResult(intent, AppConstants.SELECT_VIDEO_FROM_CAMERA);

                                video_dialog.dismiss();
                            }
                        });

                        TextView tv_cancel_selection = (TextView) add_video_selection_view.findViewById(R.id.tv_cancel_selection);
                        tv_cancel_selection.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //Toast.makeText(getActivity(), "CANCEL SELECTED", Toast.LENGTH_SHORT).show();
                                video_dialog.dismiss();
                            }
                        });

                        video_dialog.show();

                        dialog.dismiss();

                    }
                });

                TextView tv_option_audio = (TextView) add_new_option_selection_view.findViewById(R.id.tv_option_audio);
                tv_option_audio.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Toast.makeText(getActivity(),"AUDIO ASSET SELECTION",Toast.LENGTH_SHORT).show();
                        AddAudioOptionFragment fragment = new AddAudioOptionFragment();
                        main_tab_activity.pushFragments(AppConstants.TAB_POLLS, fragment, true, true);

                        dialog.dismiss();
                    }
                });

                TextView tv_cancel_selection = (TextView) add_new_option_selection_view.findViewById(R.id.tv_cancel_selection);
                tv_cancel_selection.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Toast.makeText(getActivity(), "CANCEL SELECTED", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });

        Button bt_next = (Button) V.findViewById(R.id.bt_next);
        bt_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String description = et_description.getText().toString().trim();

                if (!description.equals("")) {

                    // so there are 3 possibilities to show the next page
                    // 1. a media is loaded and there are no assets
                    // 2. a media is loaded and there are assets
                    // 3. a media is not loaded and there are assets
                    if (!SharedData.alert_payload.media.url.equals("") || (SharedData.alert_payload.media.url.equals("") && SharedData.alert_payload.assets.size() > 1)) {

                        SharedData.alert_payload.description = description;

                        if (location_manager.isProviderEnabled(LocationManager.GPS_PROVIDER) || location_manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                            if (!is_location_listener_added) {
                                // add location listener for all providers, fetch interval at 0 milliseconds at minimum distance change of 0 meters .. as fast as we can get gps
                                location_manager.requestLocationUpdates(low_provider.getName(), 0, 0f, location_listener_low);
                                location_manager.requestLocationUpdates(high_provider.getName(), 0, 0f, location_listener_high);

                                is_location_listener_added = true;

                                new GPSTimer().execute();
                            }
                        } else {
                            PollAddTimerFragment fragment = new PollAddTimerFragment();
                            main_tab_activity.pushFragments(AppConstants.TAB_POLLS, fragment, true, true);
                        }

                    } else if ((SharedData.alert_payload.media.url.equals("") && SharedData.alert_payload.assets.size() < 2)) {
                        Toast.makeText(getActivity(), "Add more options", Toast.LENGTH_SHORT).show();
                    }
                } else
                    Toast.makeText(getActivity(), "Input a description", Toast.LENGTH_SHORT).show();
            }
        });


        //TextView tv_title = (TextView) V.findViewById(R.id.tv_title);
        //TextFontHelper.setFontAndText(getActivity(), tv_title, null);

        asset_preloading_handler = new Handler() {

            public void handleMessage(Message msg) {

                String[] paths = msg.getData().getStringArray("paths");
                String asset_type = msg.getData().getString("asset_type");

                if (asset_type.equals(AppConstants.ASSET_IMAGE)) {
                    Bundle bundle = new Bundle();
                    bundle.putStringArray(AppConstants.ASSET_PATHS, paths);

                    AddPhotoOptionFragment fragment = new AddPhotoOptionFragment();
                    fragment.setArguments(bundle);

                    main_tab_activity.pushFragments(AppConstants.TAB_POLLS, fragment, true, true);
                } else if (asset_type.equals(AppConstants.ASSET_VIDEO)) {
                    Bundle bundle = new Bundle();
                    bundle.putStringArray(AppConstants.ASSET_PATHS, paths);

                    AddVideoOptionFragment fragment = new AddVideoOptionFragment();
                    fragment.setArguments(bundle);

                    main_tab_activity.pushFragments(AppConstants.TAB_POLLS, fragment, true, true);
                }
            }
        };

        // GPS RELATED

        location_manager = (LocationManager) main_tab_activity.getSystemService(Context.LOCATION_SERVICE);

        location_listener_low = new MyLocationListener();
        location_listener_high = new MyLocationListener();

        low_provider = location_manager.getProvider(location_manager.getBestProvider(createCoarseCriteria(), true));
        high_provider = location_manager.getProvider(location_manager.getBestProvider(createFineCriteria(), true));

        return V;
    }

    @Override
    public void onResume() {
        super.onResume();

        update_media_display();

        if (SharedData.alert_payload.assets.size() > 0) {

            FragmentTransaction transaction = fm.beginTransaction();
            transaction.replace(R.id.contentFragment, fragment_create_poll_options_list);
            transaction.commit();

        } else {

            FragmentTransaction transaction = fm.beginTransaction();
            transaction.replace(R.id.contentFragment, fragment_create_poll_options_default);
            transaction.commit();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        removeGPSListeners();

        is_location_listener_added = false;
        is_gps_received = false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        removeGPSListeners();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //Toast.makeText(getActivity(), Integer.toString(requestCode), Toast.LENGTH_SHORT).show();

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == AppConstants.SELECT_IMAGE_POSTAPI18) {
                ClipData clipData = data.getClipData();

                List<String> paths = new ArrayList<String>();

                if (clipData != null) {

                    for (int i = 0; i < clipData.getItemCount(); i++) {
                        ClipData.Item item = clipData.getItemAt(i);

                        Uri temp_uri = item.getUri();

                        String asset_path = "";

                        Log.i(TAG + " API 18 ", asset_path);

                        try {
                            asset_path = FileUtils.getPath(getActivity().getApplicationContext(), temp_uri);
                        } catch (Exception e) {
                            // so its not registered in the media store..thats fine. use the original
                            asset_path = temp_uri.getPath();
                        }

                        paths.add(asset_path);
                    }
                } else {
                    String asset_path;

                    Uri selectedUri = data.getData();

                    Log.i(TAG, selectedUri.toString());

                    try {
                        asset_path = FileUtils.getPath(getActivity().getApplicationContext(), selectedUri);
                    } catch (Exception e) {
                        // so its not registered in the media store..thats fine. use the original
                        asset_path = selectedUri.getPath();
                    }

                    paths.add(asset_path);
                }

                if (paths.size() > 0) {
                    Message msgObj = asset_preloading_handler.obtainMessage();
                    Bundle b = new Bundle();
                    b.putStringArray("paths", paths.toArray(new String[0]));
                    b.putString("asset_type", AppConstants.ASSET_IMAGE);
                    msgObj.setData(b);
                    asset_preloading_handler.sendMessage(msgObj);
                }

            } else if (requestCode == AppConstants.SELECT_VIDEO_POSTAPI18) {
                ClipData clipData = data.getClipData();

                List<String> paths = new ArrayList<String>();

                if (clipData != null) {

                    for (int i = 0; i < clipData.getItemCount(); i++) {
                        ClipData.Item item = clipData.getItemAt(i);

                        Uri temp_uri = item.getUri();

                        String asset_path = "";

                        Log.i(TAG + " API 18 ", asset_path);

                        try {
                            asset_path = FileUtils.getPath(getActivity().getApplicationContext(), temp_uri);
                        } catch (Exception e) {
                            // so its not registered in the media store..thats fine. use the original
                            asset_path = temp_uri.getPath();
                        }

                        paths.add(asset_path);
                    }
                } else {
                    String asset_path;

                    Uri selectedUri = data.getData();

                    Log.i(TAG, selectedUri.toString());

                    try {
                        asset_path = FileUtils.getPath(getActivity().getApplicationContext(), selectedUri);
                    } catch (Exception e) {
                        // so its not registered in the media store..thats fine. use the original
                        asset_path = selectedUri.getPath();
                    }

                    paths.add(asset_path);
                }

                if (paths.size() > 0) {
                    Message msgObj = asset_preloading_handler.obtainMessage();
                    Bundle b = new Bundle();
                    b.putStringArray("paths", paths.toArray(new String[0]));
                    b.putString("asset_type", AppConstants.ASSET_VIDEO);
                    msgObj.setData(b);
                    asset_preloading_handler.sendMessage(msgObj);
                }
            } else if (requestCode == AppConstants.SELECT_IMAGE_FROM_CAMERA) {
                Message msgObj = asset_preloading_handler.obtainMessage();
                Bundle b = new Bundle();
                List<String> paths = new ArrayList<String>();
                paths.add(camera_image_url);
                b.putStringArray("paths", paths.toArray(new String[0]));
                b.putString("asset_type", AppConstants.ASSET_IMAGE);
                msgObj.setData(b);
                asset_preloading_handler.sendMessage(msgObj);
                camera_image_url = "";
            } else if (requestCode == AppConstants.SELECT_MEDIA_IMAGE_FROM_CAMERA) {
                SharedData.alert_payload.media.url = media_camera_image_url;
                SharedData.alert_payload.media.type = AppConstants.ASSET_IMAGE;

                Picasso.with(getActivity()).load(new File(media_camera_image_url)).error(R.drawable.with_photo).fit().into(iv_asset);

                createYesNoOptions();

            } else if (requestCode == AppConstants.SELECT_IMAGE_PREAPI18) {
                List<String> paths = data.getStringArrayListExtra("paths");
                Message msgObj = asset_preloading_handler.obtainMessage();
                Bundle b = new Bundle();
                b.putStringArray("paths", paths.toArray(new String[0]));
                b.putString("asset_type", AppConstants.ASSET_IMAGE);
                msgObj.setData(b);
                asset_preloading_handler.sendMessage(msgObj);
            } else if (requestCode == AppConstants.SELECT_VIDEO_PREAPI18) {
                List<String> paths = data.getStringArrayListExtra("paths");
                Message msgObj = asset_preloading_handler.obtainMessage();
                Bundle b = new Bundle();
                b.putStringArray("paths", paths.toArray(new String[0]));
                b.putString("asset_type", AppConstants.ASSET_VIDEO);
                msgObj.setData(b);
                asset_preloading_handler.sendMessage(msgObj);
            } else {
                String asset_path;

                Uri selectedUri = data.getData();

                Log.i(TAG, selectedUri.toString());

                try {
                    asset_path = FileUtils.getPath(getActivity().getApplicationContext(), selectedUri);
                } catch (Exception e) {
                    // so its not registered in the media store..thats fine. use the original
                    asset_path = selectedUri.getPath();
                }

                Log.i(TAG, asset_path);

                switch (requestCode) {
                    case AppConstants.SELECT_VIDEO_FROM_CAMERA: {

                        Message msgObj = asset_preloading_handler.obtainMessage();
                        Bundle b = new Bundle();
                        List<String> paths = new ArrayList<String>();
                        paths.add(asset_path);
                        b.putStringArray("paths", paths.toArray(new String[0]));
                        b.putString("asset_type", AppConstants.ASSET_VIDEO);
                        msgObj.setData(b);
                        asset_preloading_handler.sendMessage(msgObj);                        
                    }
                    break;
                    case AppConstants.SELECT_MEDIA_IMAGE: {
                        SharedData.alert_payload.media.url = asset_path;
                        SharedData.alert_payload.media.type = AppConstants.ASSET_IMAGE;

                        Picasso.with(getActivity()).load(new File(asset_path)).error(R.drawable.with_photo).fit().into(iv_asset);

                        createYesNoOptions();
                    }
                    break;
                    case AppConstants.SELECT_MEDIA_VIDEO: {
                        SharedData.alert_payload.media.url = asset_path;
                        SharedData.alert_payload.media.type = AppConstants.ASSET_VIDEO;

                        Bitmap bmThumbnail = ThumbnailUtils.createVideoThumbnail(asset_path, MediaStore.Video.Thumbnails.MICRO_KIND);
                        aq.id(iv_asset).image(bmThumbnail);

                        createYesNoOptions();
                    }
                    break;
                    case AppConstants.SELECT_MEDIA_AUDIO: {
                        SharedData.alert_payload.media.url = asset_path;
                        SharedData.alert_payload.media.type = AppConstants.ASSET_AUDIO;

                        Picasso.with(getActivity()).load(R.drawable.with_audio).fit().into(iv_asset);

                        createYesNoOptions();
                    }
                    break;
                }
            }
        } else {
            if (requestCode == AppConstants.SELECT_IMAGE_FROM_CAMERA)
                camera_image_url = "";

            if (requestCode == AppConstants.SELECT_MEDIA_IMAGE_FROM_CAMERA)
                media_camera_image_url = "";
        }
    }

    private String getRealPathFromURI(Uri contentUri) {
        String path = null;
        String[] proj = {MediaStore.MediaColumns.DATA};
        Cursor cursor = main_tab_activity.getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            path = cursor.getString(column_index);
        }
        cursor.close();
        return path;
    }

    private void removeGPSListeners() {
        try {
            location_manager.removeUpdates(location_listener_low);
        } catch (Exception e) {
        }

        try {
            location_manager.removeUpdates(location_listener_high);
        } catch (Exception e) {
        }
    }

    private void update_media_display() {
        if (!SharedData.alert_payload.media.url.equals("")) {
            if (SharedData.alert_payload.media.type.equals(AppConstants.ASSET_IMAGE)) {

                Picasso.with(getActivity()).load(new File(SharedData.alert_payload.media.url)).error(R.drawable.with_photo).fit().into(iv_asset);

            } else if (SharedData.alert_payload.media.type.equals(AppConstants.ASSET_VIDEO)) {
                Bitmap bmThumbnail = ThumbnailUtils.createVideoThumbnail(SharedData.alert_payload.media.url, MediaStore.Video.Thumbnails.MICRO_KIND);
                aq.id(iv_asset).image(bmThumbnail);
            } else {
                Picasso.with(getActivity()).load(R.drawable.with_audio).fit().into(iv_asset);
            }

        }
    }

    private void createYesNoOptions() {
        if (!SharedData.alert_payload.media.url.equals("") && SharedData.alert_payload.assets.size() == 0) {
            PollInfo.AssetInfo yes_option = new PollInfo.AssetInfo();
            PollInfo.AssetInfo no_option = new PollInfo.AssetInfo();

            yes_option.description = "YES";
            yes_option.type = AppConstants.ASSET_TEXT;

            no_option.description = "NO";
            no_option.type = AppConstants.ASSET_TEXT;

            SharedData.alert_payload.assets.add(yes_option);
            SharedData.alert_payload.assets.add(no_option);
        }
    }

    // ------------------ GPS RELATED -------------------
    // -- lets do this the professional way

    /**
     * this criteria will settle for less accuracy, high power, and cost
     */
    public Criteria createCoarseCriteria() {

        Criteria c = new Criteria();
        c.setAccuracy(Criteria.ACCURACY_COARSE);
        c.setBearingAccuracy(Criteria.NO_REQUIREMENT);
        c.setAltitudeRequired(false);
        c.setBearingRequired(false);
        c.setSpeedRequired(false);
        c.setCostAllowed(true);
        c.setPowerRequirement(Criteria.POWER_LOW);
        return c;
    }

    /**
     * this criteria needs high accuracy, high power, and cost
     */
    public Criteria createFineCriteria() {

        Criteria c = new Criteria();
        c.setAccuracy(Criteria.ACCURACY_FINE);
        c.setBearingAccuracy(Criteria.NO_REQUIREMENT);
        c.setAltitudeRequired(false);
        c.setBearingRequired(false);
        c.setSpeedRequired(false);
        c.setCostAllowed(true);
        c.setPowerRequirement(Criteria.POWER_HIGH);
        return c;
    }

    private class MyLocationListener implements LocationListener {

        @Override
        public void onLocationChanged(Location loc) {

            //Toast.makeText(getActivity(), "GPS Location received", Toast.LENGTH_LONG).show();
            latitude = loc.getLatitude();
            longitude = loc.getLongitude();

            is_gps_received = true;

            removeGPSListeners();
        }

        @Override
        public void onProviderDisabled(String provider) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    }

    class GPSTimer extends AsyncTask<Void, Void, Void> {

        protected ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(main_tab_activity, "", "Waiting for 5 sec for gps fix", true, false);
        }

        @Override
        protected Void doInBackground(Void... params) {

            long startTime = System.currentTimeMillis();
            long elapsedTime = 0L;

            while (elapsedTime < 5 * 1000) {
                //perform db poll/check
                elapsedTime = (new Date()).getTime() - startTime;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void ret) {
            progressDialog.dismiss();

            if (is_gps_received) {
                SharedData.alert_payload.latitude = latitude;
                SharedData.alert_payload.longitude = longitude;
            }

            PollAddTimerFragment fragment = new PollAddTimerFragment();
            main_tab_activity.pushFragments(AppConstants.TAB_POLLS, fragment, true, true);

        }
    }
}