package com.threadr.android.app;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.threadr.android.app.Utils.PreferenceHelper;

import java.util.Map;

/**
 * Created by lilandra on 1/31/14.
 */
public class EditContactFragment extends BaseFragment {

    private static String TAG = "EditContactFragment";

    private String user_contact_id = "";

    private String name = "";
    private String email = "";
    private String phone_number = "";

    private EditText et_name;
    private EditText et_email;
    private EditText et_phone_number;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View V = inflater.inflate(R.layout.edit_contact_view, container, false);

        // this adjust the keyboard to not cover the edit text
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        et_name = (EditText) V.findViewById(R.id.et_name);
        et_email = (EditText) V.findViewById(R.id.et_email);
        et_phone_number = (EditText) V.findViewById(R.id.et_phone_number);

        Button bt_submit_contact = (Button) V.findViewById(R.id.bt_submit_contact);
        bt_submit_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                name = et_name.getText().toString().trim();
                email = et_email.getText().toString().trim();
                phone_number = et_phone_number.getText().toString().trim();

                if (name.equals("") || email.equals("") || phone_number.equals("")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    LayoutInflater inflater = getActivity().getLayoutInflater();

                    View add_contact_status_generic_view = inflater.inflate(R.layout.add_contact_status_generic_view, null);
                    builder.setView(add_contact_status_generic_view);

                    final AlertDialog dialog = builder.create();

                    TextView tv_description = (TextView) add_contact_status_generic_view.findViewById(R.id.tv_description);

                    if (name.equals(""))
                        tv_description.setText("Please input a name");
                    else if (email.equals(""))
                        tv_description.setText("Please input an email address");
                    else
                        tv_description.setText("Please input a phone number");

                    TextView tv_continue = (TextView) add_contact_status_generic_view.findViewById(R.id.tv_continue);
                    tv_continue.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    dialog.show();
                } else
                    new EditContact(name, email, phone_number, user_contact_id).execute();
            }
        });

        return V;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (arguments != null)
            user_contact_id = arguments.getString(AppConstants.CONTACT_USER_ID);

        new LoadData(user_contact_id).execute();
    }

    class LoadData extends AsyncTask<Void, Void, Void> {

        protected ProgressDialog progressDialog;

        private String user_contact_id = "";
        private boolean is_ok = false;

        public LoadData(String user_contact_id) {
            this.user_contact_id = user_contact_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(main_tab_activity, "", "Loading data", true, false);
        }

        @Override
        protected Void doInBackground(Void... params) {
            String access_token = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_ACCESS_TOKEN, "UNKNOWN");
            Map<String, Object> result = null;

            try {
                result = ThreadrAPIAdapter.doGetContactInfo(access_token, user_contact_id);

                if (result != null && result.get("result").toString().equals(AppConstants.API_RESULT_OK)) {
                    Map<String, Object> info = (Map<String, Object>) result.get("data");

                    String first_name = info.get("firstName").toString();
                    String last_name = info.get("lastName").toString();

                    name = String.format("%s %s", first_name, last_name);
                    email = info.get("email").toString();
                    phone_number = info.get("phoneNumber").toString();

                    is_ok = true;
                }
            } catch (Exception e) {
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void ret) {
            progressDialog.dismiss();

            if (is_ok) {
                et_name.setText(name);
                et_email.setText(email);
                et_phone_number.setText(phone_number);
            } else
                Toast.makeText(main_tab_activity, "Unable to complete.", Toast.LENGTH_SHORT).show();
        }
    }

    class EditContact extends AsyncTask<Void, Void, Void> {

        protected ProgressDialog progressDialog;

        private String name = "";
        private String email = "";
        private String phone_number = "";
        private String contact_id = "";
        private boolean is_ok = false;

        public EditContact(String name, String email, String phone_number, String contact_id) {
            this.name = name;
            this.email = email;
            this.phone_number = phone_number;
            this.contact_id = contact_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(main_tab_activity, "", "Editing contact ...", true, false);
        }

        @Override
        protected Void doInBackground(Void... params) {

            String access_token = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_ACCESS_TOKEN, "UNKNOWN");
            Map<String, Object> result = ThreadrAPIAdapter.doEditContact(access_token, email, name, "", phone_number, contact_id);

            if (result != null && result.get("result").toString().equals(AppConstants.API_RESULT_OK))
                is_ok = true;

            return null;
        }

        @Override
        protected void onPostExecute(Void ret) {
            progressDialog.dismiss();

            if (is_ok) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                LayoutInflater inflater = getActivity().getLayoutInflater();

                View add_contact_status_generic_view = inflater.inflate(R.layout.add_contact_status_generic_view, null);
                builder.setView(add_contact_status_generic_view);

                final AlertDialog dialog = builder.create();

                TextView tv_description = (TextView) add_contact_status_generic_view.findViewById(R.id.tv_description);

                tv_description.setText("Successfully edited contact");

                TextView tv_continue = (TextView) add_contact_status_generic_view.findViewById(R.id.tv_continue);
                tv_continue.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        main_tab_activity.onBackPressed();
                    }
                });
                dialog.show();
            } else
                Toast.makeText(main_tab_activity, "Unable to complete.", Toast.LENGTH_SHORT).show();
        }
    }


}