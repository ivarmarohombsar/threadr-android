package com.threadr.android.app;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.threadr.android.app.Utils.PreferenceHelper;

import java.util.Map;

/**
 * Created by lilandra on 3/17/14.
 */
public class EditPasswordFragment extends BaseFragment {

    private static final String TAG = "EditPasswordFragment";

    private EditText et_old_password;
    private EditText et_new_password;
    private EditText et_confirm_password;
    private TextView tv_error_description;
    private LinearLayout ll_error;

    private String old_password = "";
    private String new_password = "";
    private String confirm_password = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View V = inflater.inflate(R.layout.edit_password_view, container, false);

        // this adjust the keyboard to not cover the edit text
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        et_old_password = (EditText) V.findViewById(R.id.et_old_password);
        et_new_password = (EditText) V.findViewById(R.id.et_new_password);
        et_confirm_password = (EditText) V.findViewById(R.id.et_confirm_password);

        tv_error_description = (TextView) V.findViewById(R.id.tv_error_description);
        ll_error = (LinearLayout) V.findViewById(R.id.ll_error);

        ImageView iv_save_changes = (ImageView) V.findViewById(R.id.iv_save_changes);
        iv_save_changes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // hide keyboard
                InputMethodManager inputManager = (InputMethodManager) main_tab_activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(main_tab_activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                old_password = et_old_password.getText().toString().trim();
                new_password = et_new_password.getText().toString().trim();
                confirm_password = et_confirm_password.getText().toString().trim();

                Log.i(TAG, old_password);
                Log.i(TAG, new_password);
                Log.i(TAG, confirm_password);

                if (old_password.equals("") || new_password.equals("") || confirm_password.equals("")) {

                    ll_error.setVisibility(View.VISIBLE);

                    if (old_password.equals(""))
                        tv_error_description.setText("Old password is required");
                    else if (new_password.equals(""))
                        tv_error_description.setText("New password is required");
                    else
                        tv_error_description.setText("Confirm password is required");

                } else if (!new_password.equals(confirm_password)) {
                    ll_error.setVisibility(View.VISIBLE);
                    tv_error_description.setText("New and confirmation pasword do not match");
                }else{
                    ll_error.setVisibility(View.INVISIBLE);
                    new EditPassword(old_password, new_password).execute();
                }
            }
        });

        ll_error.setVisibility(View.INVISIBLE);

        return V;
    }

    class EditPassword extends AsyncTask<Void, Void, Void> {

        protected ProgressDialog progressDialog;

        private String old_password = "";
        private String new_password = "";
        private boolean is_ok = false;


        public EditPassword(String old_password, String new_password) {
            this.old_password = old_password;
            this.new_password = new_password;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(main_tab_activity, "", "Editing Password ...", true, false);
        }

        @Override
        protected Void doInBackground(Void... params) {

            String access_token = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_ACCESS_TOKEN, "UNKNOWN");
            try {
                Map<String, Object> result = ThreadrAPIAdapter.doEditPassword(access_token, old_password, new_password);

                if (result != null && Boolean.parseBoolean(result.get("result").toString()))
                    is_ok = true;
            } catch (Exception e) {
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void ret) {
            progressDialog.dismiss();

            if (is_ok) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                LayoutInflater inflater = getActivity().getLayoutInflater();

                View add_contact_status_generic_view = inflater.inflate(R.layout.add_contact_status_generic_view, null);
                builder.setView(add_contact_status_generic_view);

                final AlertDialog dialog = builder.create();

                TextView tv_description = (TextView) add_contact_status_generic_view.findViewById(R.id.tv_description);

                tv_description.setText("Successfully changed password");

                TextView tv_continue = (TextView) add_contact_status_generic_view.findViewById(R.id.tv_continue);
                tv_continue.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        main_tab_activity.onBackPressed();
                    }
                });
                dialog.show();
            } else {

                Toast.makeText(main_tab_activity, "Unable to complete.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}