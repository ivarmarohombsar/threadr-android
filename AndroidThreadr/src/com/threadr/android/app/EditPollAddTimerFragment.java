package com.threadr.android.app;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;
import com.threadr.android.app.Utils.PreferenceHelper;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by lilandra on 2/23/14.
 */
public class EditPollAddTimerFragment extends BaseFragment {

    private static String TAG = "EditPollAddTimerFragment";

    private NumberPicker np_day;
    private NumberPicker np_hour;
    private NumberPicker np_minute;

    private boolean is_loaded = false;

    private Integer old_duration = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View V = inflater.inflate(R.layout.poll_add_timer_view, container, false);

        np_day = (NumberPicker) V.findViewById(R.id.np_day);
        np_hour = (NumberPicker) V.findViewById(R.id.np_hour);
        np_minute = (NumberPicker) V.findViewById(R.id.np_minute);

        np_day.setMinValue(0);
        np_hour.setMinValue(0);
        np_minute.setMinValue(0);

        np_day.setMaxValue(999);
        np_hour.setMaxValue(24);
        np_minute.setMaxValue(60);

//        Button bt_reset_day = (Button) V.findViewById(R.id.bt_reset_day);
//        bt_reset_day.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                np_day.setValue(0);
//            }
//        });

//        Button bt_reset_hour = (Button) V.findViewById(R.id.bt_reset_hour);
//        bt_reset_hour.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                np_hour.setValue(0);
//            }
//        });

        Button bt_reset_minute = (Button) V.findViewById(R.id.bt_reset_minute);
        bt_reset_minute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                np_day.setValue(0);
                np_hour.setValue(0);
                np_minute.setValue(0);
            }
        });

        Button bt_post = (Button) V.findViewById(R.id.bt_post);
        bt_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int total_seconds = 0;

                int set_days = np_day.getValue();
                int set_hours = np_hour.getValue();
                int set_minutes = np_minute.getValue();

                total_seconds = (set_days * 86400) + (set_hours * 3600) + (set_minutes * 60);

                if (total_seconds > 0) {

                    if(total_seconds == old_duration)
                        total_seconds = 0; // indicate no changes in timer

                    // update shared alert payload
                    SharedData.alert_ammendment_payload.duration = total_seconds;

                    new CreateAlert().execute();
                }
            }
        });

        return V;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!is_loaded) {
            is_loaded = true;
            new LoadData().execute();
        } else
            updateDisplay();
    }

    private void loadData() {

    }

    private void updateDisplay() {
        int totalseconds = 0;

        String to_date = SharedData.alert_ammendment_payload.old_alert.expiration_date;

        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));

            Date dFromDate = new Date();
            Date dToDate = formatter.parse(to_date);

            TimeUnit timeUnit = TimeUnit.SECONDS;

            long diffInMillies = dToDate.getTime() - dFromDate.getTime();
            long s = timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);

            long days = s / (24 * 60 * 60);
            long rest = s - (days * 24 * 60 * 60);
            long std = rest / (60 * 60);
            long rest1 = rest - (std * 60 * 60);
            long min = rest1 / 60;
            long sec = s % 60;

            if (days > 0)
                np_day.setValue(Integer.parseInt(Long.toString(days)));
            if (std > 0)
                np_hour.setValue(Integer.parseInt(Long.toString(std)));
            if (min > 0)
                np_minute.setValue(Integer.parseInt(Long.toString(min)));


            // calculate old duration

            int set_days = np_day.getValue();
            int set_hours = np_hour.getValue();
            int set_minutes = np_minute.getValue();

            old_duration = (set_days * 86400) + (set_hours * 3600) + (set_minutes * 60);


        } catch (Exception e) {

        }
    }

    private void createAlert() {

        try {

            String access_token = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_ACCESS_TOKEN, "UNKNOWN");
            // get recipients

            if (SharedData.alert_ammendment_payload.target_group_ids.size() > 0) {
                // we will get all contacts under the groups

                for (String group_id : SharedData.alert_ammendment_payload.target_group_ids) {

                    Map<String, Object> result = ThreadrAPIAdapter.doGetAllGroupContacts(access_token, group_id);
                    Object contacts = result.get("data");

                    for (Map<String, Object> contact : (ArrayList<Map<String, Object>>) contacts) {

                        String contact_id = contact.get("contactId").toString();

                        if (!SharedData.alert_ammendment_payload.target_contact_ids.contains(contact_id))
                            SharedData.alert_ammendment_payload.target_contact_ids.add(contact_id);
                    }
                }
            }

            // now we do the creation of the alert

            // CONTACTS SECTION


            // get contacts to add

            List<String> contacts_to_add = null;

            Set<String> new_ids_set = new HashSet<String>(SharedData.alert_ammendment_payload.target_contact_ids);
            Set<String> old_ids_set = new HashSet<String>();

            for (PollInfo.ContactInfo old_contact : SharedData.alert_ammendment_payload.old_alert.contacts)
                old_ids_set.add(old_contact.user_id);


            new_ids_set.removeAll(old_ids_set);  // this mutates the new_ids set to contain only new ids

            contacts_to_add = new ArrayList(new_ids_set);

            for (String id : contacts_to_add)
                Log.i(TAG + "- CONTACT TO ADD", id);


            // get contacts to delete

            List<String> contacts_to_remove = null;

            new_ids_set = new HashSet<String>(SharedData.alert_ammendment_payload.target_contact_ids);

            old_ids_set = new HashSet<String>();

            for (PollInfo.ContactInfo old_contact : SharedData.alert_ammendment_payload.old_alert.contacts)
                old_ids_set.add(old_contact.user_id);

            old_ids_set.removeAll(new_ids_set);

            contacts_to_remove = new ArrayList<String>(old_ids_set);

            for (String id : contacts_to_remove)
                Log.i(TAG + "- CONTACT TO REMOVE", id);


            // ASSET_SECTION

            // get assets to add

            List<PollInfo.AssetInfo> assets_to_add = null;

            Set<PollInfo.AssetInfo> new_asset_set = new HashSet<PollInfo.AssetInfo>(SharedData.alert_ammendment_payload.assets);
            Set<PollInfo.AssetInfo> old_asset_set = new HashSet<PollInfo.AssetInfo>();

            for (PollInfo.AssetInfo old_asset : SharedData.alert_ammendment_payload.old_alert.assets)
                old_asset_set.add(old_asset);

            new_asset_set.removeAll(old_asset_set);  // this mutates the new_ids set to contain only new ids

            assets_to_add = new ArrayList(new_asset_set);

            for (PollInfo.AssetInfo temp_asset : assets_to_add)
                Log.i(TAG + "- ASSET TO ADD", temp_asset.description);

            // get asset ids to remove

            List<String> assets_to_remove = new ArrayList<String>();

            new_asset_set = new HashSet<PollInfo.AssetInfo>(SharedData.alert_ammendment_payload.assets);
            old_asset_set = new HashSet<PollInfo.AssetInfo>();

            for (PollInfo.AssetInfo old_asset : SharedData.alert_ammendment_payload.old_alert.assets)
                old_asset_set.add(old_asset);

            old_asset_set.removeAll(new_asset_set);

            List<PollInfo.AssetInfo> old_assets = new ArrayList<PollInfo.AssetInfo>(old_asset_set);

            for (PollInfo.AssetInfo old_asset : SharedData.alert_ammendment_payload.old_alert.assets) {
                if (old_assets.contains(old_asset)) {
                    assets_to_remove.add(old_asset.id);
                }
            }

            for (String id : assets_to_remove)
                Log.i(TAG + "- ASSET TO REMOVE", id);


            // TIME STAMP

            String time_stamp = "";

            Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date now_date = new Date();
            time_stamp = formatter.format(now_date);


            String description = SharedData.alert_ammendment_payload.description;
            String alert_id = SharedData.alert_ammendment_payload.old_alert.id;
            int duration = SharedData.alert_ammendment_payload.duration;

            Log.i(TAG, alert_id);
            Log.i(TAG, description);
            Log.i(TAG, Integer.toString(duration));
            Log.i(TAG, time_stamp);

            String media_asset_temp_file = "";
            String old_media_url = SharedData.alert_ammendment_payload.old_alert.media.url;
            String new_media_url = SharedData.alert_ammendment_payload.media.url;
            String media_type = SharedData.alert_ammendment_payload.media.type;


            if (old_media_url.equals("") && new_media_url.equals("")) {
                // no previous and no current media selected
                new_media_url = null;
            } else if (new_media_url.equals(old_media_url)){
                // previous and current media are the same
                new_media_url = null;
            } else if(!new_media_url.equals(old_media_url)){
                if (!new_media_url.equals("")) {

                    int i = new_media_url.lastIndexOf('.');
                    String media_ext = new_media_url.substring(i + 1);

                    String unique_id = UUID.randomUUID().toString();

                    if (media_type.equals(AppConstants.ASSET_IMAGE))
                        media_asset_temp_file = String.format("%s_photo.%s", unique_id, media_ext);
                    else if (media_type.equals(AppConstants.ASSET_VIDEO))
                        media_asset_temp_file = String.format("%s_video.%s", unique_id, media_ext);
                    else if (media_type.equals(AppConstants.ASSET_AUDIO))
                        media_asset_temp_file = String.format("%s_audio.%s", unique_id, media_ext);
                }
            }


            Map<String, Object> edit_alert_result = ThreadrAPIAdapter.doEditAlert(getActivity().getApplicationContext(), access_token, alert_id, description, duration, time_stamp, contacts_to_add, contacts_to_remove, assets_to_remove, new_media_url, media_type, media_asset_temp_file);

            for (PollInfo.AssetInfo asset : assets_to_add) {

                String file_name = "";

                int i = asset.url.lastIndexOf('.');
                String fileExtension = asset.url.substring(i + 1);

                String unique_id = UUID.randomUUID().toString();

                if (asset.type.equals(AppConstants.ASSET_IMAGE))
                    file_name = String.format("%s_photo_%s.%s", alert_id, unique_id, fileExtension);
                else if (asset.type.equals(AppConstants.ASSET_VIDEO))
                    file_name = String.format("%s_video_%s.%s", alert_id, unique_id, fileExtension);
                else if (asset.type.equals(AppConstants.ASSET_AUDIO))
                    file_name = String.format("%s_audio_%s.%s", alert_id, unique_id, fileExtension);

                Log.i(TAG, asset.type);
                Log.i(TAG, file_name);

                Map<String, Object> add_asset_result = ThreadrAPIAdapter.doAddAsset(getActivity().getApplicationContext(),access_token, alert_id, asset.type, file_name, asset.url, asset.description);

                // add thumbnail if video
                /*
                if (asset.type.equals(AppConstants.ASSET_VIDEO)) {
                    try {
                        String remote_file = add_file_result.get("filename").toString();
                        int index = remote_file.lastIndexOf(".");
                        String remote_image = remote_file.substring(0, index) + "_thumbnail.png";
                        String temp_file_name = main_tab_activity.getFilesDir() + "/temp.png";
                        Map<String, Object> add_thumbnail_result = ThreadrAPIAdapter.doAddThumbnail(access_token, asset.url, remote_image, temp_file_name);
                    } catch (Exception e) {
                    }
                }
                */
            }

            SharedData.alert_ammendment_payload.resetData();

        } catch (Exception e) {
            //Toast.makeText(getActivity(), "Unable to complete request", Toast.LENGTH_SHORT).show();
            Log.i(TAG + " editAlert", e.toString());
        }


    }

    class CreateAlert extends AsyncTask<Void, Void, Void> {

        protected ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(main_tab_activity, "", "Uploading ...", true, false);
        }

        @Override
        protected Void doInBackground(Void... params) {
            EditPollAddTimerFragment.this.createAlert();
            return null;
        }

        @Override
        protected void onPostExecute(Void ret) {
            progressDialog.dismiss();
            main_tab_activity.returnToFirstScreen();
        }
    }

    class LoadData extends AsyncTask<Void, Void, Void> {

        protected ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(main_tab_activity, "", "Loading Data", true, false);
        }

        @Override
        protected Void doInBackground(Void... params) {
            EditPollAddTimerFragment.this.loadData();
            return null;
        }

        @Override
        protected void onPostExecute(Void ret) {
            EditPollAddTimerFragment.this.updateDisplay();
            progressDialog.dismiss();
        }
    }
}
