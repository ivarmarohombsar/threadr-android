package com.threadr.android.app;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.threadr.android.app.Utils.PreferenceHelper;

import java.util.*;

/**
 * Created by lilandra on 1/23/14.
 */
public class EditWhoToAskFragment extends BaseFragment {
    private static final String TAG = "EditWhoToAskFragment";

    private final List<WhoToAskContactGroupInfo> contact_groups = new ArrayList<WhoToAskContactGroupInfo>();
    private final List<WhoToAskContactGroupInfo> temp_contact_groups = new ArrayList<WhoToAskContactGroupInfo>();

    private final List<WhoToAskContactInfo> contact_contacts = new ArrayList<WhoToAskContactInfo>();
    private final List<WhoToAskContactInfo> temp_contact_contacts = new ArrayList<WhoToAskContactInfo>();

    public static LayoutInflater inflater;

    private LinearLayout ll_groups;
    private LinearLayout ll_contacts;

    private EditText et_search;

    private boolean is_loaded = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.inflater = inflater;

        // Inflate the layout for this fragment
        View V = inflater.inflate(R.layout.who_to_ask_view, container, false);

        ll_groups = (LinearLayout) V.findViewById(R.id.ll_groups);
        ll_contacts = (LinearLayout) V.findViewById(R.id.ll_contacts);

        // setup others

        Button bt_next = (Button) V.findViewById(R.id.bt_next);
        bt_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedData.alert_ammendment_payload.resetTargetGroupIds();
                SharedData.alert_ammendment_payload.resetTargetIContactIds();

                // get all selected contact group recipient
                for (WhoToAskContactGroupInfo contact_group : contact_groups) {
                    if (contact_group.is_selected) {
                        SharedData.alert_ammendment_payload.target_group_ids.add(contact_group.id);
                        Log.i(TAG, contact_group.id);
                    }
                }

                // get all selected contact recipient
                for (WhoToAskContactInfo contact : contact_contacts) {
                    if (contact.is_selected) {
                        SharedData.alert_ammendment_payload.target_contact_ids.add(contact.id);
                        Log.i(TAG, contact.id);
                    }
                }

                if (SharedData.alert_ammendment_payload.target_group_ids.size() == 0 && SharedData.alert_ammendment_payload.target_contact_ids.size() == 0) {
                    Toast.makeText(getActivity(), "No contacts selected.", Toast.LENGTH_SHORT).show();
                } else {
                    EditPollFragment fragment = new EditPollFragment();

                    main_tab_activity.pushFragments(AppConstants.TAB_POLLS, fragment, true, true);
                }
            }
        });


        et_search = (EditText) V.findViewById(R.id.et_search);

        et_search.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String charText = s.toString().toLowerCase();

                ll_contacts.removeAllViews();

                for (final WhoToAskContactInfo contact : contact_contacts) {

                    boolean ok_to_add = false;

                    String full_name = contact.first_name + " " + contact.last_name;

                    if (charText.length() == 0 || full_name.toLowerCase().contains(charText))
                        ok_to_add = true;

                    if (ok_to_add) {

                        View v = EditWhoToAskFragment.inflater.inflate(R.layout.poll_share_add_contact_list_item, ll_groups, false);

                        TextView tv_contact_name = (TextView) v.findViewById(R.id.tv_contact_name);
                        CheckBox cb_selected = (CheckBox) v.findViewById(R.id.cb_selected);

                        // set data to display
                        tv_contact_name.setText(full_name);
                        cb_selected.setChecked(contact.is_selected);

                        // set listener
                        cb_selected.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                contact.is_selected = ((CheckBox) v).isChecked();
                            }
                        });

                        ll_contacts.addView(v);
                    }
                }
            }

        });

        return V;
    }

    @Override
    public void onResume() {
        super.onResume();

        et_search.setText("");

        /*
        if (!is_loaded) {

            Log.i(TAG, "FIRED");

            is_loaded = true;

            contact_groups.clear();
            contact_contacts.clear();

            new LoadData().execute();
        } else
            updateDisplay();

        if (contact_groups.size() > 0) {
            temp_contact_groups.clear();
            for (WhoToAskContactGroupInfo temp_group : contact_groups)
                temp_contact_groups.add(temp_group);
        }

        if (contact_contacts.size() > 0) {
            temp_contact_contacts.clear();
            for (WhoToAskContactInfo temp_contact : contact_contacts)
                temp_contact_contacts.add(temp_contact);
        }
        */

        if (contact_groups.size() > 0) {
            temp_contact_groups.clear();
            for (WhoToAskContactGroupInfo temp_group : contact_groups)
                temp_contact_groups.add(temp_group);
        }

        if (contact_contacts.size() > 0) {
            temp_contact_contacts.clear();
            for (WhoToAskContactInfo temp_contact : contact_contacts)
                temp_contact_contacts.add(temp_contact);
        }

        contact_groups.clear();
        contact_contacts.clear();

        new LoadData().execute();
    }

    private void loadData() {

        String access_token = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_ACCESS_TOKEN, "UNKNOWN");

        try {

            // retrieve group data
            Map<String, Object> group_result = null;

            try {
                group_result = ThreadrAPIAdapter.doGetUserGroups(access_token);
            } catch (Exception e) {
            }

            if (group_result != null) {

                try {
                    Object groups_data = group_result.get("data");

                    for (Map<String, Object> group : (ArrayList<Map<String, Object>>) groups_data) {

                        String group_id = group.get("groupId").toString();
                        String group_name = group.get("groupName").toString();

                        Log.i(TAG, group_name);

                        WhoToAskContactGroupInfo temp_group = new WhoToAskContactGroupInfo(group_name, group_id, false);
                        contact_groups.add(temp_group);
                    }
                } catch (Exception e) {
                }
            }

            // sort the contact group names
            HashMap<String, WhoToAskContactGroupInfo> temp_map = new HashMap<String, WhoToAskContactGroupInfo>();

            int group_seed = 0;
            for (WhoToAskContactGroupInfo group : contact_groups) {

                // we make sure to add a seed number to enforce order for contact groups
                group_seed += 1;

                String name = group.name + Integer.toString(group_seed);
                temp_map.put(name.toLowerCase().trim(), group);
            }

            SortedSet<String> keys = new TreeSet<String>(temp_map.keySet());

            contact_groups.clear();

            for (String key : keys) {
                WhoToAskContactGroupInfo contact_group = temp_map.get(key);
                contact_groups.add(contact_group);
            }


            // retrieve contacts data
            HashMap<String, WhoToAskContactInfo> temp_contacts = new HashMap<String, WhoToAskContactInfo>();
            // we exploit the fact that all users belong to All Contacts group
            Map<String, Object> all_contacts_result = null;

            try {
                all_contacts_result = ThreadrAPIAdapter.doGetAllGroupContacts(access_token, AppConstants.ALL_CONTACTS_GROUP_ID);
            } catch (Exception e) {
            }

            if (all_contacts_result != null) {

                try {
                    Object all_contacts_data = all_contacts_result.get("data");

                    int seed = 0;

                    for (Map<String, Object> contact : (ArrayList<Map<String, Object>>) all_contacts_data) {

                        String contact_id = contact.get("contactId").toString();
                        String first_name = contact.get("firstName").toString();
                        String last_name = contact.get("lastName").toString();

                        // we make sure to add a seed number to enforce order for contacts with same first and last_name
                        seed += 1;

                        String key = first_name + " " + last_name + Integer.toString(seed);

                        WhoToAskContactInfo temp_group_contact = new WhoToAskContactInfo(first_name, last_name, contact_id, false);
                        temp_contacts.put(key.toLowerCase().trim(), temp_group_contact);
                    }
                } catch (Exception e) {
                }
            }

            SortedSet<String> sorted_keys = new TreeSet<String>(temp_contacts.keySet());

            for (String key : sorted_keys) {

                WhoToAskContactInfo contact = temp_contacts.get(key);

                contact_contacts.add(contact);
            }

        } catch (Exception e) {
        }
    }

    private void updateDisplay() {

        // update groups

        ll_groups.removeAllViews();

        for (final WhoToAskContactGroupInfo group : contact_groups) {

            View v = inflater.inflate(R.layout.poll_share_add_contact_group_list_item, ll_groups, false);

            TextView tv_contact_name = (TextView) v.findViewById(R.id.tv_contact_group_name);
            ToggleButton tb_selected = (ToggleButton) v.findViewById(R.id.tb_selected);

            // set data to display

            tv_contact_name.setText(group.name);

            for (WhoToAskContactGroupInfo temp_group : temp_contact_groups) {
                if (temp_group.id.equals(group.id)) {
                    group.is_selected = temp_group.is_selected;
                    break;
                }
            }

            tb_selected.setChecked(group.is_selected);

            // set listener
            tb_selected.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    group.is_selected = ((ToggleButton) v).isChecked();
                }
            });

            ll_groups.addView(v);
        }

        // update contacts

        ll_contacts.removeAllViews();

        for (final WhoToAskContactInfo contact : contact_contacts) {

            //final WhoToAskContactInfo contact = hash_contacts.get(key);

            View v = inflater.inflate(R.layout.poll_share_add_contact_list_item, ll_groups, false);

            TextView tv_contact_name = (TextView) v.findViewById(R.id.tv_contact_name);
            CheckBox cb_selected = (CheckBox) v.findViewById(R.id.cb_selected);

            // set data to display
            String full_name = contact.first_name + " " + contact.last_name;
            tv_contact_name.setText(full_name);

            // update to selected this contact if it was a recipient of the alert
            for (PollInfo.ContactInfo temp_contact : SharedData.alert_ammendment_payload.old_alert.contacts) {
                if (temp_contact.user_id.equals(contact.id)) {
                    // if contact is a recipient of the alert, set toggle to ON
                    contact.is_selected = true;
                    break;
                }
            }

            // update this recipient with recent selection, if it previously existed
            for (WhoToAskContactInfo temp_contact : temp_contact_contacts) {
                if (temp_contact.id.equals(contact.id)) {
                    contact.is_selected = temp_contact.is_selected;
                    break;
                }
            }


            cb_selected.setChecked(contact.is_selected);
            //cb_selected.setTag(key);

            // set listener
            cb_selected.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    contact.is_selected = ((CheckBox) v).isChecked();
                }
            });

            ll_contacts.addView(v);
        }
    }

    class LoadData extends AsyncTask<Void, Void, Void> {

        protected ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(main_tab_activity, "", "Loading Data", true, false);
        }

        @Override
        protected Void doInBackground(Void... params) {
            EditWhoToAskFragment.this.loadData();
            return null;
        }

        @Override
        protected void onPostExecute(Void ret) {
            EditWhoToAskFragment.this.updateDisplay();
            progressDialog.dismiss();
        }
    }
}
