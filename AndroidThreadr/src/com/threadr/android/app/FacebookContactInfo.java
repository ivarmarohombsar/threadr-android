package com.threadr.android.app;

/**
 * Created by lilandra on 2/28/14.
 */
public class FacebookContactInfo {
    public String user_id;
    public String first_name;
    public String last_name;
    public String phone_number;
    public String email;
    public boolean is_selected = false;
}
