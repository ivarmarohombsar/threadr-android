package com.threadr.android.app;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;

/**
 * Created by lilandra on 2/28/14.
 */
public class FacebookContactListViewAdapter extends BaseAdapter {

    private String TAG = "FacebookContactListViewAdapter";

    private final Context mContext;
    private final List<FacebookContactInfo> facebook_contacts;

    public FacebookContactListViewAdapter(Context c, List contacts) {
        mContext = c;
        facebook_contacts = contacts;
    }

    @Override
    public int getCount() {
        return facebook_contacts.size();
    }

    @Override
    public Object getItem(int position) {
        return facebook_contacts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View itemView, ViewGroup parent) {
        // reference to convertView
        View v = itemView;

        final int contact_index = position;

        // inflate new layout if null
        if (v == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            v = inflater.inflate(R.layout.contact_facebook_list_item, parent, false);
        }

        if (facebook_contacts != null && !facebook_contacts.isEmpty()) {
            // get the selected entry
            final FacebookContactInfo entry = facebook_contacts.get(position);

            // load controls from layout resources

            TextView tv_contact_name = (TextView) v.findViewById(R.id.tv_contact_name);
            CheckBox cb_selected = (CheckBox) v.findViewById(R.id.cb_selected);

            // set data to display
            String full_name = entry.first_name + " " + entry.last_name;
            tv_contact_name.setText(full_name);

            tv_contact_name.setText(full_name);
            cb_selected.setChecked(entry.is_selected);
            cb_selected.setTag(entry.user_id);

            // set listener

            cb_selected.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean is_selected = ((CheckBox) v).isChecked();

                    //FacebookContactInfo temp_contact = facebook_contacts.get(contact_index);
                    //temp_contact.is_selected = is_selected;
                    entry.is_selected = is_selected;
                }
            });
        }

        // return view
        return v;
    }
}

