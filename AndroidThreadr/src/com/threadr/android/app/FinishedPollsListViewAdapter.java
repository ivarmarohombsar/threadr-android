package com.threadr.android.app;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.ArcShape;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by lilandra on 2/19/14.
 */
public class FinishedPollsListViewAdapter extends BaseAdapter {

    private String TAG = "FinishedPollsListViewAdapter";

    private Context mContext;
    private List mListAlertInfo;

    public FinishedPollsListViewAdapter(Context c, List list) {
        mContext = c;
        mListAlertInfo = list;
    }

    @Override
    public int getCount() {
        return mListAlertInfo.size();
    }

    @Override
    public Object getItem(int position) {
        return mListAlertInfo.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    static class ViewHolder {
        public ImageView iv_sender;
        public TextView tv_sender_name;
        public TextView tv_description;
        public TextView tv_posting_date;
        public TextView tv_location_name;
        public TextView tv_short_remaining_time;
        public TextView tv_result_label;
        public TextView tv_comments;
        public TextView tv_recipients;
        public LinearLayout ll_assets;

        public ImageView arcImageView;
        public ImageView grayArcImageView;
        public ImageView innerWhiteImageView;
    }

    @Override
    public View getView(int position, View itemView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(mContext);

        View rowView = itemView;

        if (rowView == null) {

            rowView = inflater.inflate(R.layout.finished_poll_list_item, parent, false);

            ViewHolder viewHolder = new ViewHolder();

            viewHolder.iv_sender = (ImageView) rowView.findViewById(R.id.sender_icon);
            viewHolder.tv_sender_name = (TextView) rowView.findViewById(R.id.tv_sender_name);
            viewHolder.tv_description = (TextView) rowView.findViewById(R.id.tv_description);
            viewHolder.tv_posting_date = (TextView) rowView.findViewById(R.id.tv_posting_date);
            viewHolder.tv_location_name = (TextView) rowView.findViewById(R.id.tv_location_name);
            viewHolder.tv_short_remaining_time = (TextView) rowView.findViewById(R.id.tv_short_remaining_time);
            viewHolder.tv_result_label = (TextView) rowView.findViewById(R.id.tv_result_label);
            viewHolder.tv_comments = (TextView) rowView.findViewById(R.id.tv_comments);
            viewHolder.tv_recipients = (TextView) rowView.findViewById(R.id.tv_recipients);
            viewHolder.ll_assets = (LinearLayout) rowView.findViewById(R.id.ll_assets);

            viewHolder.arcImageView = (ImageView) rowView.findViewById(R.id.arcImageView);
            viewHolder.grayArcImageView = (ImageView) rowView.findViewById(R.id.grayArcImageView);
            viewHolder.innerWhiteImageView = (ImageView) rowView.findViewById(R.id.innerWhiteImageView);

            rowView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder) rowView.getTag();

        // get the selected entry
        PollInfo entry = (PollInfo) mListAlertInfo.get(position);

        // set data to display

//        ShapeDrawable sDrawable = new ShapeDrawable(new ArcShape(270, 90));
//        sDrawable.getPaint().setColor(Color.RED);
//        sDrawable.setIntrinsicHeight(100);
//        sDrawable.setIntrinsicWidth(100);
//        holder.arcImageView.setImageDrawable(sDrawable);

        float getTimeRemainingPercentage = getTimeRemainingPercentage(entry.expiration_date, entry.post_date);
//        Log.i("Arthur", "getTimeRemainingPercentage = "+ getTimeRemainingPercentage);

        float angle = (1 - getTimeRemainingPercentage) * 360;

        ShapeDrawable grayDrawable = new ShapeDrawable(new ArcShape(270, angle));
        grayDrawable.getPaint().setColor(Color.LTGRAY);
        grayDrawable.setIntrinsicHeight(100);
        grayDrawable.setIntrinsicWidth(100);
        holder.grayArcImageView.setImageDrawable(grayDrawable);

        ShapeDrawable whiteDrawable = new ShapeDrawable(new ArcShape(270, 360));
        whiteDrawable.getPaint().setColor(Color.WHITE);
        whiteDrawable.setIntrinsicHeight(100);
        whiteDrawable.setIntrinsicWidth(100);
        holder.innerWhiteImageView.setImageDrawable(whiteDrawable);

        if (!entry.sender_image_url.equals(""))
            Picasso.with(mContext).load(entry.sender_image_url).fit().into(holder.iv_sender);
        else
            Picasso.with(mContext).load(R.drawable.silhouette).fit().into(holder.iv_sender);

        holder.ll_assets.removeAllViews();

        // we need only winners

        HashMap<Double, List<PollInfo.AssetInfo>> temp_map_assets = new HashMap<Double, List<PollInfo.AssetInfo>>();

        int pax_voted = 0;

        for (PollInfo.AssetInfo asset : entry.assets) {
            pax_voted += asset.count;
        }


        for (PollInfo.AssetInfo asset : entry.assets) {
            double count_percentage = 0.0;
            if (asset.count != 0)
                count_percentage = (asset.count / (pax_voted * 1.0)) * 100;

            if (temp_map_assets.containsKey(count_percentage))
                temp_map_assets.get(count_percentage).add(asset);
            else {
                temp_map_assets.put(count_percentage, new ArrayList<PollInfo.AssetInfo>());
                temp_map_assets.get(count_percentage).add(asset);
            }
        }

        List<Double> asset_percentages = new ArrayList(temp_map_assets.keySet());
        Collections.sort(asset_percentages);
        Collections.reverse(asset_percentages);

        List<PollInfo.AssetInfo> temp_assets = new LinkedList<PollInfo.AssetInfo>();

        double highest_pct = 0.0;
        if (asset_percentages.size() > 0)
            highest_pct = asset_percentages.get(0);

        if (highest_pct > 0.0) {
            for (Double key : asset_percentages) {
                if (key == highest_pct)
                    temp_assets.addAll(temp_map_assets.get(key));
                else
                    break;
            }
        }

        switch (temp_assets.size()) {
            case 0: {
                holder.tv_result_label.setText("No winner");
            }
            break;
            case 1: {
                holder.tv_result_label.setText("Solo winner");
            }
            break;
            case 2: {
                holder.tv_result_label.setText("Winners (tie):");
            }
            break;
            default: {
                holder.tv_result_label.setText("Winners:");
            }
        }

        // display the winners
        for (PollInfo.AssetInfo asset : temp_assets) {
            View asset_entry_view;

            TextView tv_percentage = null;

            if (!asset.type.equals(AppConstants.ASSET_TEXT)) {

                if (asset.type.equals(AppConstants.ASSET_VIDEO))
                    asset_entry_view = inflater.inflate(R.layout.finished_poll_list_asset_item_video, holder.ll_assets, false);
                else
                    asset_entry_view = inflater.inflate(R.layout.finished_poll_list_asset_item_audio_or_image, holder.ll_assets, false);

                View asset_view = asset_entry_view.findViewById(R.id.inc_layout);

                tv_percentage = (TextView) asset_view.findViewById(R.id.tv_percentage);
                ImageView iv_asset = (ImageView) asset_view.findViewById(R.id.iv_asset);

                if (asset.type.equals(AppConstants.ASSET_IMAGE))
                    Picasso.with(mContext).load(asset.url).fit().into(iv_asset);
                else if (asset.type.equals(AppConstants.ASSET_VIDEO)) {
                    int i = asset.url.lastIndexOf(".");
                    String remote_image = asset.url.substring(0, i) + "_thumbnail.jpg";
                    Picasso.with(mContext).load(remote_image).error(R.drawable.with_video).fit().into(iv_asset);
                } else if (asset.type.equals(AppConstants.ASSET_AUDIO))
                    Picasso.with(mContext).load(R.drawable.with_audio).fit().into(iv_asset);
            } else {
                asset_entry_view = inflater.inflate(R.layout.finished_poll_list_asset_item_text, holder.ll_assets, false);

                View asset_view = asset_entry_view.findViewById(R.id.inc_layout);

                tv_percentage = (TextView) asset_view.findViewById(R.id.tv_percentage);
                TextView tv_asset = (TextView) asset_view.findViewById(R.id.tv_asset);

                tv_asset.setText(asset.description);
            }

            if (tv_percentage != null) {
                double count_percentage = 0.0;
                if (asset.count != 0)
                    count_percentage = (asset.count / (pax_voted * 1.0)) * 100;
                String str_count_percentage = String.format("%.2f", count_percentage) + "%";
                tv_percentage.setText(str_count_percentage);
            }

            TextView tv_description = (TextView) asset_entry_view.findViewById(R.id.tv_description);
            tv_description.setText(asset.description);

            holder.ll_assets.addView(asset_entry_view);
        }

        String full_name = entry.sender_first_name + " " + entry.sender_last_name;
        holder.tv_sender_name.setText(full_name);

        holder.tv_posting_date.setText("Posted on " + convertISODate(entry.post_date));
        holder.tv_location_name.setText("From: " + entry.location_name);
        //holder.tv_short_remaining_time.setText(getShortTimeDifference(entry.expiration_date));
        holder.tv_description.setText(entry.description);

        holder.tv_comments.setText(String.valueOf(entry.comments_count) + " comments");

        String recipients = "";

        int num_comments = entry.contacts.size();

        if (num_comments == 1)
            recipients = "With " + entry.contacts.get(0).first_name;
        else if (num_comments == 2)
            recipients = "With " + entry.contacts.get(0).first_name + " & " + entry.contacts.get(1).first_name;
        else if (num_comments > 2)
            recipients = "With " + entry.contacts.get(0).first_name + "," + entry.contacts.get(1).first_name + " + " + String.valueOf(num_comments - 2) + " others";

        holder.tv_recipients.setText(recipients);

        // return view
        return rowView;
    }

    public String convertISODate(String iso_date) {
        String result = "";

        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date temp_date = formatter.parse(iso_date);

            Calendar cal = Calendar.getInstance();
            cal.setTime(temp_date);

            Locale current_locale = mContext.getResources().getConfiguration().locale;

            String month_name = cal.getDisplayName(Calendar.MONTH, Calendar.SHORT, current_locale);
            String day = Integer.toString(cal.get(Calendar.DAY_OF_MONTH));
            String year = Integer.toString(cal.get(Calendar.YEAR));

            result = month_name + " " + day + ", " + year;
        } catch (Exception e) {
        }

        return result;
    }

    public String getDetailedTimeDifference(String iso_date) {
        String result = "";

        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));

            Date future_date = formatter.parse(iso_date);
            Date now_date = new Date();

            TimeUnit timeUnit = TimeUnit.SECONDS;

            long diffInMillies = future_date.getTime() - now_date.getTime();
            long s = timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);

            long days = s / (24 * 60 * 60);
            long rest = s - (days * 24 * 60 * 60);
            long std = rest / (60 * 60);
            long rest1 = rest - (std * 60 * 60);
            long min = rest1 / 60;
            long sec = s % 60;

            if (days > 0)
                result += days + " days ";
            if (std > 0)
                result += Long.toString(std) + " hours ";
            if (min > 0)
                result += Long.toString(min) + " minutes";

        } catch (Exception e) {
        }

        return result;
    }

    public String getShortTimeDifference(String iso_date) {
        String result = "";

        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));

            Date future_date = formatter.parse(iso_date);
            Date now_date = new Date();

            TimeUnit timeUnit = TimeUnit.SECONDS;

            long diffInMillies = future_date.getTime() - now_date.getTime();
            long s = timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);

            long days = s / (24 * 60 * 60);
            long rest = s - (days * 24 * 60 * 60);
            long std = rest / (60 * 60);
            long rest1 = rest - (std * 60 * 60);
            long min = rest1 / 60;
            long sec = s % 60;

            if (days > 0)
                result += Long.toString(days) + "d";
            else if (std > 0)
                result += Long.toString(std) + "h";
            else if (min > 0)
                result += Long.toString(min) + "m";

        } catch (Exception e) {
        }

        return result;
    }

    public float getTimeRemainingPercentage(String endtDate, String creationDate) {
        float result = 100.00f;

        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));

            Date end_date = formatter.parse(endtDate);
            Date posted_date = formatter.parse(creationDate);
            Date now_date = new Date();


            TimeUnit timeUnit = TimeUnit.SECONDS;

            long totalDurationMillis = end_date.getTime() - posted_date.getTime();
            long totalSeconds = timeUnit.convert(totalDurationMillis, TimeUnit.MILLISECONDS);

            long diffInMillies = end_date.getTime() - now_date.getTime();
            long currentSeconds = timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);

            Log.i("Arthur", "totalSeconds = " + totalSeconds + " currentSeconds = " + currentSeconds);

            if (currentSeconds < totalDurationMillis) {
                float remainingSecs = (float) currentSeconds / (float) totalSeconds;
                return remainingSecs;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
}


