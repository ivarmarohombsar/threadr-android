package com.threadr.android.app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Map;

/**
 * Created by lilandra on 1/3/14.
 */
public class ForgotPasswordActivity extends Activity {

    private static final String TAG = "ForgotPasswordActivity";

    private EditText et_email;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password_view);

        et_email = (EditText) findViewById(R.id.et_email);

        Button bt_submit = (Button) findViewById(R.id.bt_submit);
        bt_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // hide keyboard
                InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                final String email = et_email.getText().toString().trim();

                new ForgotPassword(email).execute();
            }
        });

        Button bt_cancel = (Button) findViewById(R.id.bt_cancel);
        bt_cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    class ForgotPassword extends AsyncTask<Void, Void, Void> {

        protected ProgressDialog progressDialog;

        private String email = "";

        private boolean is_ok = false;

        public ForgotPassword(String email) {
            this.email = email;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(ForgotPasswordActivity.this, "", "Sending forgot password request ...", true, false);
        }

        @Override
        protected Void doInBackground(Void... params) {

            Map<String, Object> result = ThreadrAPIAdapter.doForgotPassword(email);

            if (result != null && Boolean.parseBoolean(result.get("result").toString()))
                is_ok = true;

            return null;
        }

        @Override
        protected void onPostExecute(Void ret) {
            progressDialog.dismiss();

            if (is_ok)
                finish();
            else
                Toast.makeText(ForgotPasswordActivity.this, "Unable to complete.", Toast.LENGTH_SHORT).show();
        }
    }

}