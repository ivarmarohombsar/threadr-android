package com.threadr.android.app;

import android.graphics.Bitmap;

/**
 * Created by lilandra on 3/21/14.
 */
public class GalleryItem {
    public String path = "";
    public String thumbnail_path = "";
    public String type = "";
    public boolean is_selected = false;

}
