package com.threadr.android.app;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by lilandra on 3/21/14.
 */
public class GalleryPickerActivity extends Activity {

    private static final String TAG = "GalleryPickerActivity";

    private GridView gv_media;
    private TextView tv_title;
    private List<GalleryItem> media = new ArrayList<GalleryItem>();
    private GalleryPickerGridViewAdapter adapter;

    private boolean is_loaded = false;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gallery_picker_view);

        tv_title = (TextView) findViewById(R.id.tv_title);
        gv_media = (GridView) findViewById(R.id.gv_media);

        adapter = new GalleryPickerGridViewAdapter(this, media);
        gv_media.setAdapter(adapter);

        Button bt_add = (Button) findViewById(R.id.bt_add);
        bt_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();

                ArrayList<String> assets = new ArrayList<String>();

                for (GalleryItem item : media) {
                    if (item.is_selected)
                        assets.add(item.path);
                }

                if (assets.size() > 0) {
                    returnIntent.putStringArrayListExtra("paths", assets);
                    setResult(RESULT_OK, returnIntent);
                } else {
                    setResult(RESULT_CANCELED, returnIntent);
                }

                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!is_loaded) {
            is_loaded = true;
            media.clear();

            Bundle extras = getIntent().getExtras();
            if (!extras.isEmpty()) {

                String gallery_mode = extras.getString(AppConstants.GALLERY_MODE);

                if (gallery_mode.equals(AppConstants.ASSET_IMAGE)) {
                    media.addAll(getGalleryPhotos());
                    tv_title.setText("Select Images");
                } else if (gallery_mode.equals(AppConstants.ASSET_VIDEO)) {
                    media.addAll(getGalleryVideos());
                    tv_title.setText("Select Videos");
                }

                adapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        setResult(RESULT_CANCELED, returnIntent);
        finish();
    }

    private ArrayList<GalleryItem> getGalleryPhotos() {
        ArrayList<GalleryItem> galleryList = new ArrayList<GalleryItem>();

        try {
            final String[] columns = {MediaStore.Images.Media.DATA, MediaStore.Images.Media._ID};
            final String orderBy = MediaStore.Images.Media._ID;

            Cursor internal_cursor = getContentResolver().query(MediaStore.Images.Media.INTERNAL_CONTENT_URI, columns, null, null, orderBy);
            if (internal_cursor != null && internal_cursor.getCount() > 0) {

                while (internal_cursor.moveToNext()) {
                    GalleryItem item = new GalleryItem();

                    int dataColumnIndex = internal_cursor.getColumnIndex(MediaStore.Images.Media.DATA);

                    item.path = internal_cursor.getString(dataColumnIndex);
                    item.type = AppConstants.ASSET_IMAGE;
                    galleryList.add(item);
                }
            }

            Cursor external_cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns, null, null, orderBy);
            if (external_cursor != null && external_cursor.getCount() > 0) {

                while (external_cursor.moveToNext()) {
                    GalleryItem item = new GalleryItem();

                    int dataColumnIndex = external_cursor.getColumnIndex(MediaStore.Images.Media.DATA);

                    item.path = external_cursor.getString(dataColumnIndex);
                    item.type = AppConstants.ASSET_IMAGE;
                    galleryList.add(item);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        // show newest photo at beginning of the list
        Collections.reverse(galleryList);
        return galleryList;
    }

    private ArrayList<GalleryItem> getGalleryVideos() {
        ArrayList<GalleryItem> galleryList = new ArrayList<GalleryItem>();

        try {

            final String[] columns = {MediaStore.Video.Media.DATA, MediaStore.Video.Media._ID};
            final String orderBy = MediaStore.Video.Media._ID;

            Cursor internal_cursor = getContentResolver().query(MediaStore.Video.Media.INTERNAL_CONTENT_URI, columns, null, null, orderBy);
            if (internal_cursor != null && internal_cursor.getCount() > 0) {

                while (internal_cursor.moveToNext()) {
                    GalleryItem item = new GalleryItem();

                    int dataColumnIndex = internal_cursor.getColumnIndex(MediaStore.Video.Media.DATA);

                    item.path = internal_cursor.getString(dataColumnIndex);
                    item.type = AppConstants.ASSET_VIDEO;

                    // now we get the thumbnail associated with this video

                    String video_id = internal_cursor.getString(internal_cursor.getColumnIndex(MediaStore.Video.Media._ID));

                    String[] projection = {MediaStore.Video.VideoColumns.DATA, MediaStore.Video.Thumbnails.VIDEO_ID};

                    Cursor cursor = getContentResolver().query(MediaStore.Video.Thumbnails.INTERNAL_CONTENT_URI, projection, MediaStore.Video.Thumbnails.VIDEO_ID + "=?", new String[]{video_id}, null);

                    while (cursor.moveToNext()) {

                        int thumbnail_index = cursor.getColumnIndex(MediaStore.Video.VideoColumns.DATA);

                        String thumbnail_path = cursor.getString(thumbnail_index);

                        item.thumbnail_path = thumbnail_path;
                    }

                    cursor.close();

                    galleryList.add(item);
                }
            }

            internal_cursor.close();


            Cursor external_cursor = getContentResolver().query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, columns, null, null, orderBy);
            if (external_cursor != null && external_cursor.getCount() > 0) {

                while (external_cursor.moveToNext()) {
                    GalleryItem item = new GalleryItem();

                    int dataColumnIndex = external_cursor.getColumnIndex(MediaStore.Video.Media.DATA);

                    item.path = external_cursor.getString(dataColumnIndex);
                    item.type = AppConstants.ASSET_VIDEO;

                    // now we get the thumbnail associated with this video

                    String video_id = external_cursor.getString(external_cursor.getColumnIndex(MediaStore.Video.Media._ID));

                    String[] projection = {MediaStore.Video.VideoColumns.DATA, MediaStore.Video.Thumbnails.VIDEO_ID};

                    Cursor cursor = getContentResolver().query(MediaStore.Video.Thumbnails.EXTERNAL_CONTENT_URI, projection, MediaStore.Video.Thumbnails.VIDEO_ID + "=?", new String[]{video_id}, null);

                    while (cursor.moveToNext()) {

                        int thumbnail_index = cursor.getColumnIndex(MediaStore.Video.VideoColumns.DATA);

                        String thumbnail_path = cursor.getString(thumbnail_index);

                        item.thumbnail_path = thumbnail_path;
                    }

                    cursor.close();

                    galleryList.add(item);
                }
            }

            external_cursor.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        // show newest video at beginning of the list
        Collections.reverse(galleryList);
        return galleryList;
    }
}