package com.threadr.android.app;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.List;

/**
 * Created by lilandra on 3/21/14.
 */
public class GalleryPickerGridViewAdapter extends BaseAdapter {

    private final String TAG = "GalleryPickerGridViewAdapter";

    private final Context mContext;
    private final List<GalleryItem> media;

    public GalleryPickerGridViewAdapter(Context c, List list) {
        mContext = c;
        media = list;
    }

    @Override
    public int getCount() {
        return media.size();
    }

    @Override
    public Object getItem(int position) {
        return media.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View itemView, ViewGroup parent) {
        // reference to convertView
        View v = itemView;

        // inflate new layout if null
        if (v == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            v = inflater.inflate(R.layout.gallery_picker_item, parent, false);
        }


        if (media != null && !media.isEmpty()) {
            // get the selected entry
            final GalleryItem entry = media.get(position);

            Log.i(TAG, entry.path);
            Log.i(TAG, entry.type);

            ImageView iv_asset = (ImageView) v.findViewById(R.id.iv_asset);

            CheckBox cb = (CheckBox) v.findViewById(R.id.cb_selected);
            cb.setChecked(entry.is_selected);

            cb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean is_checked = ((CheckBox) v).isChecked();
                    entry.is_selected = is_checked;
                }
            });

            if (entry.type.equals(AppConstants.ASSET_IMAGE))
                Picasso.with(mContext).load(new File(entry.path)).fit().centerCrop().into(iv_asset);
            else if (entry.type.equals(AppConstants.ASSET_VIDEO)) {
                if (!entry.thumbnail_path.equals(""))
                    Picasso.with(mContext).load(new File(entry.thumbnail_path)).fit().centerCrop().into(iv_asset);
                else
                    new LoadImage(entry.path, iv_asset).execute();
            }
        }

        return v;
    }

    class LoadImage extends AsyncTask<Void, Void, Void> {

        private String asset_path = "";
        private String thumbnail_path = "";
        private ImageView iv_view;

        private boolean is_ok = false;

        private static final String HEXES = "0123456789ABCDEF";

        private String getHex(byte[] raw) {
            if (raw == null) {
                return null;
            }
            final StringBuilder hex = new StringBuilder(2 * raw.length);
            for (final byte b : raw) {
                hex.append(HEXES.charAt((b & 0xF0) >> 4))
                        .append(HEXES.charAt((b & 0x0F)));
            }
            return hex.toString();
        }

        public LoadImage(String asset_path, ImageView iv_view) {
            this.asset_path = asset_path;
            this.iv_view = iv_view;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            // so the vendor wants to play rough

            try {

                MessageDigest md5 = MessageDigest.getInstance("MD5");
                md5.reset();
                md5.update(asset_path.getBytes(Charset.forName("UTF8")));
                byte[] resultByte = md5.digest();

                thumbnail_path = mContext.getCacheDir() + "/" + getHex(resultByte) + ".png";

                File temp_file = new File(thumbnail_path);

                if (!temp_file.exists()) {
                    temp_file.createNewFile();

                    Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(asset_path, MediaStore.Video.Thumbnails.MICRO_KIND);

                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
                    byte[] bitmapdata = bos.toByteArray();

                    //write the bytes in file
                    FileOutputStream fos = new FileOutputStream(temp_file);
                    fos.write(bitmapdata);
                    fos.flush();
                    fos.close();

                    bos.flush();
                    bos.close();
                }
            } catch (Exception e) {
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void ret) {

            Picasso.with(mContext).load(new File(thumbnail_path)).error(R.drawable.with_video).fit().centerCrop().into(iv_view);

        }
    }
}