package com.threadr.android.app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.newrelic.agent.android.NewRelic;
import com.threadr.android.app.Utils.PreferenceHelper;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

/**
 * Created by lilandra on 1/3/14.
 */
public class LoginActivity extends Activity {

    private final static String TAG = LoginActivity.class.getSimpleName();

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private final String SENDER_ID = "121654768165";

    private GoogleCloudMessaging gcm;

    private EditText et_email;
    private EditText et_password;

    private String regid;
    private String token;

    public static final String FBREGISTER = "facebookregister";


    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(final Session session, final SessionState state, final Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_view);

        SharedData.ok_to_exit = false; // we reset for next run to open

        et_email = (EditText) findViewById(R.id.et_email);
        et_password = (EditText) findViewById(R.id.et_password);

        Button bt_login = (Button) findViewById(R.id.bt_login);
        TextView tv_forgot_password = (TextView) findViewById(R.id.tv_forgot_password);
        TextView tv_create_account = (TextView) findViewById(R.id.tv_create_account);

        String forgot_password = "<u>Forgot Password?</u>";
        tv_forgot_password.setText(Html.fromHtml(forgot_password));

        bt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getApplicationContext(),"ON LOGIN", Toast.LENGTH_SHORT).show();

                // hide keyboard
                InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                final String email = et_email.getText().toString();
                final String password = et_password.getText().toString();

                if (email.equals("") || password.equals(""))
                    Toast.makeText(getApplicationContext(), "Missing email or password", Toast.LENGTH_SHORT).show();
                else {
                    new LoginWithEmailTask().execute(email, password);
                }
            }
        });

        tv_forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ForgotPasswordActivity.class);
                startActivity(intent);

                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            }
        });

        tv_create_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(intent);

                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            }
        });

        LoginButton authButton = (LoginButton) findViewById(R.id.authButton);
        authButton.setReadPermissions(AppConstants.fbPermissions);
        authButton.setSessionStatusCallback(callback);
        authButton.setApplicationId(getString(R.string.app_id));
        authButton.setBackgroundResource(R.drawable.button_facebook);
        authButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

        NewRelic.withApplicationToken("AAca4e15d5c5a2bb763673aa53c9460fb5a6fd41ee").start(this.getApplication());

        EasyTracker.getInstance(this).activityStart(this);

        boolean registerFB = getIntent().getBooleanExtra(FBREGISTER, false);
        if (registerFB) {
            registerWithFacebook();
            // this extra is only set when launching login from registration activity.
            // in current activity flow, the parent of tabmain is login activity. we remove this extra to make sure non-register non-fb login launching dont use fb login
            getIntent().removeExtra(FBREGISTER);
        }

//        printHashKey();
    }

    private void registerWithFacebook() {
        Session session = Session.getActiveSession();
        if (session != null) {
            if (!session.isOpened())
                session.openForRead(new Session.OpenRequest(this).setCallback(callback).setPermissions(AppConstants.fbPermissions));
            else
                Session.openActiveSession(this, true, callback);
        } else
            Session.openActiveSession(this, true, callback);

    }

    @Override
    protected void onResume() {
        super.onResume();

        //Check if we have a token;
        token = PreferenceHelper.getInstance(getApplicationContext()).getString(AppConstants.USER_ACCESS_TOKEN, null);

        if (SharedData.ok_to_exit) {
            finish();
        } else {

            //If we have a token, then we automatically log in
            if (token != null) {

                if (checkPlayServices()) {
                    gcm = GoogleCloudMessaging.getInstance(this);
                    regid = getRegistrationId(getApplicationContext());

                    if (regid.isEmpty()) {
                        new DoRegisterToGCM().execute();  // the post execute function does auto login if there is token
                    } else {
                        // already registered, proceed to main application
                        Intent intent = new Intent(getApplicationContext(), TabMain.class);
                        startActivity(intent);
                    }

                } else {
                    Log.i(TAG, "No valid Google Play Services APK found. Aborting application");
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        EasyTracker.getInstance(this).activityStop(this);
    }

    private void onSessionStateChange(final Session session, SessionState state, Exception exception) {
        if (session != null && session.isOpened()) {
            // Get the user's data.
            // Make an API call to get user data and define a
            // new callback to handle the response.
            Request request = Request.newMeRequest(session,
                    new Request.GraphUserCallback() {
                        @Override
                        public void onCompleted(GraphUser user, Response response) {
                            // If the response is successful
                            if (session == Session.getActiveSession()) {
                                if (user != null) {
                                    String fbToken = Session.getActiveSession().getAccessToken();
                                    saveFBToken(LoginActivity.this, fbToken);
                                    new LoginWithEmailTask().execute(fbToken);
                                }
                            }
                            if (response.getError() != null) {
                                // Handle errors, will do so later.
//                                Log.i(TAG, "Facebook error = "+ response.getError());
                                Toast.makeText(getApplicationContext(), response.getError().toString(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
            );
            request.executeAsync();
        }
    }


    private void populatePreferrences(Map<String, Object> result) {
        if (result != null) {
            boolean login_ok = Boolean.parseBoolean(result.get("result").toString());
            if (login_ok) {
                // store login result

                PreferenceHelper.getInstance(getApplicationContext()).setString(AppConstants.USER_ACCESS_TOKEN, result.get("access_token").toString());
                PreferenceHelper.getInstance(getApplicationContext()).setString(AppConstants.USER_ID, result.get("userId").toString());
                PreferenceHelper.getInstance(getApplicationContext()).setString(AppConstants.USER_FIRST_NAME, result.get("firstName").toString());
                PreferenceHelper.getInstance(getApplicationContext()).setString(AppConstants.USER_LAST_NAME, result.get("lastName").toString());
                PreferenceHelper.getInstance(getApplicationContext()).setString(AppConstants.USER_EMAIL, result.get("email").toString());
                PreferenceHelper.getInstance(getApplicationContext()).setString(AppConstants.USER_PHONE_NUMBER, result.get("phoneNumber").toString());
                PreferenceHelper.getInstance(getApplicationContext()).setString(AppConstants.USER_PROFILE_IMAGE, result.get("profileImage").toString());
                PreferenceHelper.getInstance(getApplicationContext()).setBoolean(AppConstants.USER_EMAIL_NOTIFICATION, Integer.parseInt(result.get("emailNotification").toString()) != 0);

            }
        }
    }


    private class LoginWithEmailTask extends AsyncTask<String, Void, Boolean> {

        private ProgressDialog progressDialog;
        Map<String, Object> result = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(LoginActivity.this, null, "Loading...");
        }

        @Override
        protected Boolean doInBackground(String... params) {
            String email = params[0];
            String password = null;

            if (params.length > 1)
                password = params[1];

            if (password != null)
                result = ThreadrAPIAdapter.doLogin(email, password);
            else {
                //If password is null, then email must be facebook token, so we treat this as facebook login
                result = ThreadrAPIAdapter.doLogin(email);  //email here is actually the facebook token
            }

            if (result != null && Boolean.parseBoolean(result.get("result").toString())) {
                populatePreferrences(result);
                return true;
            } else
                return false;

        }

        @Override
        protected void onPostExecute(Boolean response) {
            super.onPostExecute(response);

            progressDialog.dismiss();

            if (response) {
                gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
                regid = getRegistrationId(getApplicationContext());

                if (regid.isEmpty()) {
                    new DoRegisterToGCM().execute();  // the post execute function does auto login if there is token
                } else {
                    // already registered, proceed to main application
                    Intent intent = new Intent(getApplicationContext(), TabMain.class);
                    startActivity(intent);
                }
            } else {
                if (result != null) {
                    String errorMessage = result.get("errorMessage").toString();
                    Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(getApplicationContext(), "Unable to complete login", Toast.LENGTH_SHORT).show();
            }
        }
    }

    class DoRegisterToGCM extends AsyncTask<Void, Void, Void> {

        //protected ProgressDialog progressDialog;

        private boolean is_ok = false;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //progressDialog = ProgressDialog.show(LoginActivity.this, "", "Registering to GCM", true, false);
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {

                if (gcm == null) {
                    gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
                }

                regid = gcm.register(SENDER_ID);

                Log.i(TAG + " registration id", regid);

                String access_token = getSharedPreferences(AppConstants.SHARED_DATA, Context.MODE_PRIVATE).getString(AppConstants.USER_ACCESS_TOKEN, "UNKNOWN");
                Map<String, Object> result = ThreadrAPIAdapter.doAddDeviceToken(access_token, regid);

                if (result != null && Boolean.parseBoolean(result.get("result").toString())) {
                    storeRegistrationId(getApplicationContext(), regid);
                    is_ok = true;
                }
            } catch (Exception e) {

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void ret) {
            //progressDialog.dismiss();

            if (is_ok) {
                //If we have a token, then we automatically log in
                token = PreferenceHelper.getInstance(getApplicationContext()).getString(AppConstants.USER_ACCESS_TOKEN, null);
                if (token != null) {
                    Intent intent = new Intent(getApplicationContext(), TabMain.class);
                    startActivity(intent);
                }
            } else
                Toast.makeText(getApplicationContext(), "Unable to complete GCM registration.", Toast.LENGTH_SHORT).show();
        }
    }

    public void printHashKey() {

        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.aclipsa.zipaclip", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("TEMPTAGHASH KEY:",
                        android.util.Base64.encodeToString(md.digest(), android.util.Base64.DEFAULT));
            }
        } catch (NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGcmPreferences(context);
        String registrationId = prefs.getString(AppConstants.REGISTRATION_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(AppConstants.APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }

    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGcmPreferences(context);
        int appVersion = getAppVersion(context);
        Log.i(TAG, "Saving regId on app version " + appVersion);
        Log.i(TAG, "Saving regId " + regId);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(AppConstants.REGISTRATION_ID, regId);
        editor.putInt(AppConstants.APP_VERSION, appVersion);
        editor.commit();
    }

    private SharedPreferences getGcmPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return getSharedPreferences(TAG,
                Context.MODE_PRIVATE);
    }

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    public void saveFBToken(Context context, String token) {
        SharedPreferences shared_data = context.getSharedPreferences(AppConstants.SHARED_DATA, context.MODE_PRIVATE);
        SharedPreferences.Editor editor = shared_data.edit();
        editor.putString(AppConstants.USER_FACEBOOK_ACCESS_TOKEN, token);
        editor.commit();
    }
}