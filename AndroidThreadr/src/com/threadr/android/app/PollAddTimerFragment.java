package com.threadr.android.app;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;
import com.threadr.android.app.Utils.PreferenceHelper;
import com.threadr.android.app.Utils.TextFontHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by lilandra on 1/15/14.
 */
public class PollAddTimerFragment extends BaseFragment {

    private static final String TAG = "PollAddTimerFragment";

    private NumberPicker np_day;
    private NumberPicker np_hour;
    private NumberPicker np_minute;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View V = inflater.inflate(R.layout.poll_add_timer_view, container, false);

        np_day = (NumberPicker) V.findViewById(R.id.np_day);
        np_hour = (NumberPicker) V.findViewById(R.id.np_hour);
        np_minute = (NumberPicker) V.findViewById(R.id.np_minute);

        np_day.setMinValue(0);
        np_hour.setMinValue(0);
        np_minute.setMinValue(0);

        np_day.setMaxValue(999);
        np_hour.setMaxValue(24);
        np_minute.setMaxValue(60);

//        Button bt_reset_day = (Button) V.findViewById(R.id.bt_reset_day);
//        bt_reset_day.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                np_day.setValue(0);
//            }
//        });

//        Button bt_reset_hour = (Button) V.findViewById(R.id.bt_reset_hour);
//        bt_reset_hour.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                np_hour.setValue(0);
//            }
//        });

        Button bt_reset_minute = (Button) V.findViewById(R.id.bt_reset_minute);
        bt_reset_minute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                np_day.setValue(0);
                np_hour.setValue(0);
                np_minute.setValue(0);
            }
        });

        Button bt_post = (Button) V.findViewById(R.id.bt_post);
        bt_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int total_seconds = 0;

                int set_days = np_day.getValue();
                int set_hours = np_hour.getValue();
                int set_minutes = np_minute.getValue();

                total_seconds = (set_days * 86400) + (set_hours * 3600) + (set_minutes * 60);

                if (total_seconds > 0) {

                    // update shared alert payload
                    SharedData.alert_payload.duration = total_seconds;

                    new CreatePoll().execute();
                }
            }
        });

        //TextView tv_title = (TextView) V.findViewById(R.id.tv_title);
        //TextFontHelper.setFontAndText(getActivity(), tv_title, null);

        return V;
    }

    private void createAlert() {
        try {

            String access_token = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_ACCESS_TOKEN, "UNKNOWN");
            // get recipients

            if (SharedData.alert_payload.target_group_ids.size() > 0) {
                // we will get all contacts under the groups

                for (String group_id : SharedData.alert_payload.target_group_ids) {

                    Map<String, Object> result = ThreadrAPIAdapter.doGetAllGroupContacts(access_token, group_id);

                    try {
                        Object contacts = result.get("data");

                        for (Map<String, Object> contact : (ArrayList<Map<String, Object>>) contacts) {

                            String contact_id = contact.get("contactId").toString();

                            if (!SharedData.alert_payload.target_contact_ids.contains(contact_id))
                                SharedData.alert_payload.target_contact_ids.add(contact_id);
                        }
                    } catch (Exception e) {
                    }
                }
            }

            // now we do the creation of the alert

            String media_asset_temp_file = "";

            if (!SharedData.alert_payload.media.url.equals("")) {

                int i = SharedData.alert_payload.media.url.lastIndexOf('.');
                String media_ext = SharedData.alert_payload.media.url.substring(i + 1);

                String unique_id = UUID.randomUUID().toString();

                if (SharedData.alert_payload.media.type.equals(AppConstants.ASSET_IMAGE))
                    media_asset_temp_file = String.format("%s_photo.%s", unique_id, media_ext);
                else if (SharedData.alert_payload.media.type.equals(AppConstants.ASSET_VIDEO))
                    media_asset_temp_file = String.format("%s_video.%s", unique_id, media_ext);
                else if (SharedData.alert_payload.media.type.equals(AppConstants.ASSET_AUDIO))
                    media_asset_temp_file = String.format("%s_audio.%s", unique_id, media_ext);
            }

            Log.i(TAG + "MEDIA URL",SharedData.alert_payload.media.url);
            Log.i(TAG + "MEDIA TEMP FILE", media_asset_temp_file);


            Map<String, Object> create_alert_result;

            if (SharedData.alert_payload.longitude != 0.0 && SharedData.alert_payload.latitude != 0.0) {
                String location_name = "";
                Map<String, Object> reverse_geolocation;
                try {
                    reverse_geolocation = ThreadrAPIAdapter.getLocation(SharedData.alert_payload.latitude, SharedData.alert_payload.longitude);
                    List<Map<String, Object>> data = (ArrayList<Map<String, Object>>) reverse_geolocation.get("results");

                    //street level
                    //location_name = data.get(0).get("formatted_address").toString();

                    //block level
                    location_name = data.get(1).get("formatted_address").toString();

                } catch (Exception e) {
                }

                create_alert_result = ThreadrAPIAdapter.doCreateAlertWithGPS(getActivity().getApplicationContext(), access_token, SharedData.alert_payload.choices, SharedData.alert_payload.target_contact_ids, SharedData.alert_payload.description, SharedData.alert_payload.duration, SharedData.alert_payload.latitude, SharedData.alert_payload.longitude, location_name, SharedData.alert_payload.media.url, SharedData.alert_payload.media.type, media_asset_temp_file);
            } else
                create_alert_result = ThreadrAPIAdapter.doCreateAlert(getActivity().getApplicationContext(), access_token, SharedData.alert_payload.choices, SharedData.alert_payload.target_contact_ids, SharedData.alert_payload.description, SharedData.alert_payload.duration, SharedData.alert_payload.media.url, SharedData.alert_payload.media.type, media_asset_temp_file);

            String alert_id = create_alert_result.get("alertId").toString();

            SharedData.alert_payload.alert_id = alert_id;

            Log.i(TAG, alert_id);
            Log.i(TAG, SharedData.alert_payload.description);
            Log.i(TAG, Integer.toString(SharedData.alert_payload.duration));


            for (PollInfo.AssetInfo asset : SharedData.alert_payload.assets) {

                String file_name = "";

                int i = asset.url.lastIndexOf('.');
                String fileExtension = asset.url.substring(i + 1);

                String unique_id = UUID.randomUUID().toString();

                if (asset.type.equals(AppConstants.ASSET_IMAGE))
                    file_name = String.format("%s_photo_%s.%s", alert_id, unique_id, fileExtension);
                else if (asset.type.equals(AppConstants.ASSET_VIDEO))
                    file_name = String.format("%s_video_%s.%s", alert_id, unique_id, fileExtension);
                else if (asset.type.equals(AppConstants.ASSET_AUDIO))
                    file_name = String.format("%s_audio_%s.%s", alert_id, unique_id, fileExtension);

                Log.i(TAG, asset.type);
                Log.i(TAG, file_name);

                Map<String, Object> add_file_result = ThreadrAPIAdapter.doAddAsset(getActivity().getApplicationContext(), access_token, alert_id, asset.type, file_name, asset.url, asset.description);

            }
        } catch (Exception e) {
            Log.i(TAG + " createAlert", e.toString());
        }

        // we make sure we reset the shared alert payload to be ready for the next send
        SharedData.alert_payload.resetData();

        // make sure we reload
        SharedData.retrieved_polls.must_reload = true;
    }

    class CreatePoll extends AsyncTask<Void, Void, Void> {

        protected ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(main_tab_activity, "", "Uploading ...", true, false);
        }

        @Override
        protected Void doInBackground(Void... params) {
            PollAddTimerFragment.this.createAlert();
            return null;
        }

        @Override
        protected void onPostExecute(Void ret) {
            progressDialog.dismiss();
            main_tab_activity.returnToFirstScreen();
        }
    }
}