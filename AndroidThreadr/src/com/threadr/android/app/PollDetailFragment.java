package com.threadr.android.app;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.ArcShape;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.squareup.picasso.Picasso;
import com.threadr.android.app.Utils.PreferenceHelper;

import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by lilandra on 1/21/14.
 */
public class PollDetailFragment extends BaseFragment {

    private static final String TAG = "PollDetailFragment";
    private static final String BASE_SHARE_URL = "http://askthreadr.com/index.php/threadr/alert/";
    private static final int UI_UPDATE_INTERVAL_MILLISECONDS = 10000;

    private final PollInfo alert_info = new PollInfo();
    private String alert_id = null;

    private TextView tv_sender_name;
    private TextView tv_description;
    private TextView tv_posting_date;
    private TextView tv_location_name;
    private TextView tv_short_remaining_time;
    private TextView tv_option_label;
    private EditText et_message;
    private ImageView iv_sender;
    private ImageView iv_vote_info;
    private ImageView arcImageView;
    private ImageView grayArcImageView;
    private ImageView innerWhiteImageView;

    private Button bt_send;

    private LinearLayout ll_edit_share_delete;
    private LinearLayout ll_media;
    private LinearLayout ll_assets;
    private LinearLayout ll_options;
    private LinearLayout ll_messages;

    private LayoutInflater inflater;

    private boolean is_owned = true;
    private boolean is_expired = false;
    private boolean is_loaded = false;

    private Handler ui_update_handler;

    private Runnable ui_update_payload;

    private LoadAlertInfoSilently silent_loader;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        this.inflater = inflater;

        View V = inflater.inflate(R.layout.poll_detail_view, container, false);

        // this adjust the keyboard to not cover the edit text
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        ll_edit_share_delete = (LinearLayout) V.findViewById(R.id.ll_edit_share_delete);
        ll_media = (LinearLayout) V.findViewById(R.id.ll_media);
        ll_assets = (LinearLayout) V.findViewById(R.id.ll_assets);
        ll_options = (LinearLayout) V.findViewById(R.id.ll_options);
        ll_messages = (LinearLayout) V.findViewById(R.id.ll_messages);
        iv_sender = (ImageView) V.findViewById(R.id.sender_icon);
        arcImageView = (ImageView) V.findViewById(R.id.arcImageView);
        grayArcImageView = (ImageView) V.findViewById(R.id.grayArcImageView);
        innerWhiteImageView = (ImageView) V.findViewById(R.id.innerWhiteImageView);
        iv_vote_info = (ImageView) V.findViewById(R.id.iv_vote_tally_info);
        tv_sender_name = (TextView) V.findViewById(R.id.tv_sender_name);
        tv_description = (TextView) V.findViewById(R.id.tv_description);
        tv_posting_date = (TextView) V.findViewById(R.id.tv_posting_date);
        tv_location_name = (TextView) V.findViewById(R.id.tv_location_name);
        tv_short_remaining_time = (TextView) V.findViewById(R.id.tv_short_remaining_time);
        View tv_edit = (View) V.findViewById(R.id.tv_edit);
        View tv_share = (View) V.findViewById(R.id.tv_share);
        View tv_delete = (View) V.findViewById(R.id.tv_delete);
        tv_option_label = (TextView) V.findViewById(R.id.tv_option_label);
        et_message = (EditText) V.findViewById(R.id.et_message);
        bt_send = (Button) V.findViewById(R.id.bt_send);

        // set click listeners

        tv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!is_expired) {
                    // save this alert info that you wish to edit

                    SharedData.alert_ammendment_payload.resetData();

                    SharedData.alert_ammendment_payload.old_alert.Reset();

                    SharedData.alert_ammendment_payload.old_alert.id = alert_info.id;
                    SharedData.alert_ammendment_payload.old_alert.description = alert_info.description;
                    SharedData.alert_ammendment_payload.old_alert.post_date = alert_info.post_date;
                    SharedData.alert_ammendment_payload.old_alert.expiration_date = alert_info.expiration_date;

                    SharedData.alert_ammendment_payload.old_alert.sender_user_id = alert_info.sender_user_id;
                    SharedData.alert_ammendment_payload.old_alert.sender_first_name = alert_info.sender_first_name;
                    SharedData.alert_ammendment_payload.old_alert.sender_last_name = alert_info.sender_last_name;
                    SharedData.alert_ammendment_payload.old_alert.sender_image_url = alert_info.sender_image_url;
                    SharedData.alert_ammendment_payload.old_alert.media.url = alert_info.media.url;
                    SharedData.alert_ammendment_payload.old_alert.media.type = alert_info.media.type;
                    SharedData.alert_ammendment_payload.media.url = alert_info.media.url;
                    SharedData.alert_ammendment_payload.media.type = alert_info.media.type;

                    SharedData.alert_ammendment_payload.description = alert_info.description;

                    for (PollInfo.AssetInfo asset : alert_info.assets) {
                        SharedData.alert_ammendment_payload.old_alert.assets.add(asset);
                        SharedData.alert_ammendment_payload.assets.add(asset);
                    }

                    for (PollInfo.ContactInfo contact : alert_info.contacts)
                        SharedData.alert_ammendment_payload.old_alert.contacts.add(contact);

                    EditWhoToAskFragment fragment = new EditWhoToAskFragment();

                    main_tab_activity.pushFragments(AppConstants.TAB_POLLS, fragment, true, true);
                }
            }
        });

        tv_share.setVisibility(View.INVISIBLE);

        /*
        tv_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!is_expired) {
                    Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                    intent.setType("text/plain");

                    String access_token = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_ACCESS_TOKEN, "UNKNOWN");
                    String share_link = BASE_SHARE_URL;

                    try {
                        share_link = share_link + URLEncoder.encode(access_token, "UTF-8") + "/" + alert_info.id;
                    } catch (Exception e) {
                    }

                    // Add data to the intent, the receiving app will decide what to do with it.
                    intent.putExtra(Intent.EXTRA_SUBJECT, "Threadr Share");
                    intent.putExtra(Intent.EXTRA_TEXT, share_link);

                    startActivity(Intent.createChooser(intent, "Where do you want it shared ?"));
                }
            }
        });
        */

        tv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                LayoutInflater inflater = getActivity().getLayoutInflater();

                View add_contact_status_generic_view = inflater.inflate(R.layout.generic_delete_prompt_view, null);
                builder.setView(add_contact_status_generic_view);

                final AlertDialog dialog = builder.create();

                TextView tv_description = (TextView) add_contact_status_generic_view.findViewById(R.id.tv_description);

                tv_description.setText("Are you sure you want to delete this thread ?");

                TextView tv_no = (TextView) add_contact_status_generic_view.findViewById(R.id.tv_no);
                tv_no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                TextView tv_yes = (TextView) add_contact_status_generic_view.findViewById(R.id.tv_yes);
                tv_yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();

                        new DeleteAlert(alert_id).execute();
                    }
                });

                dialog.show();
            }
        });

        tv_location_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity()) == ConnectionResult.SUCCESS) {
                    Bundle bundle = new Bundle();
                    bundle.putString(AppConstants.LOC_NAME, alert_info.location_name);
                    bundle.putDouble(AppConstants.LOC_LATITUDE, alert_info.latitude);
                    bundle.putDouble(AppConstants.LOC_LONGITUDE, alert_info.longitude);

                    PollLocationFragment fragment = new PollLocationFragment();
                    fragment.setArguments(bundle);

                    main_tab_activity.pushFragments(AppConstants.TAB_POLLS, fragment, true, true);
                } else
                    Toast.makeText(getActivity(), "GooglePlayServices is old or not present", Toast.LENGTH_SHORT).show();
            }
        });

        iv_vote_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PollDetailVoteBreakdownFragment fragment = new PollDetailVoteBreakdownFragment();
                main_tab_activity.pushFragments(AppConstants.TAB_POLLS, fragment, true, true);
            }
        });

        bt_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // hide keyboard
                InputMethodManager inputManager = (InputMethodManager) main_tab_activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(main_tab_activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                String message = et_message.getText().toString();
                if (isOwned())
                    new AddMessage(alert_info.id, message).execute();
                else
                    new AddMessage(alert_info.original_id, message).execute();
            }
        });

        return V;
    }

    @Override
    public void onResume() {
        super.onResume();

        alert_id = arguments.getString(AppConstants.ALERT_ID);

        if (ui_update_handler == null)
            ui_update_handler = new Handler();

        if (ui_update_payload == null)
            ui_update_payload = new Runnable() {
                @Override
                public void run() {

                    Log.i(TAG, "REFRESHING DISPLAY");

                    silent_loader = new LoadAlertInfoSilently();
                    silent_loader.execute();

                    //ui_update_handler.postDelayed(this, UI_UPDATE_INTERVAL_MILLISECONDS);
                }
            };

        if (!is_loaded) {
            is_loaded = true;
            new LoadAlertInfo().execute();
        } else {
            updateDisplay();
        }

        ui_update_handler.postDelayed(ui_update_payload, UI_UPDATE_INTERVAL_MILLISECONDS);
    }

    @Override
    public void onPause() {
        super.onPause();

        if (silent_loader != null) {
            silent_loader.cancel(true);
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
            }
        }

        ui_update_handler.removeCallbacksAndMessages(null);

    }

    private synchronized void loadAlertInfo() {

        alert_info.Reset();
        SharedData.vote_data.reset();

        String access_token = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_ACCESS_TOKEN, "UNKNOWN");
        Map<String, Object> result = null;
        try {
            result = ThreadrAPIAdapter.doGetAlertInfo(access_token, alert_id);
        } catch (Exception e) {
            Log.i(TAG + " loadAlertInfo", e.toString());
        }

        if (result != null) {

            try {
                Map<String, Object> alert = (Map<String, Object>) result.get("data");

                alert_info.id = alert.get("alertId").toString();
                alert_info.original_id = alert.get("origin").toString();
                alert_info.description = alert.get("description").toString();
                alert_info.post_date = alert.get("timestamp").toString();
                alert_info.expiration_date = alert.get("expirationDate").toString();
                alert_info.location_name = alert.get("locationName").toString();
                alert_info.latitude = Double.parseDouble(alert.get("latitude").toString());
                alert_info.longitude = Double.parseDouble(alert.get("longitude").toString());
                alert_info.media.url = alert.get("filename").toString();
                alert_info.media.type = alert.get("type").toString();

                Map<String, Object> user_info = (Map<String, Object>) alert.get("userInfo");
                alert_info.sender_user_id = user_info.get("userId").toString();
                alert_info.sender_first_name = user_info.get("firstName").toString();
                alert_info.sender_last_name = user_info.get("lastName").toString();
                alert_info.sender_image_url = user_info.get("profileImage").toString();

                Object assets = alert.get("assets");
                for (Map<String, Object> asset : (ArrayList<Map<String, Object>>) assets) {
                    PollInfo.AssetInfo asset_info = new PollInfo.AssetInfo();
                    asset_info.id = asset.get("assetId").toString();
                    asset_info.url = asset.get("filename").toString();
                    asset_info.type = asset.get("type").toString();
                    asset_info.description = asset.get("choice").toString();
                    asset_info.count = Integer.parseInt(asset.get("count").toString());

                    alert_info.assets.add(asset_info);
                    SharedData.vote_data.assets.add(asset_info);
                }

                Object contacts = alert.get("contacts");
                for (Map<String, Object> contact : (ArrayList<Map<String, Object>>) contacts) {
                    PollInfo.ContactInfo alert_contact = new PollInfo.ContactInfo();
                    alert_contact.user_id = contact.get("userId").toString();
                    alert_contact.contact_id = contact.get("alertContactId").toString();
                    alert_contact.image_url = contact.get("profileImage").toString();
                    alert_contact.first_name = contact.get("firstName").toString();
                    alert_contact.last_name = contact.get("lastName").toString();
                    alert_contact.asset_id = contact.get("assetId").toString();

                    alert_info.contacts.add(alert_contact);
                    SharedData.vote_data.contacts.add(alert_contact);
                }

            } catch (Exception e) {
            }
        }
    }

    private synchronized void loadAlertMessages() {

        alert_info.messages.clear();

        String access_token = main_tab_activity.getSharedPreferences(AppConstants.SHARED_DATA, Context.MODE_PRIVATE).getString(AppConstants.USER_ACCESS_TOKEN, "UNKNOWN");

        Map<String, Object> result = null;

        try {
            if (isOwned())
                result = ThreadrAPIAdapter.doGetMessageList(access_token, alert_info.id);
            else
                result = ThreadrAPIAdapter.doGetMessageList(access_token, alert_info.original_id);

        } catch (Exception e) {
            Log.i(TAG + " loadAlertMessages", e.toString());
        }

        if (result != null) {

            try {
                Object messages = result.get("data");

                for (Map<String, Object> message : (ArrayList<Map<String, Object>>) messages) {
                    PollInfo.MessageInfo alert_message = new PollInfo.MessageInfo();
                    alert_message.text = message.get("message").toString();
                    alert_message.time_stamp = message.get("timestamp").toString();
                    alert_message.sender = message.get("name").toString();
                    alert_message.image_url = message.get("profileImage").toString();

                    alert_info.messages.add(alert_message);
                }
            } catch (Exception e) {
            }
        }
    }

    private void updateDisplay() {

        // UPDATE THE VISUAL INDICATOR
        float getTimeRemainingPercentage = getTimeRemainingPercentage(alert_info.expiration_date, alert_info.post_date);
        float angle = (1 - getTimeRemainingPercentage) * 360;

        ShapeDrawable grayDrawable = new ShapeDrawable(new ArcShape(270, angle));
        grayDrawable.getPaint().setColor(Color.LTGRAY);
        grayDrawable.setIntrinsicHeight(100);
        grayDrawable.setIntrinsicWidth(100);
        grayArcImageView.setImageDrawable(grayDrawable);

        ShapeDrawable whiteDrawable = new ShapeDrawable(new ArcShape(270, 360));
        whiteDrawable.getPaint().setColor(Color.WHITE);
        whiteDrawable.setIntrinsicHeight(100);
        whiteDrawable.setIntrinsicWidth(100);
        innerWhiteImageView.setImageDrawable(whiteDrawable);

        // ASCERTAIN OWNERSHIP OF POLL
        String my_user_id = main_tab_activity.getSharedPreferences(AppConstants.SHARED_DATA, Context.MODE_PRIVATE).getString(AppConstants.USER_ID, "UNKNOWN");

        final String contact_id = getAlertContactId();
        is_expired = isExpired(alert_info.expiration_date);

        is_owned = isOwned();


        // UPDATE POLL OPERATIONS
        if (!is_owned)
            ll_edit_share_delete.setVisibility(View.GONE);

        // UPDATE NAME
        String full_name = alert_info.sender_first_name + " " + alert_info.sender_last_name;
        tv_sender_name.setText(full_name);

        // UPDATE POSTING DATE
        tv_posting_date.setText("Posted on " + convertISODate(alert_info.post_date));

        // UPDATE POSTING LOCATION
        tv_location_name.setText("From: " + alert_info.location_name);

        // UPDATE NUMERICAL INDICATOR
        if (!is_expired)
            tv_short_remaining_time.setText(getShortTimeDifference(alert_info.expiration_date));
        else {
            tv_short_remaining_time.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 15);
            tv_short_remaining_time.setText("done");
        }

        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int height = metrics.heightPixels;
        int width = metrics.widthPixels;

        // UPDATE DESCRIPTION
        if (alert_info.media.url.equals("")) {

            tv_description.setVisibility(View.VISIBLE);
            ll_media.setVisibility(View.GONE);

            tv_description.setText(alert_info.description);
        } else {

            tv_description.setVisibility(View.GONE);
            ll_media.setVisibility(View.VISIBLE);
            ll_media.removeAllViews();

            if (alert_info.media.type.equals(AppConstants.ASSET_IMAGE)) {

                View asset_view = null;

                asset_view = inflater.inflate(R.layout.poll_media_image, ll_media, false);

                RelativeLayout.LayoutParams parms = new RelativeLayout.LayoutParams(width, width);
                RelativeLayout rl_asset = (RelativeLayout) asset_view.findViewById(R.id.rl_asset);
                rl_asset.setLayoutParams(parms);

                ImageView iv_asset = (ImageView) rl_asset.findViewById(R.id.iv_asset);
                TextView tv_description = (TextView) rl_asset.findViewById(R.id.tv_description);

                iv_asset.setAdjustViewBounds(true);
                iv_asset.setScaleType(ImageView.ScaleType.FIT_CENTER);

                Picasso.with(getActivity()).load(alert_info.media.url).into(iv_asset);
                tv_description.setText(alert_info.description);

                iv_asset.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            Uri theURI = Uri.parse(alert_info.media.url);
                            Intent intent = new Intent();
                            intent.setAction(android.content.Intent.ACTION_VIEW);
                            intent.setData(theURI); // opening via a browser is the most reliable
                            startActivity(intent);
                        } catch (Exception e) {
                            Toast.makeText(main_tab_activity, "Cannot process the media", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                ll_media.addView(asset_view);

            } else if (alert_info.media.type.equals(AppConstants.ASSET_VIDEO)) {

                View asset_view = null;

                asset_view = inflater.inflate(R.layout.poll_media_video, ll_media, false);

                RelativeLayout.LayoutParams parms = new RelativeLayout.LayoutParams(width, width);
                RelativeLayout rl_asset = (RelativeLayout) asset_view.findViewById(R.id.rl_asset);
                rl_asset.setLayoutParams(parms);

                ImageView iv_asset = (ImageView) rl_asset.findViewById(R.id.iv_asset);
                TextView tv_description = (TextView) rl_asset.findViewById(R.id.tv_description);

                int i = alert_info.media.url.lastIndexOf(".");
                String remote_image = alert_info.media.url.substring(0, i) + "_thumbnail.jpg";
                Picasso.with(getActivity()).load(remote_image).error(R.drawable.with_video).resize(width, width).centerCrop().into(iv_asset);

                tv_description.setText(alert_info.description);

                iv_asset.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            Uri theURI = Uri.parse(alert_info.media.url);
                            Intent intent = new Intent();
                            intent.setAction(android.content.Intent.ACTION_VIEW);
                            intent.setDataAndType(theURI, URLConnection.guessContentTypeFromName(alert_info.media.url));
                            startActivity(intent);
                        } catch (Exception e) {
                            Toast.makeText(main_tab_activity, "Cannot process the media", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                ll_media.addView(asset_view);

            } else {

                View asset_view = null;

                asset_view = inflater.inflate(R.layout.poll_media_audio, ll_media, false);

                RelativeLayout rl_asset = (RelativeLayout) asset_view.findViewById(R.id.rl_asset);

                ImageView iv_play = (ImageView) rl_asset.findViewById(R.id.iv_play);
                ImageView iv_asset = (ImageView) rl_asset.findViewById(R.id.iv_asset);
                TextView tv_description = (TextView) rl_asset.findViewById(R.id.tv_description);

                tv_description.setText(alert_info.description);

                iv_play.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            Uri theURI = Uri.parse(alert_info.media.url);
                            Intent intent = new Intent();
                            intent.setAction(android.content.Intent.ACTION_VIEW);
                            intent.setDataAndType(theURI, URLConnection.guessContentTypeFromName(alert_info.media.url));
                            startActivity(intent);
                        } catch (Exception e) {
                            Toast.makeText(main_tab_activity, "Cannot process the media", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                iv_asset.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            Uri theURI = Uri.parse(alert_info.media.url);
                            Intent intent = new Intent();
                            intent.setAction(android.content.Intent.ACTION_VIEW);
                            intent.setDataAndType(theURI, URLConnection.guessContentTypeFromName(alert_info.media.url));
                            startActivity(intent);
                        } catch (Exception e) {
                            Toast.makeText(main_tab_activity, "Cannot process the media", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                ll_media.addView(asset_view);
            }
        }

        // UPDATE AVATAR
        if (!alert_info.sender_image_url.equals(""))
            Picasso.with(getActivity()).load(alert_info.sender_image_url).into(iv_sender);
        else
            Picasso.with(getActivity()).load(R.drawable.silhouette).into(iv_sender);

        // UPDATE POLL OPTIONS LABEL
        if (is_owned || is_expired)
            tv_option_label.setText("Poll Options:");
        else
            tv_option_label.setText("Poll Options (double-tap to vote):");

        // UPDATE VOTE INFO ICON
        if (!is_owned)
            iv_vote_info.setVisibility(View.GONE);

        // UPDATE ASSETS
        ll_assets.removeAllViews();

        String voted_asset_id = "";

        for (PollInfo.ContactInfo contactInfo : alert_info.contacts) {
            if (contactInfo.user_id.equals(my_user_id)) {
                if (!contactInfo.asset_id.equals("0"))
                    voted_asset_id = contactInfo.asset_id;
            }
        }

        int pax_voted = 0;

        for (PollInfo.AssetInfo asset : alert_info.assets) {
            pax_voted += asset.count;
        }

        int order_number = 0;

        for (final PollInfo.AssetInfo asset_info : alert_info.assets) {

            Log.i(TAG, asset_info.url);

            View asset_view = null;

            if (is_owned) {

                // POLL CREATOR MODE

                if (asset_info.type.equals(AppConstants.ASSET_IMAGE)) {
                    asset_view = inflater.inflate(R.layout.poll_detail_creator_image, ll_options, false);

                    RelativeLayout.LayoutParams parms = new RelativeLayout.LayoutParams(width, width);
                    RelativeLayout rl_asset = (RelativeLayout) asset_view.findViewById(R.id.rl_asset);
                    rl_asset.setLayoutParams(parms);

                    ImageView iv_asset = (ImageView) rl_asset.findViewById(R.id.iv_asset);
                    TextView tv_percentage = (TextView) rl_asset.findViewById(R.id.tv_percentage);
                    TextView tv_description = (TextView) rl_asset.findViewById(R.id.tv_description);

                    iv_asset.setAdjustViewBounds(true);
                    iv_asset.setScaleType(ImageView.ScaleType.FIT_CENTER);

                    Picasso.with(getActivity()).load(asset_info.url).into(iv_asset);
                    tv_description.setText(asset_info.description);

                    double count_percentage = 0.0;
                    if (asset_info.count != 0)
                        count_percentage = (asset_info.count / (pax_voted * 1.0)) * 100;
                    String str_count_percentage = String.format("%.2f", count_percentage) + "%";
                    tv_percentage.setText(str_count_percentage);

                    iv_asset.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                Uri theURI = Uri.parse(asset_info.url);
                                Intent intent = new Intent();
                                intent.setAction(android.content.Intent.ACTION_VIEW);
                                intent.setData(theURI); // opening via a browser is the most reliable
                                startActivity(intent);
                            } catch (Exception e) {
                                Toast.makeText(main_tab_activity, "Cannot process the media", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                    ll_assets.addView(asset_view);
                } else if (asset_info.type.equals(AppConstants.ASSET_AUDIO)) {
                    asset_view = inflater.inflate(R.layout.poll_detail_creator_audio, ll_options, false);

                    RelativeLayout rl_asset = (RelativeLayout) asset_view.findViewById(R.id.rl_asset);

                    ImageView iv_play = (ImageView) rl_asset.findViewById(R.id.iv_play);
                    ImageView iv_asset = (ImageView) rl_asset.findViewById(R.id.iv_asset);
                    TextView tv_percentage = (TextView) rl_asset.findViewById(R.id.tv_percentage);
                    TextView tv_description = (TextView) rl_asset.findViewById(R.id.tv_description);

                    tv_description.setText(asset_info.description);

                    double count_percentage = 0.0;
                    if (asset_info.count != 0)
                        count_percentage = (asset_info.count / (pax_voted * 1.0)) * 100;
                    String str_count_percentage = String.format("%.2f", count_percentage) + "%";
                    tv_percentage.setText(str_count_percentage);

                    iv_play.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                Uri theURI = Uri.parse(asset_info.url);
                                Intent intent = new Intent();
                                intent.setAction(android.content.Intent.ACTION_VIEW);
                                intent.setDataAndType(theURI, URLConnection.guessContentTypeFromName(asset_info.url));
                                startActivity(intent);
                            } catch (Exception e) {
                                Toast.makeText(main_tab_activity, "Cannot process the media", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                    iv_asset.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                Uri theURI = Uri.parse(asset_info.url);
                                Intent intent = new Intent();
                                intent.setAction(android.content.Intent.ACTION_VIEW);
                                intent.setDataAndType(theURI, URLConnection.guessContentTypeFromName(asset_info.url));
                                startActivity(intent);
                            } catch (Exception e) {
                                Toast.makeText(main_tab_activity, "Cannot process the media", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                    ll_assets.addView(asset_view);
                } else if (asset_info.type.equals(AppConstants.ASSET_TEXT)) {
                    asset_view = inflater.inflate(R.layout.poll_detail_creator_text, ll_options, false);

                    RelativeLayout rl_asset = (RelativeLayout) asset_view.findViewById(R.id.rl_asset);

                    ImageView iv_asset = (ImageView) rl_asset.findViewById(R.id.iv_asset);
                    TextView tv_percentage = (TextView) rl_asset.findViewById(R.id.tv_percentage);
                    TextView tv_description = (TextView) rl_asset.findViewById(R.id.tv_description);

                    tv_description.setText(asset_info.description);

                    double count_percentage = 0.0;
                    if (asset_info.count != 0)
                        count_percentage = (asset_info.count / (pax_voted * 1.0)) * 100;
                    String str_count_percentage = String.format("%.2f", count_percentage) + "%";
                    tv_percentage.setText(str_count_percentage);

                    ll_assets.addView(asset_view);
                } else if (asset_info.type.equals(AppConstants.ASSET_VIDEO)) {
                    asset_view = inflater.inflate(R.layout.poll_detail_creator_video, ll_options, false);

                    RelativeLayout.LayoutParams parms = new RelativeLayout.LayoutParams(width, width);
                    RelativeLayout rl_asset = (RelativeLayout) asset_view.findViewById(R.id.rl_asset);
                    rl_asset.setLayoutParams(parms);

                    ImageView iv_asset = (ImageView) rl_asset.findViewById(R.id.iv_asset);
                    TextView tv_percentage = (TextView) rl_asset.findViewById(R.id.tv_percentage);
                    TextView tv_description = (TextView) rl_asset.findViewById(R.id.tv_description);

                    int i = asset_info.url.lastIndexOf(".");
                    String remote_image = asset_info.url.substring(0, i) + "_thumbnail.jpg";
                    Picasso.with(getActivity()).load(remote_image).error(R.drawable.with_video).resize(width, width).centerCrop().into(iv_asset);

                    tv_description.setText(asset_info.description);

                    double count_percentage = 0.0;
                    if (asset_info.count != 0)
                        count_percentage = (asset_info.count / (pax_voted * 1.0)) * 100;
                    String str_count_percentage = String.format("%.2f", count_percentage) + "%";
                    tv_percentage.setText(str_count_percentage);

                    iv_asset.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                Uri theURI = Uri.parse(asset_info.url);
                                Intent intent = new Intent();
                                intent.setAction(android.content.Intent.ACTION_VIEW);
                                intent.setDataAndType(theURI, URLConnection.guessContentTypeFromName(asset_info.url));
                                startActivity(intent);
                            } catch (Exception e) {
                                Toast.makeText(main_tab_activity, "Cannot process the media", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                    ll_assets.addView(asset_view);
                }
            } else {

                order_number += 1;

                // POLL RECIPIENT MODE

                if (asset_info.type.equals(AppConstants.ASSET_IMAGE)) {
                    asset_view = inflater.inflate(R.layout.poll_detail_recipient_image, ll_options, false);

                    RelativeLayout.LayoutParams parms = new RelativeLayout.LayoutParams(width, width);
                    final RelativeLayout rl_asset = (RelativeLayout) asset_view.findViewById(R.id.rl_asset);
                    rl_asset.setLayoutParams(parms);

                    LinearLayout ll_description = (LinearLayout) rl_asset.findViewById(R.id.ll_description);
                    ImageView iv_asset = (ImageView) rl_asset.findViewById(R.id.iv_asset);
                    ImageView iv_check = (ImageView) rl_asset.findViewById(R.id.iv_check);
                    TextView tv_number = (TextView) rl_asset.findViewById(R.id.tv_number);
                    TextView tv_description = (TextView) rl_asset.findViewById(R.id.tv_description);

                    iv_asset.setAdjustViewBounds(true);
                    iv_asset.setScaleType(ImageView.ScaleType.FIT_CENTER);

                    Picasso.with(getActivity()).load(asset_info.url).into(iv_asset);
                    tv_description.setText(asset_info.description);

                    if (voted_asset_id.equals(asset_info.id)) {
                        iv_check.setVisibility(View.VISIBLE);
                        tv_number.setVisibility(View.GONE);
                        ll_description.setBackgroundColor(getResources().getColor(R.color.threadr_red2));
                    } else {
                        iv_check.setVisibility(View.GONE);
                        tv_number.setVisibility(View.VISIBLE);
                        tv_number.setText(String.valueOf(order_number));
                    }

                    iv_asset.setOnClickListener(new DoubleClickListener() {

                        @Override
                        public void onSingleClick(View v) {
                            try {
                                Uri theURI = Uri.parse(asset_info.url);
                                Intent intent = new Intent();
                                intent.setAction(android.content.Intent.ACTION_VIEW);
                                intent.setData(theURI); // opening image via a browser is the most reliable
                                startActivity(intent);
                            } catch (Exception e) {
                                Toast.makeText(main_tab_activity, "Cannot process the media", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onDoubleClick(View v) {
                            if (!is_expired) {
                                // do the fancy trickery here
                                new DoVote(alert_id, contact_id, asset_info.id).execute();
                            }
                        }
                    });

                    ll_assets.addView(asset_view);
                } else if (asset_info.type.equals(AppConstants.ASSET_AUDIO)) {
                    asset_view = inflater.inflate(R.layout.poll_detail_recipient_audio, ll_options, false);

                    RelativeLayout rl_asset = (RelativeLayout) asset_view.findViewById(R.id.rl_asset);

                    LinearLayout ll_description = (LinearLayout) rl_asset.findViewById(R.id.ll_description);

                    ImageView iv_play = (ImageView) rl_asset.findViewById(R.id.iv_play);
                    ImageView iv_asset = (ImageView) rl_asset.findViewById(R.id.iv_asset);
                    ImageView iv_check = (ImageView) rl_asset.findViewById(R.id.iv_check);
                    TextView tv_number = (TextView) rl_asset.findViewById(R.id.tv_number);
                    TextView tv_description = (TextView) rl_asset.findViewById(R.id.tv_description);

                    tv_description.setText(asset_info.description);

                    if (voted_asset_id.equals(asset_info.id)) {
                        iv_check.setVisibility(View.VISIBLE);
                        tv_number.setVisibility(View.GONE);
                        ll_description.setBackgroundColor(getResources().getColor(R.color.threadr_red2));
                    } else {
                        iv_check.setVisibility(View.GONE);
                        tv_number.setVisibility(View.VISIBLE);
                        tv_number.setText(String.valueOf(order_number));
                    }

                    iv_play.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                Uri theURI = Uri.parse(asset_info.url);
                                Intent intent = new Intent();
                                intent.setAction(android.content.Intent.ACTION_VIEW);
                                intent.setDataAndType(theURI, URLConnection.guessContentTypeFromName(asset_info.url));
                                startActivity(intent);
                            } catch (Exception e) {
                                Toast.makeText(main_tab_activity, "Cannot process the media", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                    iv_asset.setOnClickListener(new DoubleClickListener() {

                        @Override
                        public void onSingleClick(View v) {
                            try {
                                Uri theURI = Uri.parse(asset_info.url);
                                Intent intent = new Intent();
                                intent.setAction(android.content.Intent.ACTION_VIEW);
                                intent.setDataAndType(theURI, URLConnection.guessContentTypeFromName(asset_info.url));
                                startActivity(intent);
                            } catch (Exception e) {
                                Toast.makeText(main_tab_activity, "Cannot process the media", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onDoubleClick(View v) {
                            if (!is_expired) {
                                // do the fancy trickery here
                                new DoVote(alert_id, contact_id, asset_info.id).execute();
                            }
                        }
                    });

                    ll_assets.addView(asset_view);
                } else if (asset_info.type.equals(AppConstants.ASSET_TEXT)) {
                    asset_view = inflater.inflate(R.layout.poll_detail_recipient_text, ll_options, false);

                    RelativeLayout rl_asset = (RelativeLayout) asset_view.findViewById(R.id.rl_asset);

                    LinearLayout ll_description = (LinearLayout) rl_asset.findViewById(R.id.ll_description);
                    ImageView iv_asset = (ImageView) rl_asset.findViewById(R.id.iv_asset);
                    ImageView iv_check = (ImageView) rl_asset.findViewById(R.id.iv_check);
                    TextView tv_number = (TextView) rl_asset.findViewById(R.id.tv_number);
                    TextView tv_description = (TextView) rl_asset.findViewById(R.id.tv_description);

                    tv_description.setText(asset_info.description);

                    if (voted_asset_id.equals(asset_info.id)) {
                        iv_check.setVisibility(View.VISIBLE);
                        tv_number.setVisibility(View.GONE);
                        ll_description.setBackgroundColor(getResources().getColor(R.color.threadr_red2));
                    } else {
                        iv_check.setVisibility(View.GONE);
                        tv_number.setVisibility(View.VISIBLE);
                        tv_number.setText(String.valueOf(order_number));
                    }

                    iv_asset.setOnClickListener(new DoubleClickListener() {

                        @Override
                        public void onSingleClick(View v) {
                        }

                        @Override
                        public void onDoubleClick(View v) {
                            if (!is_expired) {
                                // do the fancy trickery here
                                new DoVote(alert_id, contact_id, asset_info.id).execute();
                            }
                        }
                    });


                    ll_assets.addView(asset_view);
                } else if (asset_info.type.equals(AppConstants.ASSET_VIDEO)) {
                    asset_view = inflater.inflate(R.layout.poll_detail_recipient_video, ll_options, false);

                    RelativeLayout.LayoutParams parms = new RelativeLayout.LayoutParams(width, width);
                    RelativeLayout rl_asset = (RelativeLayout) asset_view.findViewById(R.id.rl_asset);
                    rl_asset.setLayoutParams(parms);

                    LinearLayout ll_description = (LinearLayout) rl_asset.findViewById(R.id.ll_description);
                    ImageView iv_asset = (ImageView) rl_asset.findViewById(R.id.iv_asset);
                    ImageView iv_check = (ImageView) rl_asset.findViewById(R.id.iv_check);
                    TextView tv_number = (TextView) rl_asset.findViewById(R.id.tv_number);
                    TextView tv_description = (TextView) rl_asset.findViewById(R.id.tv_description);

                    int i = asset_info.url.lastIndexOf(".");
                    String remote_image = asset_info.url.substring(0, i) + "_thumbnail.jpg";
                    Picasso.with(getActivity()).load(remote_image).error(R.drawable.with_video).resize(width, width).centerCrop().into(iv_asset);

                    tv_description.setText(asset_info.description);

                    if (voted_asset_id.equals(asset_info.id)) {
                        iv_check.setVisibility(View.VISIBLE);
                        tv_number.setVisibility(View.GONE);
                        ll_description.setBackgroundColor(getResources().getColor(R.color.threadr_red2));
                    } else {
                        iv_check.setVisibility(View.GONE);
                        tv_number.setVisibility(View.VISIBLE);
                        tv_number.setText(String.valueOf(order_number));
                    }

                    iv_asset.setOnClickListener(new DoubleClickListener() {

                        @Override
                        public void onSingleClick(View v) {
                            try {
                                Uri theURI = Uri.parse(asset_info.url);
                                Intent intent = new Intent();
                                intent.setAction(android.content.Intent.ACTION_VIEW);
                                intent.setDataAndType(theURI, URLConnection.guessContentTypeFromName(asset_info.url));
                                startActivity(intent);
                            } catch (Exception e) {
                                Toast.makeText(main_tab_activity, "Cannot process the media", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onDoubleClick(View v) {
                            if (!is_expired) {
                                // do the fancy trickery here
                                new DoVote(alert_id, contact_id, asset_info.id).execute();
                            }
                        }
                    });

                    ll_assets.addView(asset_view);
                }
            }
        }

        // UPDATE MESSAGE LIST
        ll_messages.removeAllViews();

        updateMessageList();

        // UPDATE MESSAGE LINE AND SEND BUTTON VISIBILITY
        if (!is_expired) {
            et_message.setVisibility(View.VISIBLE);
            bt_send.setVisibility(View.VISIBLE);
        } else {
            et_message.setVisibility(View.GONE);
            bt_send.setVisibility(View.GONE);
        }

    }

    private synchronized void updateMessageList() {

        ll_messages.removeAllViews();

        for (PollInfo.MessageInfo message_info : alert_info.messages) {

            View v;
            if (message_info.sender.equals("You"))
                v = inflater.inflate(R.layout.poll_detail_message_list_item_owned, ll_messages, false);
            else
                v = inflater.inflate(R.layout.poll_detail_message_list_item_not_owned, ll_messages, false);

            ImageView iv_image = (ImageView) v.findViewById(R.id.sender_icon);
            TextView tv_message = (TextView) v.findViewById(R.id.tv_message);
            TextView tv_sender_info = (TextView) v.findViewById(R.id.tv_sender_info);

            tv_message.setText(message_info.text);
            tv_sender_info.setText(message_info.sender + " " + convertISODateWithTime(message_info.time_stamp));


            Picasso.with(getActivity()).load(message_info.image_url).error(R.drawable.silhouette).into(iv_image);

            ll_messages.addView(v);
        }
    }

    class LoadAlertInfo extends AsyncTask<Void, Void, Void> {

        protected ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(main_tab_activity, "", "Loading info", true, false);
        }

        @Override
        protected Void doInBackground(Void... params) {
            PollDetailFragment.this.loadAlertInfo();
            PollDetailFragment.this.loadAlertMessages();
            return null;
        }

        @Override
        protected void onPostExecute(Void ret) {
            PollDetailFragment.this.updateDisplay();
            progressDialog.dismiss();
        }
    }

    class LoadAlertInfoSilently extends AsyncTask<Void, Void, Void> {

        //protected ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //progressDialog = ProgressDialog.show(main_tab_activity, "", "Loading info silently", true, false);
        }

        @Override
        protected Void doInBackground(Void... params) {

            PollDetailFragment.this.loadAlertInfo();
            PollDetailFragment.this.loadAlertMessages();

            return null;
        }

        @Override
        protected void onPostExecute(Void ret) {

            PollDetailFragment.this.updateDisplay();

            //progressDialog.dismiss();

            ui_update_handler.postDelayed(ui_update_payload, UI_UPDATE_INTERVAL_MILLISECONDS);

        }
    }

    class LoadAlertMessages extends AsyncTask<Void, Void, Void> {

        protected ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(main_tab_activity, "", "Refreshing message list", true, false);
        }

        @Override
        protected Void doInBackground(Void... params) {
            PollDetailFragment.this.loadAlertMessages();
            return null;
        }

        @Override
        protected void onPostExecute(Void ret) {
            PollDetailFragment.this.updateMessageList();
            progressDialog.dismiss();
        }
    }

    class AddMessage extends AsyncTask<Void, Void, Void> {

        protected ProgressDialog progressDialog;

        private String alert_id = "";
        private String message = "";

        private boolean is_ok = false;

        public AddMessage(String alert_id, String message) {
            this.alert_id = alert_id;
            this.message = message;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(main_tab_activity, "", "Adding message ...", true, false);
        }

        @Override
        protected Void doInBackground(Void... params) {

            String access_token = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_ACCESS_TOKEN, "UNKNOWN");
            Map<String, Object> result = ThreadrAPIAdapter.doAddMessage(access_token, alert_id, message);

            if (result != null && Boolean.parseBoolean(result.get("result").toString()))
                is_ok = true;

            return null;
        }

        @Override
        protected void onPostExecute(Void ret) {
            progressDialog.dismiss();

            if (is_ok) {
                et_message.setText("");
                new LoadAlertMessages().execute();
            } else
                Toast.makeText(main_tab_activity, "Unable to complete.", Toast.LENGTH_SHORT).show();
        }
    }

    class DoVote extends AsyncTask<Void, Void, Void> {

        protected ProgressDialog progressDialog;

        private String alert_id = "";
        private String alert_contact_id = "";
        private String asset_id = "";

        private boolean is_ok = false;

        public DoVote(String alert_id, String alert_contact_id, String asset_id) {
            this.alert_id = alert_id;
            this.alert_contact_id = alert_contact_id;
            this.asset_id = asset_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(main_tab_activity, "", "Voting ...", true, false);
        }

        @Override
        protected Void doInBackground(Void... params) {

            String access_token = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_ACCESS_TOKEN, "UNKNOWN");
            Map<String, Object> result = ThreadrAPIAdapter.doVote(access_token, alert_id, alert_contact_id, asset_id);

            if (result != null && Boolean.parseBoolean(result.get("result").toString()))
                is_ok = true;

            return null;
        }

        @Override
        protected void onPostExecute(Void ret) {
            progressDialog.dismiss();

            if (is_ok) {
                //alert_info.Reset();
                //SharedData.vote_data.reset();
                new LoadAlertInfo().execute();
            } else
                Toast.makeText(main_tab_activity, "Unable to complete.", Toast.LENGTH_SHORT).show();
        }
    }

    class DeleteAlert extends AsyncTask<Void, Void, Void> {

        protected ProgressDialog progressDialog;

        private String alert_id = "";

        private boolean is_ok = false;

        public DeleteAlert(String alert_id) {
            this.alert_id = alert_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(main_tab_activity, "", "Deleting thread ...", true, false);
        }

        @Override
        protected Void doInBackground(Void... params) {

            String access_token = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_ACCESS_TOKEN, "UNKNOWN");

            try {
                Map<String, Object> result = ThreadrAPIAdapter.doDeleteAlert(access_token, alert_id);

                if (result != null && Boolean.parseBoolean(result.get("result").toString()))
                    is_ok = true;

            } catch (Exception e) {
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void ret) {
            progressDialog.dismiss();

            if (is_ok) {
                SharedData.retrieved_polls.must_reload = true;
                main_tab_activity.onBackPressed();
            } else
                Toast.makeText(main_tab_activity, "Unable to complete.", Toast.LENGTH_SHORT).show();
        }
    }

    public String convertISODate(String iso_date) {
        String result = "";

        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date temp_date = formatter.parse(iso_date);

            Calendar cal = Calendar.getInstance();
            cal.setTime(temp_date);

            Locale current_locale = getResources().getConfiguration().locale;

            String month_name = cal.getDisplayName(Calendar.MONTH, Calendar.SHORT, current_locale);
            String day = Integer.toString(cal.get(Calendar.DAY_OF_MONTH));
            String year = Integer.toString(cal.get(Calendar.YEAR));

            result = month_name + " " + day + ", " + year;
        } catch (Exception e) {
        }

        return result;
    }

    public String convertISODateWithTime(String iso_date) {
        String result = "";

        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date temp_date = formatter.parse(iso_date);

            Locale current_locale = getResources().getConfiguration().locale;

            SimpleDateFormat local_format = new SimpleDateFormat("LLL dd, yyyy - hh:mm a", current_locale);

            result = local_format.format(temp_date).toString();


        } catch (Exception e) {
        }

        return result;
    }

    public String getDetailedTimeDifference(String iso_date) {
        String result = "";

        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));

            Date future_date = formatter.parse(iso_date);
            Date now_date = new Date();

            TimeUnit timeUnit = TimeUnit.SECONDS;

            long diffInMillies = future_date.getTime() - now_date.getTime();
            long s = timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);

            long days = s / (24 * 60 * 60);
            long rest = s - (days * 24 * 60 * 60);
            long std = rest / (60 * 60);
            long rest1 = rest - (std * 60 * 60);
            long min = rest1 / 60;
            long sec = s % 60;

            if (days > 0)
                result += days + " days ";
            if (std > 0)
                result += Long.toString(std) + " hours ";
            if (min > 0)
                result += Long.toString(min) + " minutes";

        } catch (Exception e) {
        }

        return result;
    }

    public String getShortTimeDifference(String iso_date) {
        String result = "";

        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));

            Date future_date = formatter.parse(iso_date);
            Date now_date = new Date();

            TimeUnit timeUnit = TimeUnit.SECONDS;

            long diffInMillies = future_date.getTime() - now_date.getTime();
            long s = timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);

            long days = s / (24 * 60 * 60);
            long rest = s - (days * 24 * 60 * 60);
            long std = rest / (60 * 60);
            long rest1 = rest - (std * 60 * 60);
            long min = rest1 / 60;
            long sec = s % 60;

            if (days > 0)
                result += Long.toString(days) + "d";
            else if (std > 0)
                result += Long.toString(std) + "h";
            else if (min > 0)
                result += Long.toString(min) + "m";

        } catch (Exception e) {
        }

        return result;
    }

    public String getAlertContactId() {
        String result = "";

        String my_user_id = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_ID, "UNKNOWN");

        Log.i(TAG + " MY USER ID", my_user_id);

        for (PollInfo.ContactInfo contact : alert_info.contacts) {

            String user_id = contact.user_id;
            String alert_contact_id = contact.contact_id;

            Log.i(TAG + " TEMP USER ID", user_id);

            if (user_id.equals(my_user_id)) {
                result = alert_contact_id;
                break;
            }
        }

        Log.i(TAG + "getAlertContactId()", result);

        return result;
    }

    private boolean isExpired(String expiration_date) {
        boolean result = false;

        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));

            Date future_date = formatter.parse(expiration_date);
            Date now_date = new Date();

            long diffInMillies = future_date.getTime() - now_date.getTime();

            if (diffInMillies < 0)
                result = true;

        } catch (Exception e) {
        }

        return result;
    }

    public float getTimeRemainingPercentage(String endtDate, String creationDate) {
        float result = 100.00f;

        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));

            Date end_date = formatter.parse(endtDate);
            Date posted_date = formatter.parse(creationDate);
            Date now_date = new Date();


            TimeUnit timeUnit = TimeUnit.SECONDS;

            long totalDurationMillis = end_date.getTime() - posted_date.getTime();
            long totalSeconds = timeUnit.convert(totalDurationMillis, TimeUnit.MILLISECONDS);

            long diffInMillies = end_date.getTime() - now_date.getTime();
            long currentSeconds = timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);

            Log.i("Arthur", "totalSeconds = " + totalSeconds + " currentSeconds = " + currentSeconds);

            if (currentSeconds < totalDurationMillis) {
                result = (float) currentSeconds / (float) totalSeconds;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public abstract class DoubleClickListener implements View.OnClickListener {

        private View v;

        private static final long DOUBLE_CLICK_TIME_DELTA = 400;//milliseconds

        boolean mHasDoubleClicked = false;
        long lastClickTime = 0;

        Handler myHandler = new Handler() {
            public void handleMessage(Message m) {
                if (!mHasDoubleClicked) {
                    //Toast.makeText(getApplicationContext(), "Single Click Event", Toast.LENGTH_SHORT).show();
                    onSingleClick(v);
                }

                mHasDoubleClicked = false;
            }
        };

        @Override
        public void onClick(View v) {

            this.v = v;

            long clickTime = System.currentTimeMillis();
            long elapsed = clickTime - lastClickTime;

            Log.i(TAG, "ELAPSED TIME BETWEEN CLICK " + Long.toString(elapsed));

            if (clickTime - lastClickTime < DOUBLE_CLICK_TIME_DELTA) {
                mHasDoubleClicked = true;
                onDoubleClick(v);
            } else {
                Message m = new Message();
                myHandler.sendMessageDelayed(m, DOUBLE_CLICK_TIME_DELTA);
            }

            lastClickTime = clickTime;
        }

        public abstract void onSingleClick(View v);

        public abstract void onDoubleClick(View v);
    }

    private boolean isOwned() {

        boolean result = false;

        String my_user_id = main_tab_activity.getSharedPreferences(AppConstants.SHARED_DATA, Context.MODE_PRIVATE).getString(AppConstants.USER_ID, "UNKNOWN");

        if (alert_info.sender_user_id.equals(my_user_id)) {
            result = true;
        }

        return result;
    }
}