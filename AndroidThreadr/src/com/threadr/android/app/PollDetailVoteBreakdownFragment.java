package com.threadr.android.app;

import android.graphics.Typeface;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by lilandra on 1/25/14.
 */
public class PollDetailVoteBreakdownFragment extends BaseFragment {

    private static String TAG = "PollDetailVoteBreakdownFragment";

    private LayoutInflater inflater;
    private LinearLayout ll_data;

    private Button bt_done;

    private boolean is_loaded = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.inflater = inflater;

        View V = inflater.inflate(R.layout.poll_detail_vote_breakdown_view, container, false);

        ll_data = (LinearLayout) V.findViewById(R.id.ll_data);

        bt_done = (Button) V.findViewById(R.id.bt_done);

        bt_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                main_tab_activity.onBackPressed();
            }
        });

        return V;
    }

    @Override
    public void onResume() {
        super.onResume();


        ll_data.removeAllViews();

        // create vote mapping

        HashMap<String, List<PollInfo.ContactInfo>> data = new HashMap<String, List<PollInfo.ContactInfo>>();

        for (PollInfo.ContactInfo contact : SharedData.vote_data.contacts) {

            String asset_id = contact.asset_id;
            String first_name = contact.first_name;
            String last_name = contact.last_name;

            if (!data.containsKey(asset_id))
                data.put(asset_id, new ArrayList<PollInfo.ContactInfo>());

            data.get(asset_id).add(contact);
        }

        LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        // create display for those who voted
        int count = 0;

        for (PollInfo.AssetInfo choice : SharedData.vote_data.assets) {
            count = count + 1;

            String id = choice.id;
            String description = choice.description;

            TextView tv_option = new TextView(main_tab_activity);
            tv_option.setLayoutParams(parms);

            tv_option.setText(String.format("#%d %s", count, description.trim()));
            tv_option.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
            tv_option.setTypeface(tv_option.getTypeface(), Typeface.BOLD);
            tv_option.setBackgroundColor(getResources().getColor(R.color.light_gray));
            tv_option.setPadding(20, 5, 0, 5);
            ll_data.addView(tv_option);

            if (data.containsKey(id) && data.get(id).size() > 0) {
                for (PollInfo.ContactInfo contact : data.get(id)) {

                    View contact_view = inflater.inflate(R.layout.vote_contact_item, ll_data, false);
                    ImageView iv_contact = (ImageView) contact_view.findViewById(R.id.iv_contact);
                    TextView tv_name = (TextView) contact_view.findViewById(R.id.tv_name);

                    Picasso.with(getActivity()).load(contact.image_url).error(R.drawable.silhouette).fit().centerCrop().into(iv_contact);
                    tv_name.setText(contact.first_name + " " + contact.last_name);

                    ll_data.addView(contact_view);
                }
            } else {
                TextView tv_name = new TextView(main_tab_activity);
                tv_name.setLayoutParams(parms);
                tv_name.setText("No Votes");
                tv_name.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
                tv_name.setPadding(20, 10, 0, 10);
                ll_data.addView(tv_name);
            }
        }


        // create display for those who didnt vote

        TextView tv_option_no_vote = new TextView(main_tab_activity);
        tv_option_no_vote.setLayoutParams(parms);
        tv_option_no_vote.setText("Pending Voters");
        tv_option_no_vote.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
        tv_option_no_vote.setTypeface(tv_option_no_vote.getTypeface(), Typeface.BOLD);
        tv_option_no_vote.setBackgroundColor(getResources().getColor(R.color.light_gray));
        tv_option_no_vote.setPadding(20, 5, 0, 5);
        ll_data.addView(tv_option_no_vote);

        for (PollInfo.ContactInfo contact : SharedData.vote_data.contacts) {

            if (contact.asset_id.equals("0")) {
                View contact_view = inflater.inflate(R.layout.vote_contact_item, ll_data, false);
                ImageView iv_contact = (ImageView) contact_view.findViewById(R.id.iv_contact);
                TextView tv_name = (TextView) contact_view.findViewById(R.id.tv_name);

                Picasso.with(getActivity()).load(contact.image_url).error(R.drawable.silhouette).fit().centerCrop().into(iv_contact);
                tv_name.setText(contact.first_name + " " + contact.last_name);

                ll_data.addView(contact_view);
            }
        }

    }
}