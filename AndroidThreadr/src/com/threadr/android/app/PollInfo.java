package com.threadr.android.app;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lilandra on 1/18/14.
 */

public class PollInfo {
    public String id = "";
    public String original_id = "";
    public String sender_user_id = "";
    public String sender_first_name = "";
    public String sender_last_name = "";
    public String sender_image_url = "";
    public String post_date = "";
    public String expiration_date = "";
    public String duration = "";
    public String description = "";
    public double latitude = 0.0;
    public double longitude = 0.0;
    public String location_name = "";
    public int comments_count = 0;
    public List<ContactInfo> contacts = new ArrayList<ContactInfo>();
    public List<AssetInfo> assets = new ArrayList<AssetInfo>();
    //public List<ChoiceInfo> choices = new ArrayList<ChoiceInfo>();
    public List<MessageInfo> messages = new ArrayList<MessageInfo>();
    public AssetInfo media = new AssetInfo();

    public void Reset() {
        id = "";
        original_id = "";
        sender_user_id = "";
        sender_first_name = "";
        sender_last_name = "";
        sender_image_url = "";
        post_date = "";
        expiration_date = "";
        duration = "";
        description = "";
        latitude = 0.0;
        longitude = 0.0;
        location_name = "";
        comments_count = 0;
        contacts.clear();
        assets.clear();
        //choices.clear();
        messages.clear();
        media.clear();
    }

    public static class ContactInfo {
        public String user_id = "";
        public String contact_id = "";
        public String first_name = "";
        public String last_name = "";
        public String image_url = "";
        public String asset_id = "";
    }

    public static class AssetInfo {
        public String id = "";
        public String type = "";
        public String url = "";
        public String description = "";
        public int count = 0;

        public void clear(){
            id = "";
            type = "";
            url = "";
            description = "";
            count = 0;
        }
    }

    /*
    public static class ChoiceInfo {

        public String id = "";
        public String description = "";
        public int count = 0;
    }
    */

    public static class MessageInfo {
        public String text = "";
        public String sender = "";
        public String time_stamp = "";
        public String image_url = "";
    }
}
