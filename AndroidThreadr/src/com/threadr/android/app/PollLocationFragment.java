package com.threadr.android.app;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by lilandra on 3/2/14.
 */
public class PollLocationFragment extends BaseFragment {

    private static final String TAG = "PollLocationFragment";

    //private static final LatLng CEBU = new LatLng(10.3167200, 123.8907100);

    private double latitude = 0.0;
    private double longitude = 0.0;
    private String location_name = "";

    private MapView mv_map;
    private Button bt_done;
    private TextView tv_location;
    private GoogleMap map;

    private boolean is_loaded = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View V = inflater.inflate(R.layout.poll_location_view, container, false);

        bt_done = (Button) V.findViewById(R.id.bt_done);
        tv_location = (TextView) V.findViewById(R.id.tv_location);


        try {
            MapsInitializer.initialize(getActivity());
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }


        //MapsInitializer.initialize(getActivity()); // we will check the presence of GooglePlayServices before this Fragment is created

        mv_map = (MapView) V.findViewById(R.id.mapView);
        mv_map.onCreate(savedInstanceState);

        map = mv_map.getMap();

        bt_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                main_tab_activity.onBackPressed();
            }
        });

        return V;
    }

    @Override
    public void onResume() {
        super.onResume();
        mv_map.onResume();

        if (!is_loaded) {
            is_loaded = true;

            map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

            map.clear();

            map.getUiSettings().setCompassEnabled(true);
            map.getUiSettings().setZoomControlsEnabled(true);
            map.getUiSettings().setMyLocationButtonEnabled(true);

            if (arguments != null) {
                latitude = arguments.getDouble(AppConstants.LOC_LATITUDE);
                longitude = arguments.getDouble(AppConstants.LOC_LONGITUDE);
                location_name = arguments.getString(AppConstants.LOC_NAME);
            }

            tv_location.setText(location_name);


            LatLng geo_loc = new LatLng(latitude, longitude);

            //geo_loc = CEBU;

            MarkerOptions m_marker = new MarkerOptions().position(geo_loc).title(location_name);

            map.addMarker(m_marker);

            map.animateCamera(CameraUpdateFactory.newLatLngZoom(geo_loc, 12.0f));
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mv_map.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mv_map.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mv_map.onLowMemory();
    }
}