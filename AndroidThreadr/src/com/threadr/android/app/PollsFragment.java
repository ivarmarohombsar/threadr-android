package com.threadr.android.app;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.threadr.android.app.Utils.TextFontHelper;

public class PollsFragment extends BaseFragment {

    private static String TAG = "PollsFragment";

    private final int TAB_ACTIVE_POLLS = 1;
    private final int TAB_FINISHED_POLLS = 2;

    private FragmentManager fm;
    private ActivePollsFragment fragment_active_polls;
    private FinishedPollsFragment fragment_finished_polls;

    private int current_mode = -1;

    private Button bt_active_polls;
    private Button bt_finished_polls;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View V = inflater.inflate(R.layout.polls_tab_view, container, false);

        fm = main_tab_activity.getSupportFragmentManager();

        fragment_active_polls = new ActivePollsFragment();
        fragment_finished_polls = new FinishedPollsFragment();

        bt_active_polls = (Button) V.findViewById(R.id.bt_active_polls);
        bt_active_polls.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                current_mode = TAB_ACTIVE_POLLS;

                FragmentTransaction transaction = fm.beginTransaction();
                transaction.replace(R.id.contentFragment, fragment_active_polls);
                transaction.commit();

                setTabPolls(current_mode);
            }
        });

        bt_finished_polls = (Button) V.findViewById(R.id.bt_finished_polls);
        bt_finished_polls.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                current_mode = TAB_FINISHED_POLLS;

                FragmentTransaction transaction = fm.beginTransaction();
                transaction.replace(R.id.contentFragment, fragment_finished_polls);
                transaction.commit();

                setTabPolls(current_mode);
            }
        });

        Button bt_new_poll = (Button) V.findViewById(R.id.bt_new_poll);
        bt_new_poll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // make sure we reset our shared alert payload

                SharedData.alert_payload.resetData();

                WhoToAskFragment fragment = new WhoToAskFragment();

                main_tab_activity.pushFragments(AppConstants.TAB_POLLS, fragment, true, true);
            }
        });

        // setup others

        //TextView tv_title = (TextView) V.findViewById(R.id.tv_title);
        //TextFontHelper.setFontAndText(getActivity(), tv_title, null);

        setTabPolls(current_mode);

        return V;
    }

    private void setTabPolls(int current_mode){
        if(current_mode == TAB_ACTIVE_POLLS){
            bt_active_polls.setBackgroundResource(R.drawable.rounded_rectangle_gray_left);
            bt_active_polls.setTextColor(Color.WHITE);

            bt_finished_polls.setBackgroundResource(R.drawable.rounded_rectangle_gray_outline_right);
            bt_finished_polls.setTextColor(getResources().getColor(R.color.threadr_gray_1));
        }
        else if(current_mode == TAB_FINISHED_POLLS){
            bt_finished_polls.setBackgroundResource(R.drawable.rounded_rectangle_gray_right);
            bt_finished_polls.setTextColor(Color.WHITE);

            bt_active_polls.setBackgroundResource(R.drawable.rounded_rectangle_gray_outline_left);
            bt_active_polls.setTextColor(getResources().getColor(R.color.threadr_gray_1));
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        //Toast.makeText(getActivity(),"PollsFragment",Toast.LENGTH_SHORT).show();
        if (current_mode == -1)
            current_mode = TAB_ACTIVE_POLLS;


        updateDisplay();
    }

    private void updateDisplay() {
        switch (current_mode) {
            case TAB_ACTIVE_POLLS: {
                //Toast.makeText(getActivity(),"PollsFragment - Active",Toast.LENGTH_SHORT).show();
                bt_active_polls.performClick();
            }
            break;
            case TAB_FINISHED_POLLS: {
                //Toast.makeText(getActivity(),"PollsFragment - Finished",Toast.LENGTH_SHORT).show();
                bt_finished_polls.performClick();
            }
            break;
        }
    }
}