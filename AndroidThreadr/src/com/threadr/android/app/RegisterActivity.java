package com.threadr.android.app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;

import java.util.Map;
import java.util.StringTokenizer;

/**
 * Created by lilandra on 1/3/14.
 */
public class RegisterActivity extends Activity {

    private static String TAG = "RegisterActivity";

    private EditText et_name;
    private EditText et_email;
    private EditText et_phone_number;
    private EditText et_password;
    private EditText et_password_confirm;
    private Button bt_register;
    private TextView tv_have_account_login;

    private ImageButton registerFacebookImageButton;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_view);

        // this adjust the keyboard to not cover the edit text
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        et_name = (EditText) findViewById(R.id.et_name);
        et_email = (EditText) findViewById(R.id.et_email);
        et_phone_number = (EditText) findViewById(R.id.et_phone_number);
        et_password = (EditText) findViewById(R.id.et_password);
        et_password_confirm = (EditText) findViewById(R.id.et_password_confirm);
        bt_register = (Button) findViewById(R.id.bt_register);
        tv_have_account_login = (TextView) findViewById(R.id.tv_have_account_login);

        String create_account = "<u>Login</u>";
        tv_have_account_login.setText(Html.fromHtml(create_account));

        et_name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    String s = et_name.getText().toString();
                    String capitalized_line = capitalize(s);
                    et_name.setText(capitalized_line);
                }
            }
        });

        bt_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // hide keyboard
                InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                final String name = et_name.getText().toString().trim();
                final String email = et_email.getText().toString().trim();
                final String phone_number = et_phone_number.getText().toString().trim();
                final String password = et_password.getText().toString().trim();
                final String password_confirm = et_password_confirm.getText().toString().trim();

                new Register(email, name, password, phone_number).execute();
            }
        });

        tv_have_account_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        registerFacebookImageButton = (ImageButton) findViewById(R.id.registerFacebookImageButton);
        registerFacebookImageButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                intent.putExtra(LoginActivity.FBREGISTER, true);
                startActivity(intent);
            }
        });

    }

    private String capitalize(String line) {
        StringTokenizer token = new StringTokenizer(line);
        String CapLine = "";

        boolean is_first_word = true;

        while (token.hasMoreElements()) {
            String tok = token.nextToken().toString();

            if (is_first_word) {
                if (tok.length() == 1)
                    CapLine += Character.toUpperCase(tok.charAt(0));
                else
                    CapLine += Character.toUpperCase(tok.charAt(0)) + tok.substring(1);
            } else {
                if (tok.length() == 1)
                    CapLine += " " + Character.toUpperCase(tok.charAt(0));
                else
                    CapLine += " " + Character.toUpperCase(tok.charAt(0)) + tok.substring(1);
            }


            is_first_word = false;
        }
        return CapLine;
    }

    class Register extends AsyncTask<Void, Void, Void> {

        protected ProgressDialog progressDialog;

        private String email = "";
        private String name = "";
        private String password = "";
        private String phone_number = "";
        private String error_msg = "";

        private boolean is_ok = false;

        public Register(String email, String name, String password, String phone_number) {
            this.email = email;
            this.name = name;
            this.password = password;
            this.phone_number = phone_number;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(RegisterActivity.this, "", "Registering ...", true, false);
        }

        @Override
        protected Void doInBackground(Void... params) {

            Map<String, Object> result = ThreadrAPIAdapter.doRegister(email, name, "", password, phone_number);

            if (result != null && Boolean.parseBoolean(result.get("result").toString()))
                is_ok = true;
            else
                error_msg = result.get("errorMessage").toString();

            return null;
        }

        @Override
        protected void onPostExecute(Void ret) {
            progressDialog.dismiss();

            if (is_ok)
                finish();
            else if (!error_msg.equals(""))
                Toast.makeText(RegisterActivity.this, error_msg, Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(RegisterActivity.this, "Unable to complete.", Toast.LENGTH_SHORT).show();
        }
    }

}




























