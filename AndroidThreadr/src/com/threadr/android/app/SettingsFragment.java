package com.threadr.android.app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.*;
import com.facebook.Session;
import com.squareup.picasso.Picasso;
import com.threadr.android.app.Utils.FileUtils;
import com.threadr.android.app.Utils.PreferenceHelper;

import java.util.Map;

public class SettingsFragment extends BaseFragment {

    private static final String TAG = "SettingsFragment";

    private static final int SELECT_IMAGE = 1;

    private Button bt_upload_photo;
    private Button bt_edit_password;
    private ImageView iv_save_changes;
    private ToggleButton tb_facebook;
    private ToggleButton tb_email_notification;

    private EditText et_name;
    private EditText et_email;
    private EditText et_phone_number;

    private ImageView iv_avatar;

    private boolean facebook_login_status = false;
    private boolean previous_email_notification_status = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View V = inflater.inflate(R.layout.settings_tab_view, container, false);

        // this adjust the keyboard to not cover the edit text
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        et_name = (EditText) V.findViewById(R.id.et_name);
        et_email = (EditText) V.findViewById(R.id.et_email);
        et_phone_number = (EditText) V.findViewById(R.id.et_phone_number);

        Button bt_logout = (Button) V.findViewById(R.id.bt_logout);
        //bt_reset_password = (Button) V.findViewById(R.id.bt_reset_password);
        bt_upload_photo = (Button) V.findViewById(R.id.bt_upload_photo);
        bt_edit_password = (Button) V.findViewById(R.id.bt_edit_password);
        iv_save_changes = (ImageView) V.findViewById(R.id.iv_save_changes);
        tb_facebook = (ToggleButton) V.findViewById(R.id.tb_facebook);
        tb_email_notification = (ToggleButton) V.findViewById(R.id.tb_email_notifications);

        iv_avatar = (ImageView) V.findViewById(R.id.iv_avatar);


        bt_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // get welcome screen toggle
                boolean skipWelcome = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getBoolean(AppConstants.SKIP_WELCOME, false);

                // clear all shared values
                //SharedPreferences shared_data = getActivity().getSharedPreferences(AppConstants.SHARED_DATA, Context.MODE_PRIVATE);
                //SharedPreferences.Editor editor = shared_data.edit();
                //editor.clear();
                //editor.commit();
                PreferenceHelper.getInstance(getActivity().getApplicationContext()).clear();

                //persist welcome screen toggle
                PreferenceHelper.getInstance(getActivity().getApplicationContext()).setBoolean(AppConstants.SKIP_WELCOME, skipWelcome);

                //persist facebook login option
                //shared_data = getActivity().getSharedPreferences(AppConstants.SHARED_DATA, Context.MODE_PRIVATE);
                //editor = shared_data.edit();
                //editor.putBoolean(AppConstants.USER_ALLOW_FACEBOOK_LOGIN, facebook_login_status);
                //editor.commit();
                PreferenceHelper.getInstance(getActivity().getApplicationContext()).setBoolean(AppConstants.USER_ALLOW_FACEBOOK_LOGIN, facebook_login_status);

                //We also logout on FB
                if (Session.getActiveSession() != null) {
                    //Toast.makeText(getActivity(),"EEK FB SESSION STILL EXISTS", Toast.LENGTH_SHORT).show();

                    /*Session.getActiveSession().addCallback(new StatusCallback() {
                        @Override
						public void call(Session session, SessionState state, Exception exception) {
							if(session == null){ logout(); }
							else if(session.isClosed()) { logout(); }
						}
					});
					*/

                    // regardless if a session is closed or not, lets clear it for good measure
                    Session.getActiveSession().closeAndClearTokenInformation();

                    logout();
                } else {
                    logout();
                }
            }
        });

        bt_upload_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                main_tab_activity.startActivityForResult(Intent.createChooser(intent, "Select Image"), SELECT_IMAGE);
            }
        });

        bt_edit_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditPasswordFragment fragment = new EditPasswordFragment();
                main_tab_activity.pushFragments(AppConstants.TAB_SETTINGS, fragment, true, true);
            }
        });

        iv_save_changes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String name = et_name.getText().toString().trim();
                String email = et_email.getText().toString().trim();
                String phone_number = et_phone_number.getText().toString().trim();

                new SaveChanges(name, email, phone_number).execute();
            }
        });

        tb_facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                facebook_login_status = !facebook_login_status;
                tb_facebook.setChecked(facebook_login_status);

                //SharedPreferences shared_data = getActivity().getSharedPreferences(AppConstants.SHARED_DATA, Context.MODE_PRIVATE);
                //SharedPreferences.Editor editor = shared_data.edit();
                //editor.putBoolean(AppConstants.USER_ALLOW_FACEBOOK_LOGIN, facebook_login_status);
                //editor.commit();

                PreferenceHelper.getInstance(getActivity().getApplicationContext()).setBoolean(AppConstants.USER_ALLOW_FACEBOOK_LOGIN, facebook_login_status);
            }
        });

        tb_email_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new ToggleEmailNotification().execute();
            }
        });

        return V;
    }

    @Override
    public void onResume() {
        super.onResume();

        updateFromSavedValues();
    }

    private void logout() {
        main_tab_activity.logout_mode = true;
        main_tab_activity.onBackPressed();
    }

    private void updateFromSavedValues() {
        //String profile_image_url = getActivity().getSharedPreferences(AppConstants.SHARED_DATA, Context.MODE_PRIVATE).getString(AppConstants.USER_PROFILE_IMAGE, "UNKNOWN");
        //String first_name = getActivity().getSharedPreferences(AppConstants.SHARED_DATA, Context.MODE_PRIVATE).getString(AppConstants.USER_FIRST_NAME, "UNKNOWN");
        //String last_name = getActivity().getSharedPreferences(AppConstants.SHARED_DATA, Context.MODE_PRIVATE).getString(AppConstants.USER_LAST_NAME, "UNKNOWN");
        //String email = getActivity().getSharedPreferences(AppConstants.SHARED_DATA, Context.MODE_PRIVATE).getString(AppConstants.USER_EMAIL, "UNKNOWN");
        //String phone_number = getActivity().getSharedPreferences(AppConstants.SHARED_DATA, Context.MODE_PRIVATE).getString(AppConstants.USER_PHONE_NUMBER, "UNKNOWN");
        //boolean email_notification = getActivity().getSharedPreferences(AppConstants.SHARED_DATA, Context.MODE_PRIVATE).getBoolean(AppConstants.USER_EMAIL_NOTIFICATION, false);
        //boolean allow_fb_login = getActivity().getSharedPreferences(AppConstants.SHARED_DATA, Context.MODE_PRIVATE).getBoolean(AppConstants.USER_ALLOW_FACEBOOK_LOGIN, true);

        String profile_image_url = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_PROFILE_IMAGE, "UNKNOWN");
        String first_name = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_FIRST_NAME, "UNKNOWN");
        String last_name = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_LAST_NAME, "UNKNOWN");
        String email = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_EMAIL, "UNKNOWN");
        String phone_number = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_PHONE_NUMBER, "UNKNOWN");
        boolean email_notification = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getBoolean(AppConstants.USER_EMAIL_NOTIFICATION, false);
        boolean allow_fb_login = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getBoolean(AppConstants.USER_ALLOW_FACEBOOK_LOGIN, true);

        Picasso.with(getActivity()).load(profile_image_url).error(R.drawable.silhouette).fit().centerCrop().into(iv_avatar);

        et_name.setText(first_name + " " + last_name);
        et_email.setText(email);
        et_phone_number.setText(phone_number);
        tb_facebook.setChecked(allow_fb_login);
        tb_email_notification.setChecked(email_notification);

        facebook_login_status = allow_fb_login;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //Toast.makeText(getActivity(), Integer.toString(requestCode), Toast.LENGTH_SHORT).show();
        if (resultCode == Activity.RESULT_OK) {

            Uri selectedUri = data.getData();

            String image_file_path = "";

            try {
                //image_file_path = getRealPathFromURI(selectedUri);
                image_file_path = FileUtils.getPath(getActivity().getApplicationContext(), selectedUri);
            } catch (Exception e) {
                // so its not registered in the media store..thats fine. use the original
                image_file_path = selectedUri.getPath();
            }

            Log.i(TAG, image_file_path);

            new UploadAvatar(image_file_path).execute();
        }
    }

    private String getRealPathFromURI(Uri contentUri) {
        String path = null;
        String[] proj = {MediaStore.MediaColumns.DATA};
        Cursor cursor = main_tab_activity.getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            path = cursor.getString(column_index);
        }
        cursor.close();
        return path;
    }

    class UploadAvatar extends AsyncTask<Void, Void, Void> {

        protected ProgressDialog progressDialog;

        private String image_file_path = "";
        private String profile_image_url = "";
        private boolean is_ok = false;

        public UploadAvatar(String image_file_path) {
            this.image_file_path = image_file_path;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            progressDialog = new ProgressDialog(getActivity());
//            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//            progressDialog.setMessage("Uploading Avatar ...");
//            progressDialog.setIndeterminate(true);
//            progressDialog.setCancelable(false);
//            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            String access_token = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_ACCESS_TOKEN, "UNKNOWN");
            String temp_file_name = main_tab_activity.getCacheDir() + "/temp.png";
            Map<String, Object> result = ThreadrAPIAdapter.doUploadAvatar(access_token, image_file_path, temp_file_name);

            if (result != null && Boolean.parseBoolean(result.get("result").toString())) {
                profile_image_url = result.get("filename").toString();

                //SharedPreferences shared_data = getActivity().getSharedPreferences(AppConstants.SHARED_DATA, Context.MODE_PRIVATE);
                //SharedPreferences.Editor editor = shared_data.edit();
                //editor.putString(AppConstants.USER_PROFILE_IMAGE, profile_image_url);
                //editor.commit();

                PreferenceHelper.getInstance(getActivity().getApplicationContext()).setString(AppConstants.USER_PROFILE_IMAGE, profile_image_url);

                is_ok = true;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void ret) {
//            progressDialog.dismiss();

            if (is_ok) {
                Picasso.with(getActivity()).load(profile_image_url).fit().centerCrop().into(iv_avatar);
            } else
                Toast.makeText(main_tab_activity, "Unable to complete.", Toast.LENGTH_SHORT).show();
        }
    }

    class SaveChanges extends AsyncTask<Void, Void, Void> {

        protected ProgressDialog progressDialog;

        private String name = "";
        private String email = "";
        private String phone_number = "";
        private boolean is_ok = false;

        public SaveChanges(String name, String email, String phone_number) {
            this.name = name;
            this.email = email;
            this.phone_number = phone_number;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setMessage("Saving Changes ...");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            String access_token = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_ACCESS_TOKEN, "UNKNOWN");

            try {

                Map<String, Object> result = ThreadrAPIAdapter.doEditProfile(access_token, email, name, "", phone_number);

                if (result != null && Boolean.parseBoolean(result.get("result").toString())) {

                    // update shared preferences
                    //SharedPreferences shared_data = getActivity().getSharedPreferences(AppConstants.SHARED_DATA, Context.MODE_PRIVATE);
                    //SharedPreferences.Editor editor = shared_data.edit();
                    //editor.putString(AppConstants.USER_FIRST_NAME, name);
                    //editor.putString(AppConstants.USER_EMAIL, email);
                    //editor.putString(AppConstants.USER_PHONE_NUMBER, phone_number);
                    //editor.commit();

                    PreferenceHelper.getInstance(getActivity().getApplicationContext()).setString(AppConstants.USER_FIRST_NAME, name);
                    PreferenceHelper.getInstance(getActivity().getApplicationContext()).setString(AppConstants.USER_EMAIL, email);
                    PreferenceHelper.getInstance(getActivity().getApplicationContext()).setString(AppConstants.USER_PHONE_NUMBER, phone_number);

                    is_ok = true;
                }

            } catch (Exception e) {
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void ret) {
            progressDialog.dismiss();

            if (is_ok) {
                // update ui
                //String first_name = getActivity().getSharedPreferences(AppConstants.SHARED_DATA, Context.MODE_PRIVATE).getString(AppConstants.USER_FIRST_NAME, "UNKNOWN");
                //String last_name = getActivity().getSharedPreferences(AppConstants.SHARED_DATA, Context.MODE_PRIVATE).getString(AppConstants.USER_LAST_NAME, "UNKNOWN");
                //String email = getActivity().getSharedPreferences(AppConstants.SHARED_DATA, Context.MODE_PRIVATE).getString(AppConstants.USER_EMAIL, "UNKNOWN");
                //String phone_number = getActivity().getSharedPreferences(AppConstants.SHARED_DATA, Context.MODE_PRIVATE).getString(AppConstants.USER_PHONE_NUMBER, "UNKNOWN");

                String first_name = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_FIRST_NAME, "UNKNOWN");
                String last_name = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_LAST_NAME, "UNKNOWN");
                String email = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_EMAIL, "UNKNOWN");
                String phone_number = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_PHONE_NUMBER, "UNKNOWN");

                et_name.setText(first_name + " " + last_name);
                et_email.setText(email);
                et_phone_number.setText(phone_number);

            } else
                Toast.makeText(main_tab_activity, "Unable to complete.", Toast.LENGTH_SHORT).show();
        }
    }

    class ToggleEmailNotification extends AsyncTask<Void, Void, Void> {

        protected ProgressDialog progressDialog;

        private boolean is_ok = false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setMessage("Toggling email notification ...");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            String access_token = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getString(AppConstants.USER_ACCESS_TOKEN, "UNKNOWN");

            try {

                Map<String, Object> result = ThreadrAPIAdapter.doToggleEmailNotification(access_token);

                if (result != null && Boolean.parseBoolean(result.get("result").toString())) {

                    // update shared preferences
                    //SharedPreferences shared_data = getActivity().getSharedPreferences(AppConstants.SHARED_DATA, Context.MODE_PRIVATE);
                    //SharedPreferences.Editor editor = shared_data.edit();
                    //editor.putBoolean(AppConstants.USER_EMAIL_NOTIFICATION, !previous_email_notification_status);
                    //editor.commit();

                    PreferenceHelper.getInstance(getActivity().getApplicationContext()).setBoolean(AppConstants.USER_EMAIL_NOTIFICATION, !previous_email_notification_status);

                    previous_email_notification_status = !previous_email_notification_status;
                    is_ok = true;
                }

            } catch (Exception e) {
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void ret) {
            progressDialog.dismiss();

            if (is_ok) {
                // update ui
                //boolean email_notification = getActivity().getSharedPreferences(AppConstants.SHARED_DATA, Context.MODE_PRIVATE).getBoolean(AppConstants.USER_EMAIL_NOTIFICATION, false);
                boolean email_notification = PreferenceHelper.getInstance(getActivity().getApplicationContext()).getBoolean(AppConstants.USER_EMAIL_NOTIFICATION, false);
                tb_email_notification.setChecked(email_notification);
            } else
                Toast.makeText(main_tab_activity, "Unable to complete.", Toast.LENGTH_SHORT).show();
        }
    }

}