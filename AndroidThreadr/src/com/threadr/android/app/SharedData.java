package com.threadr.android.app;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by lilandra on 1/19/14.
 */
public class SharedData {

    public static class AlertPayload {
        public List<String> target_group_ids = new ArrayList<String>();
        public List<String> target_contact_ids = new ArrayList<String>();
        public List<String> choices = new ArrayList<String>();
        public List<PollInfo.AssetInfo> assets = new ArrayList<PollInfo.AssetInfo>();
        public String description = "";
        //public String asset_type = "";
        public int duration = -1;
        public String alert_id = "";
        public double latitude = 0.0;
        public double longitude = 0.0;
        public PollInfo.AssetInfo media = new PollInfo.AssetInfo();

        public void resetTargetGroupIds() {
            target_group_ids.clear();
        }

        public void resetTargetIContactIds() {
            target_contact_ids.clear();
        }

        public void resetAssets() {
            assets.clear();
        }

        public void resetChoices() {
            choices.clear();
        }

        public void resetData() {
            description = "";
            alert_id = "";
            duration = -1;
            //asset_type = "";
            target_contact_ids.clear();
            target_group_ids.clear();
            choices.clear();
            assets.clear();
            media.clear();
        }
    }

    public static class AlertAmmendmentPayload {
        public PollInfo old_alert = new PollInfo();
        public List<String> target_group_ids = new ArrayList<String>();
        public List<String> target_contact_ids = new ArrayList<String>();
        public List<String> choices = new ArrayList<String>();
        public List<PollInfo.AssetInfo> assets = new ArrayList<PollInfo.AssetInfo>();
        public String description = "";
        //public String asset_type = "";
        public int duration = -1;
        public String alert_id = "";
        public PollInfo.AssetInfo media = new PollInfo.AssetInfo();

        public void resetTargetGroupIds() {
            target_group_ids.clear();
        }

        public void resetTargetIContactIds() {
            target_contact_ids.clear();
        }

        public void resetAssets() {
            assets.clear();
        }

        public void resetChoices() {
            choices.clear();
        }

        public void resetData() {
            old_alert.Reset();
            description = "";
            alert_id = "";
            duration = -1;
            //asset_type = "";
            target_contact_ids.clear();
            target_group_ids.clear();
            choices.clear();
            assets.clear();
            media.clear();
        }
    }

    public static class VoteData {
        public List<PollInfo.AssetInfo> assets = new ArrayList<PollInfo.AssetInfo>();
        public List<PollInfo.ContactInfo> contacts = new ArrayList<PollInfo.ContactInfo>();

        public void reset() {
            assets.clear();
            contacts.clear();
        }
    }

    public static AlertPayload alert_payload = new AlertPayload();
    public static AlertAmmendmentPayload alert_ammendment_payload = new AlertAmmendmentPayload();
    public static VoteData vote_data = new VoteData();
    public static RetrievedPolls retrieved_polls = new RetrievedPolls();

    public static class RetrievedPolls {
        public boolean must_reload = true;
        public List<PollInfo> polls = new ArrayList<PollInfo>();
        public HashMap<String, List<PollInfo.MessageInfo>> messages = new HashMap<String, List<PollInfo.MessageInfo>>();

        public void reset() {
            must_reload = true;
            polls.clear();
            messages.clear();
        }
    }

    public static boolean ok_to_exit = false;
}
