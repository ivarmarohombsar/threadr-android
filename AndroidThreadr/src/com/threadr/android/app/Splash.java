package com.threadr.android.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

/**
 * Created by lilandra on 1/3/14.
 */
public class Splash extends Activity {

    private class SplashHandler extends Handler {

        public void handleMessage(Message msg) {
            // switch to identify the message by its code
            switch (msg.what) {
                default:
                case 0:
                    super.handleMessage(msg);

                    Intent intent = new Intent();
                    intent.setClass(Splash.this, LoginActivity.class);
                    startActivity(intent);

                    Splash.this.finish();
            }
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        SplashHandler mHandler = new SplashHandler();
        Message msg = new Message();

        msg.what = 0;

        mHandler.sendMessageDelayed(msg, 3000);
    }
}