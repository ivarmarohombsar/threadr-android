package com.threadr.android.app;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.Toast;
import com.androidquery.util.AQUtility;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.threadr.android.app.Utils.PreferenceHelper;
import com.threadr.android.app.Welcome.WelcomeActivity;

import java.util.HashMap;
import java.util.Stack;

public class TabMain extends FragmentActivity {

    private TabHost mTabHost;

    private HashMap<String, Stack<Fragment>> mStacks;

    private String mCurrentTab;
    public boolean logout_mode = false;

    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(final Session session, final SessionState state, final Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };

    /*Comes here when user switch tab, or we do programmatically*/
    TabHost.OnTabChangeListener listener = new TabHost.OnTabChangeListener() {
        public void onTabChanged(String tabId) {
        /*Set current tab..*/
            mCurrentTab = tabId;

            if (mStacks.get(tabId).size() == 0) {
          /*
           *    First time this tab is selected. So add first fragment of that tab.
           *    Dont need animation, so that argument is false.
           *    We are adding a new fragment which is not present in stack. So add to stack is true.
           */
                if (tabId.equals(AppConstants.TAB_CONTACTS)) {
//                    pushFragments(tabId, new ContactsFragment(), false, true);
                    pushFragments(tabId, new ContactGroupsFragment(), false, true);
                } else if (tabId.equals(AppConstants.TAB_POLLS)) {
                    pushFragments(tabId, new PollsFragment(), false, true);
                } else if (tabId.equals(AppConstants.TAB_SETTINGS)) {
                    pushFragments(tabId, new SettingsFragment(), false, true);
                }

            } else {
          /*
           *    We are switching tabs, and target tab is already has atleast one fragment.
           *    No need of animation, no need of stack pushing. Just show the target fragment
           */
                pushFragments(tabId, mStacks.get(tabId).lastElement(), false, false);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tabmain);

        // set orientation to portrait
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mStacks = new HashMap<String, Stack<Fragment>>();
        mStacks.put(AppConstants.TAB_CONTACTS, new Stack<Fragment>());
        mStacks.put(AppConstants.TAB_POLLS, new Stack<Fragment>());
        mStacks.put(AppConstants.TAB_SETTINGS, new Stack<Fragment>());

        mTabHost = (TabHost) findViewById(android.R.id.tabhost);
        mTabHost.setOnTabChangedListener(listener);
        mTabHost.setup();

        initializeTabs();

        //Show welcome screen
        boolean skipWelcome = PreferenceHelper.getInstance(getApplicationContext()).getBoolean(AppConstants.SKIP_WELCOME, false);
        if(!skipWelcome){
            Intent intent = new Intent(this, WelcomeActivity.class);
            startActivity(intent);
        }
    }

    public void initializeTabs() {

        LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        /* Setup your tab icons and content views.. Nothing special in this..*/
        TabHost.TabSpec spec = mTabHost.newTabSpec(AppConstants.TAB_CONTACTS);

        spec.setContent(new TabHost.TabContentFactory() {
            public View createTabContent(String tag) {
                return findViewById(R.id.realtabcontent);
            }
        });
        //spec.setIndicator("Settings", getResources().getDrawable(R.drawable.tab_contacts));
        ImageView iv_contacts = new ImageView(this);
        iv_contacts.setLayoutParams(parms);
        iv_contacts.setScaleType(ImageView.ScaleType.FIT_XY);
        iv_contacts.setAdjustViewBounds(true);
        iv_contacts.setImageResource(R.drawable.tab_contacts);
        spec.setIndicator(iv_contacts);
        mTabHost.addTab(spec);

        spec = mTabHost.newTabSpec(AppConstants.TAB_POLLS);
        spec.setContent(new TabHost.TabContentFactory() {
            public View createTabContent(String tag) {
                return findViewById(R.id.realtabcontent);
            }
        });
        //spec.setIndicator("Polls", getResources().getDrawable(R.drawable.tab_polls));
        ImageView iv_polls = new ImageView(this);
        iv_polls.setLayoutParams(parms);
        iv_polls.setScaleType(ImageView.ScaleType.FIT_XY);
        iv_polls.setAdjustViewBounds(true);
        iv_polls.setImageResource(R.drawable.tab_polls);
        spec.setIndicator(iv_polls);
        mTabHost.addTab(spec);

        spec = mTabHost.newTabSpec(AppConstants.TAB_SETTINGS);
        spec.setContent(new TabHost.TabContentFactory() {
            public View createTabContent(String tag) {
                return findViewById(R.id.realtabcontent);
            }
        });
        //spec.setIndicator("Settings", getResources().getDrawable(R.drawable.tab_settings));
        ImageView iv_settings = new ImageView(this);
        iv_settings.setLayoutParams(parms);
        iv_settings.setScaleType(ImageView.ScaleType.FIT_XY);
        iv_settings.setAdjustViewBounds(true);
        iv_settings.setImageResource(R.drawable.tab_settings);
        spec.setIndicator(iv_settings);
        mTabHost.addTab(spec);

        mTabHost.setCurrentTab(1);
    }

    /* Might be useful if we want to switch tab programmatically, from inside any of the fragment.*/
    public void setCurrentTab(int val) {
        mTabHost.setCurrentTab(val);
    }

    /*
         *      To add fragment to a tab.
         *  tag             ->  Tab identifier
         *  fragment        ->  Fragment to show, in tab identified by tag
         *  shouldAnimate   ->  should animate transaction. false when we switch tabs, or adding first fragment to a tab
         *                      true when when we are pushing more fragment into navigation stack.
         *  shouldAdd       ->  Should add to fragment navigation stack (mStacks.get(tag)). false when we are switching tabs (except for the first time)
         *                      true in all other cases.
         */
    public void pushFragments(String tag, Fragment fragment, boolean shouldAnimate, boolean shouldAdd) {
        if (shouldAdd)
            mStacks.get(tag).push(fragment);
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        if (shouldAnimate)
            ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        ft.replace(R.id.realtabcontent, fragment);
        ft.commit();
    }


    public void popFragments() {
      /*
       *    Select the second last fragment in current tab's stack..
       *    which will be shown after the fragment transaction given below
       */
        Fragment fragment = mStacks.get(mCurrentTab).elementAt(mStacks.get(mCurrentTab).size() - 2);

      /*pop current fragment from stack.. */
        mStacks.get(mCurrentTab).pop();

      /* We have the target fragment in hand.. Just show it.. Show a standard navigation animation*/
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        ft.replace(R.id.realtabcontent, fragment);
        ft.commit();
    }


    @Override
    public void onBackPressed() {
        if (mStacks.get(mCurrentTab).size() == 1) {
            // We are already showing first fragment of current tab, so when back pressed, we will finish this activity..
            if(!logout_mode)
                SharedData.ok_to_exit = true;

            finish();
            return;
        }

        /*  Each fragment represent a screen in application (at least in my requirement, just like an activity used to represent a screen). So if I want to do any particular action
         *  when back button is pressed, I can do that inside the fragment itself. For this I used AppBaseFragment, so that each fragment can override onBackPressed() or onActivityResult()
         *  kind of events, and activity can pass it to them. Make sure just do your non navigation (popping) logic in fragment, since popping of fragment is done here itself.
         */
        ((BaseFragment) mStacks.get(mCurrentTab).lastElement()).onBackPressed();

        /* Goto previous fragment in navigation stack of this tab */
        popFragments();
    }


    /*
     *   Imagine if you wanted to get an image selected using ImagePicker intent to the fragment. Ofcourse I could have created a public function
     *  in that fragment, and called it from the activity. But couldn't resist myself.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (mStacks.get(mCurrentTab).size() == 0) {
            Toast.makeText(this, mCurrentTab + " ZERO", Toast.LENGTH_SHORT).show();
            return;
        }

        /*Now current fragment on screen gets onActivityResult callback..*/
        mStacks.get(mCurrentTab).lastElement().onActivityResult(requestCode, resultCode, data);

        try {
            Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
        } catch (Exception e) {
            // bro, exception diri when pics are taken
        }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // lets make sure we clean the cache
        AQUtility.cleanCacheAsync(this);
    }

    public void returnToFirstScreen() {
        while (mStacks.get(mCurrentTab).size() != 1) {
            ((BaseFragment) mStacks.get(mCurrentTab).lastElement()).onBackPressed();
            popFragments();
        }
    }

    public void getFacebookToken() {
        Session session = Session.getActiveSession();
        if (session != null) {
            if (!session.isOpened())
                session.openForRead(new Session.OpenRequest(this).setCallback(callback).setPermissions(AppConstants.fbPermissions));
            else
                Session.openActiveSession(this, true, callback);
        } else
            Session.openActiveSession(this, true, callback);

    }

    private void onSessionStateChange(final Session session, SessionState state, Exception exception) {
        if (session != null && session.isOpened()) {
            // Get the user's data.
            // Make an API call to get user data and define a
            // new callback to handle the response.
            Request request = Request.newMeRequest(session,
                    new Request.GraphUserCallback() {
                        @Override
                        public void onCompleted(GraphUser user, Response response) {
                            // If the response is successful
                            if (session == Session.getActiveSession()) {
                                if (user != null) {
                                    String fbToken = Session.getActiveSession().getAccessToken();
                                    saveFBToken(TabMain.this, fbToken);

                                    //This is a facebook session change, so we navigate back to the add facebook friends list
                                    pushFragments(AppConstants.TAB_CONTACTS, new AddContactFromFacebook(), true, true);

                                }
                            }
                            if (response.getError() != null) {
                                // Handle errors, will do so later.
//                                Log.i(TAG, "Facebook error = "+ response.getError());
                                Toast.makeText(getApplicationContext(), response.getError().toString(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
            request.executeAsync();
        }
    }

    public void saveFBToken(Context context, String token) {
        SharedPreferences shared_data = context.getSharedPreferences(AppConstants.SHARED_DATA, context.MODE_PRIVATE);
        SharedPreferences.Editor editor = shared_data.edit();
        editor.putString(AppConstants.USER_FACEBOOK_ACCESS_TOKEN, token);
        editor.commit();
    }

}


