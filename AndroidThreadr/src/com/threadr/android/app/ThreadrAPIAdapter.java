package com.threadr.android.app;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.util.Log;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.util.EntityUtils;

import java.io.*;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by lilandra on 1/14/14.
 */
public class ThreadrAPIAdapter {

    private static final String TAG = "ThreadrAPIAdapter";

    private static final String BASE_URL = "http://api.askthreadr.com/index.php/api/";

    private static final String HEXES = "0123456789ABCDEF";

    private static final int MAX_IMAGE_WIDTH = 320;

    private static final ObjectMapper mapper = new ObjectMapper();

    private static String getHex(byte[] raw) {
        if (raw == null) {
            return null;
        }
        final StringBuilder hex = new StringBuilder(2 * raw.length);
        for (final byte b : raw) {
            hex.append(HEXES.charAt((b & 0xF0) >> 4))
                    .append(HEXES.charAt((b & 0x0F)));
        }
        return hex.toString();
    }

    private static String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params) {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    private static String doPostRequest(String url, List<NameValuePair> params) {

        String req_response = "";

        HttpClient httpclient = new DefaultHttpClient();
        httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
        httpclient.getParams().setBooleanParameter(CoreConnectionPNames.TCP_NODELAY, true);

        HttpPost httppost = new HttpPost(url);

        if (params != null) {
            Log.v("POST API Request Parameters", params.toString());
        }

        try {
            if (params != null) {
                httppost.setEntity(new UrlEncodedFormEntity(params));
            }

            HttpResponse response = httpclient.execute(httppost);
            req_response = EntityUtils.toString(response.getEntity());
        } catch (ClientProtocolException e) {
        } catch (IOException e) {
        }

        return req_response;
    }

    // login
    public static Map<String, Object> doLogin(String email, String password) {
        Map<String, Object> data = null;

        String url = BASE_URL + "login";

        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            md5.reset();
            md5.update(password.getBytes(Charset.forName("UTF8")));
            byte[] resultByte = md5.digest();
            String hex_password = getHex(resultByte);

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("email", email));
            params.add(new BasicNameValuePair("password", hex_password));

            String json_response = doPostRequest(url, params);
            Log.i(TAG + " RESPONSE", json_response);

            data = mapper.readValue(json_response, Map.class);
        } catch (Exception e) {
            Log.i(TAG + " doLogin", e.toString());
        }

        return data;
    }

    // login
    public static Map<String, Object> doLogin(String accessToken) {
        Map<String, Object> data = null;

        String url = BASE_URL + "login";

        try {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("access_token", accessToken));


            String json_response = doPostRequest(url, params);
            Log.d("JSON RESPONSE", json_response);

            data = mapper.readValue(json_response, Map.class);
        } catch (Exception e) {
            Log.i(TAG + " doLogin", e.toString());
        }

        return data;
    }

    // register
    public static Map<String, Object> doRegister(String email, String first_name, String last_name, String password, String phone_number) {
        Map<String, Object> data = null;

        String url = BASE_URL + "register";

        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            md5.reset();
            md5.update(password.getBytes(Charset.forName("UTF8")));
            byte[] resultByte = md5.digest();
            String hex_password = getHex(resultByte);

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("email", email));
            params.add(new BasicNameValuePair("password", hex_password));
            params.add(new BasicNameValuePair("firstName", first_name));
            params.add(new BasicNameValuePair("lastName", last_name));
            params.add(new BasicNameValuePair("phoneNumber", phone_number));

            String json_response = doPostRequest(url, params);
            Log.i(TAG + " RESPONSE", json_response);

            data = mapper.readValue(json_response, Map.class);
        } catch (Exception e) {
            Log.i(TAG + " doRegister", e.toString());
        }

        return data;
    }

    // edit profile
    public static Map<String, Object> doEditProfile(String access_token, String email, String first_name, String last_name, String phone_number) {
        Map<String, Object> data = null;

        String url = BASE_URL + "editProfile";

        try {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("access_token", access_token));
            params.add(new BasicNameValuePair("email", email));
            params.add(new BasicNameValuePair("firstName", first_name));
            params.add(new BasicNameValuePair("lastName", last_name));
            params.add(new BasicNameValuePair("phoneNumber", phone_number));

            String json_response = doPostRequest(url, params);
            Log.i(TAG + " RESPONSE", json_response);

            data = mapper.readValue(json_response, Map.class);
        } catch (Exception e) {
            Log.i(TAG + " doEditProfile", e.toString());
        }

        return data;
    }

    // create contact group
    public static Map<String, Object> doCreateContactGroup(String access_token, String group_name) {
        Map<String, Object> data = null;

        String url = BASE_URL + "createGroup";

        try {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("access_token", access_token));
            params.add(new BasicNameValuePair("groupName", group_name));

            String json_response = doPostRequest(url, params);
            Log.i(TAG + " RESPONSE", json_response);

            data = mapper.readValue(json_response, Map.class);
        } catch (Exception e) {
            Log.i(TAG + " doCreateContactGroup", e.toString());
        }

        return data;
    }

    // create contact
    public static Map<String, Object> doCreateContact(String access_token, String email, String first_name, String last_name, String phone_number, String group_id) {
        Map<String, Object> data = null;

        String url = BASE_URL + "createContact";

        try {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("access_token", access_token));
            params.add(new BasicNameValuePair("email", email));
            params.add(new BasicNameValuePair("firstName", first_name));
            params.add(new BasicNameValuePair("lastName", last_name));
            params.add(new BasicNameValuePair("phoneNumber", phone_number));
            params.add(new BasicNameValuePair("groupId", group_id));

            String json_response = doPostRequest(url, params);
            Log.i(TAG + " RESPONSE", json_response);

            data = mapper.readValue(json_response, Map.class);
        } catch (Exception e) {
            Log.i(TAG + " doCreateContact", e.toString());
        }

        return data;
    }

    // edit contact
    public static Map<String, Object> doEditContact(String access_token, String email, String first_name, String last_name, String phone_number, String user_contact_id) {
        Map<String, Object> data = null;

        String url = BASE_URL + "editContact";

        try {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("access_token", access_token));
            params.add(new BasicNameValuePair("email", email));
            params.add(new BasicNameValuePair("firstName", first_name));
            params.add(new BasicNameValuePair("lastName", last_name));
            params.add(new BasicNameValuePair("phoneNumber", phone_number));
            params.add(new BasicNameValuePair("userContactId", user_contact_id));

            String json_response = doPostRequest(url, params);
            Log.i(TAG + " RESPONSE", json_response);

            data = mapper.readValue(json_response, Map.class);
        } catch (Exception e) {
            Log.i(TAG + " doCreateContact", e.toString());
        }

        return data;
    }

    // add existing contact to group
    public static Map<String, Object> doAddExistingContactToGroup(String access_token, List<String> user_contact_ids, String group_id) {
        Map<String, Object> data = null;

        String url = BASE_URL + "addExistingContactToGroup";

        try {
            List<NameValuePair> params = new ArrayList<NameValuePair>();

            params.add(new BasicNameValuePair("access_token", access_token));

            for (int i = 0; i < user_contact_ids.size(); i++) {
                String contact_to_add_param = "userContactId[" + Integer.toString(i) + "]";
                params.add(new BasicNameValuePair(contact_to_add_param, user_contact_ids.get(i).toString()));
            }

            params.add(new BasicNameValuePair("groupId", group_id));

            String json_response = doPostRequest(url, params);
            Log.i(TAG + " RESPONSE", json_response);

            data = mapper.readValue(json_response, Map.class);
        } catch (Exception e) {
            Log.i(TAG + " doAddExistingContactToGroup", e.toString());
        }

        return data;
    }

    // create alert
    public static Map<String, Object> doEditAlert(Context context, String access_token, String alert_id, String description, int duration, String time_stamp, List contacts_to_add, List contacts_to_remove, List assets_to_remove, String media_asset, String media_asset_type, String media_temp_file) {
        Map<String, Object> data = null;

        String url = BASE_URL + "editAlert";

        try {

            HttpClient httpclient = new DefaultHttpClient();
            httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            httpclient.getParams().setBooleanParameter(CoreConnectionPNames.TCP_NODELAY, true);

            HttpPost post = new HttpPost(url);

            MultipartEntityBuilder multipartEntity = MultipartEntityBuilder.create();
            multipartEntity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

            multipartEntity.addPart("access_token", new StringBody(access_token));
            multipartEntity.addPart("alertId", new StringBody(alert_id));
            multipartEntity.addPart("description", new StringBody(description));
            multipartEntity.addPart("expirationDate", new StringBody(Integer.toString(duration)));
            multipartEntity.addPart("timestamp", new StringBody(time_stamp));

            for (int i = 0; i < contacts_to_add.size(); i++) {
                String contact_to_add_param = "contactsAdd[" + Integer.toString(i) + "]";
                multipartEntity.addPart(contact_to_add_param, new StringBody(contacts_to_add.get(i).toString()));
            }

            for (int i = 0; i < contacts_to_remove.size(); i++) {
                String contact_to_remove_param = "contactsRemove[" + Integer.toString(i) + "]";
                multipartEntity.addPart(contact_to_remove_param, new StringBody(contacts_to_remove.get(i).toString()));
            }

            for (int i = 0; i < assets_to_remove.size(); i++) {
                String asset_to_remove_param = "assetsRemove[" + Integer.toString(i) + "]";
                multipartEntity.addPart(asset_to_remove_param, new StringBody(assets_to_remove.get(i).toString()));
            }

            File asset = null;

            boolean ok_to_delete = false;

            if (media_asset != null) {
                if (!media_asset.equals("")) {

                    asset = new File(media_asset);

                    multipartEntity.addPart("filename", new StringBody(media_temp_file));
                    multipartEntity.addPart("type", new StringBody(media_asset_type));

                    if (media_asset_type.equals(AppConstants.ASSET_IMAGE)) {

                        Bitmap b = BitmapFactory.decodeFile(media_asset);

                        int original_width = b.getWidth();
                        int original_height = b.getHeight();

                        if (original_width > MAX_IMAGE_WIDTH) {

                            Double dHeight = (MAX_IMAGE_WIDTH * original_height * 1.0) / original_width;

                            int destHeight = Integer.valueOf(dHeight.intValue());

                            Bitmap b2 = Bitmap.createScaledBitmap(b, MAX_IMAGE_WIDTH, destHeight, false);
                            ByteArrayOutputStream outStream = new ByteArrayOutputStream();

                            b2.compress(Bitmap.CompressFormat.PNG, 0, outStream);

                            String temp_image_path = context.getCacheDir() + "/" + media_temp_file;
                            asset = new File(temp_image_path);

                            asset.createNewFile();

                            ok_to_delete = true;

                            FileOutputStream fo = new FileOutputStream(asset);
                            fo.write(outStream.toByteArray());

                            fo.flush();
                            fo.close();

                            outStream.flush();
                            outStream.close();
                        }
                    }

                    Log.i(TAG, asset.getName());

                    multipartEntity.addPart("asset", new FileBody(asset, ContentType.APPLICATION_OCTET_STREAM, asset.getName()));
                } else
                    multipartEntity.addPart("filename", new StringBody(""));
            }

            post.setEntity(multipartEntity.build());

            HttpResponse response = httpclient.execute(post);

            if (ok_to_delete) {
                try {
                    // delete the created file
                    asset.getCanonicalFile().delete();
                } catch (Exception e) {
                }
            }

            String json_response = EntityUtils.toString(response.getEntity());
            Log.i(TAG + " doEditAlert", json_response);


            data = mapper.readValue(json_response, Map.class);
        } catch (Exception e) {
            Log.i(TAG + " doEditAlert", e.toString());
        }

        return data;
    }

    // create alert
    public static Map<String, Object> doCreateAlert(Context context, String access_token, List choices, List contacts, String description, int expiration_time, String media_asset, String media_asset_type, String media_temp_file) {
        Map<String, Object> data = null;

        String url = BASE_URL + "addAlert";

        try {

            HttpClient httpclient = new DefaultHttpClient();
            httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            httpclient.getParams().setBooleanParameter(CoreConnectionPNames.TCP_NODELAY, true);

            HttpPost post = new HttpPost(url);

            MultipartEntityBuilder multipartEntity = MultipartEntityBuilder.create();
            multipartEntity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            multipartEntity.addPart("access_token", new StringBody(access_token));

            for (int i = 0; i < choices.size(); i++) {
                String choice_param = "choices[" + Integer.toString(i) + "]";
                multipartEntity.addPart(choice_param, new StringBody(choices.get(i).toString()));
            }

            for (int i = 0; i < contacts.size(); i++) {
                String contact_param = "contacts[" + Integer.toString(i) + "]";
                multipartEntity.addPart(contact_param, new StringBody(contacts.get(i).toString()));
            }

            multipartEntity.addPart("description", new StringBody(description));
            multipartEntity.addPart("expirationDate", new StringBody(Integer.toString(expiration_time)));

            File asset = null;

            boolean ok_to_delete = false;

            if (!media_asset.equals("")) {

                asset = new File(media_asset);

                multipartEntity.addPart("filename", new StringBody(media_temp_file));
                multipartEntity.addPart("type", new StringBody(media_asset_type));

                if (media_asset_type.equals(AppConstants.ASSET_IMAGE)) {

                    Bitmap b = BitmapFactory.decodeFile(media_asset);

                    int original_width = b.getWidth();
                    int original_height = b.getHeight();

                    if (original_width > MAX_IMAGE_WIDTH) {

                        Double dHeight = (MAX_IMAGE_WIDTH * original_height * 1.0) / original_width;

                        int destHeight = Integer.valueOf(dHeight.intValue());

                        Bitmap b2 = Bitmap.createScaledBitmap(b, MAX_IMAGE_WIDTH, destHeight, false);
                        ByteArrayOutputStream outStream = new ByteArrayOutputStream();

                        b2.compress(Bitmap.CompressFormat.PNG, 0, outStream);

                        String temp_image_path = context.getCacheDir() + "/" + media_temp_file;
                        asset = new File(temp_image_path);

                        asset.createNewFile();

                        ok_to_delete = true;

                        FileOutputStream fo = new FileOutputStream(asset);
                        fo.write(outStream.toByteArray());

                        fo.flush();
                        fo.close();

                        outStream.flush();
                        outStream.close();
                    }
                }


                Log.i(TAG, asset.getName());

                multipartEntity.addPart("asset", new FileBody(asset, ContentType.APPLICATION_OCTET_STREAM, asset.getName()));
            }


            post.setEntity(multipartEntity.build());

            HttpResponse response = httpclient.execute(post);

            if (ok_to_delete) {
                try {
                    // delete the created file
                    asset.getCanonicalFile().delete();
                } catch (Exception e) {
                }
            }

            String json_response = EntityUtils.toString(response.getEntity());
            Log.i(TAG + " doCreateAlert", json_response);


            data = mapper.readValue(json_response, Map.class);
        } catch (Exception e) {
            Log.i(TAG + " doCreateAlert", e.toString());
        }

        return data;
    }

    // create alert
    public static Map<String, Object> doCreateAlertWithGPS(Context context, String access_token, List choices, List contacts, String description, int expiration_time, double latitude, double longitude, String location_name, String media_asset, String media_asset_type, String media_temp_file) {
        Map<String, Object> data = null;

        String url = BASE_URL + "addAlert";

        try {

            HttpClient httpclient = new DefaultHttpClient();
            httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            httpclient.getParams().setBooleanParameter(CoreConnectionPNames.TCP_NODELAY, true);

            HttpPost post = new HttpPost(url);

            MultipartEntityBuilder multipartEntity = MultipartEntityBuilder.create();
            multipartEntity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            multipartEntity.addPart("access_token", new StringBody(access_token));

            for (int i = 0; i < choices.size(); i++) {
                String choice_param = "choices[" + Integer.toString(i) + "]";
                multipartEntity.addPart(choice_param, new StringBody(choices.get(i).toString()));
            }

            for (int i = 0; i < contacts.size(); i++) {
                String contact_param = "contacts[" + Integer.toString(i) + "]";
                multipartEntity.addPart(contact_param, new StringBody(contacts.get(i).toString()));
            }

            multipartEntity.addPart("description", new StringBody(description));
            multipartEntity.addPart("expirationDate", new StringBody(Integer.toString(expiration_time)));
            multipartEntity.addPart("latitude", new StringBody(Double.toString(latitude)));
            multipartEntity.addPart("longitude", new StringBody(Double.toString(longitude)));
            multipartEntity.addPart("locationName", new StringBody(location_name));

            File asset = null;

            boolean ok_to_delete = false;

            if (!media_asset.equals("")) {

                asset = new File(media_asset);

                multipartEntity.addPart("filename", new StringBody(media_temp_file));
                multipartEntity.addPart("type", new StringBody(media_asset_type));

                if (media_asset_type.equals(AppConstants.ASSET_IMAGE)) {

                    Bitmap b = BitmapFactory.decodeFile(media_asset);

                    int original_width = b.getWidth();
                    int original_height = b.getHeight();

                    if (original_width > MAX_IMAGE_WIDTH) {

                        Double dHeight = (MAX_IMAGE_WIDTH * original_height * 1.0) / original_width;

                        int destHeight = Integer.valueOf(dHeight.intValue());

                        Bitmap b2 = Bitmap.createScaledBitmap(b, MAX_IMAGE_WIDTH, destHeight, false);
                        ByteArrayOutputStream outStream = new ByteArrayOutputStream();

                        b2.compress(Bitmap.CompressFormat.PNG, 0, outStream);

                        String temp_image_path = context.getCacheDir() + "/" + media_temp_file;
                        asset = new File(temp_image_path);

                        asset.createNewFile();

                        ok_to_delete = true;

                        FileOutputStream fo = new FileOutputStream(asset);
                        fo.write(outStream.toByteArray());

                        fo.flush();
                        fo.close();

                        outStream.flush();
                        outStream.close();
                    }
                }

                Log.i(TAG, asset.getName());

                multipartEntity.addPart("asset", new FileBody(asset, ContentType.APPLICATION_OCTET_STREAM, asset.getName()));
            }

            post.setEntity(multipartEntity.build());

            HttpResponse response = httpclient.execute(post);

            if (ok_to_delete) {
                try {
                    // delete the created file
                    asset.getCanonicalFile().delete();
                } catch (Exception e) {
                }
            }

            String json_response = EntityUtils.toString(response.getEntity());
            Log.i(TAG + " doCreateAlertWithGPS", json_response);

            data = mapper.readValue(json_response, Map.class);
        } catch (Exception e) {
            Log.i(TAG + " doCreateAlertWithGPS", e.toString());
        }

        return data;
    }


    // add message
    public static Map<String, Object> doAddMessage(String access_token, String alert_id, String message) {
        Map<String, Object> data = null;

        String url = BASE_URL + "addMessage";

        try {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("access_token", access_token));
            params.add(new BasicNameValuePair("alertId", alert_id));
            params.add(new BasicNameValuePair("message", message));

            String json_response = doPostRequest(url, params);
            Log.i(TAG + " RESPONSE", json_response);

            data = mapper.readValue(json_response, Map.class);
        } catch (Exception e) {
            Log.i(TAG + " doAddMessage", e.toString());
        }

        return data;
    }

    // vote
    public static Map<String, Object> doVote(String access_token, String alert_id, String alert_contact_id, String asset_id) {
        Map<String, Object> data = null;

        String url = BASE_URL + "vote";

        try {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("access_token", access_token));
            params.add(new BasicNameValuePair("alertId", alert_id));
            params.add(new BasicNameValuePair("alertContactId", alert_contact_id));
            params.add(new BasicNameValuePair("assetId", asset_id));

            String json_response = doPostRequest(url, params);
            Log.i(TAG + " RESPONSE", json_response);

            data = mapper.readValue(json_response, Map.class);
        } catch (Exception e) {
            Log.i(TAG + " doVote", e.toString());
        }

        return data;
    }

    // add file
    public static Map<String, Object> doAddAsset(Context context, String access_token, String alert_id, String asset_type, String file_name, String file_path, String description) {
        Map<String, Object> data = null;

        String url = BASE_URL + "addAsset";

        try {

            /*
            FileInputStream fin = new FileInputStream(file_path);

            Base64.InputStream b64_input_stream = new Base64.InputStream(fin, Base64.ENCODE);

            file_content = b64_input_stream.toString();

            b64_input_stream.close();
            fin.close();
            */

            Log.i(TAG + " doAddAsset", file_name);

            HttpClient httpclient = new DefaultHttpClient();
            httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            httpclient.getParams().setBooleanParameter(CoreConnectionPNames.TCP_NODELAY, true);

            HttpPost post = new HttpPost(url);

            MultipartEntityBuilder multipartEntity = MultipartEntityBuilder.create();
            multipartEntity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            multipartEntity.addPart("access_token", new StringBody(access_token));
            multipartEntity.addPart("alertId", new StringBody(alert_id));
            multipartEntity.addPart("type", new StringBody(asset_type));
            multipartEntity.addPart("choice", new StringBody(description));

            File asset = new File(file_path);

            boolean ok_to_delete = false;

            if (!asset_type.equals(AppConstants.ASSET_TEXT)) {
                multipartEntity.addPart("filename", new StringBody(file_name));

                if (asset_type.equals(AppConstants.ASSET_IMAGE)) {

                    Bitmap b = BitmapFactory.decodeFile(file_path);

                    int original_width = b.getWidth();
                    int original_height = b.getHeight();

                    if (original_width > MAX_IMAGE_WIDTH) {

                        Double dHeight = (MAX_IMAGE_WIDTH * original_height * 1.0) / original_width;

                        int destHeight = Integer.valueOf(dHeight.intValue());

                        Bitmap b2 = Bitmap.createScaledBitmap(b, MAX_IMAGE_WIDTH, destHeight, false);
                        ByteArrayOutputStream outStream = new ByteArrayOutputStream();

                        b2.compress(Bitmap.CompressFormat.PNG, 0, outStream);

                        String temp_image_path = context.getCacheDir() + "/" + file_name;
                        asset = new File(temp_image_path);

                        asset.createNewFile();

                        ok_to_delete = true;

                        FileOutputStream fo = new FileOutputStream(asset);
                        fo.write(outStream.toByteArray());

                        fo.flush();
                        fo.close();

                        outStream.flush();
                        outStream.close();
                    }
                }

                Log.i(TAG, asset.getName());

                multipartEntity.addPart("asset", new FileBody(asset, ContentType.APPLICATION_OCTET_STREAM, asset.getName()));
            }

            post.setEntity(multipartEntity.build());

            HttpResponse response = httpclient.execute(post);

            if (ok_to_delete) {
                try {
                    // delete the created file
                    asset.getCanonicalFile().delete();
                } catch (Exception e) {
                }
            }

            String json_response = EntityUtils.toString(response.getEntity());
            Log.i(TAG + " RESPONSE", json_response);

            data = mapper.readValue(json_response, Map.class);
        } catch (Exception e) {
            Log.i(TAG + " doAddFile", e.toString());
        }

        return data;
    }

    // add thumbnail
    public static Map<String, Object> doUploadAvatar(String access_token, String image_file_path, String temp_file) {
        Map<String, Object> data = null;

        int AVATAR_WIDTH = 140;
        int AVATAR_HEIGHT = 140;

        String url = BASE_URL + "uploadAvatar";

        try {
            HttpClient httpclient = new DefaultHttpClient();
            httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            httpclient.getParams().setBooleanParameter(CoreConnectionPNames.TCP_NODELAY, true);

            HttpPost post = new HttpPost(url);

            MultipartEntityBuilder multipartEntity = MultipartEntityBuilder.create();
            multipartEntity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            multipartEntity.addPart("access_token", new StringBody(access_token));

            Bitmap bmThumbnail = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(image_file_path), AVATAR_WIDTH, AVATAR_HEIGHT);

            File asset = new File(temp_file);
            asset.createNewFile();

            //Convert bitmap to byte array
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bmThumbnail.compress(Bitmap.CompressFormat.PNG, 0, bos);
            byte[] bitmapdata = bos.toByteArray();

            //write the bytes in file
            FileOutputStream fos = new FileOutputStream(asset);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();

            bos.flush();
            bos.close();

            multipartEntity.addPart("asset", new FileBody(asset, ContentType.APPLICATION_OCTET_STREAM, asset.getName()));
            post.setEntity(multipartEntity.build());
            HttpResponse response = httpclient.execute(post);

            // delete the created file
            asset.getCanonicalFile().delete();

            String json_response = EntityUtils.toString(response.getEntity());
            Log.i(TAG + " RESPONSE", json_response);

            data = mapper.readValue(json_response, Map.class);
        } catch (Exception e) {
            Log.i(TAG + " doUploadAvatar", e.toString());
        }

        return data;
    }


    // delete contact
    public static Map<String, Object> doDeleteContact(String access_token, String user_contact_id, boolean to_group_only) {

        Log.i(TAG, user_contact_id);
        //Log.i(TAG, Boolean.toString(to_group_only));

        Map<String, Object> data = null;

        String url = BASE_URL + "deleteContact";

        try {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("access_token", URLEncoder.encode(access_token, "UTF-8")));
            params.add(new BasicNameValuePair("userContactId", user_contact_id));

            if (to_group_only)
                params.add(new BasicNameValuePair("toGroup", Boolean.toString(to_group_only)));

            String json_response = doPostRequest(url, params);
            Log.i(TAG + " RESPONSE", json_response);

            data = mapper.readValue(json_response, Map.class);
        } catch (Exception e) {
            Log.i(TAG + " doDeleteContact", e.toString());
        }

        return data;
    }

    // delete contact
    public static Map<String, Object> doDeleteContact(String access_token, String user_contact_id) {

        Log.i(TAG, user_contact_id);

        Map<String, Object> data = null;

        String url = BASE_URL + "deleteContact";

        try {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("access_token", URLEncoder.encode(access_token, "UTF-8")));
            params.add(new BasicNameValuePair("userContactId", user_contact_id));

            String json_response = doPostRequest(url, params);
            Log.i(TAG + " RESPONSE", json_response);

            data = mapper.readValue(json_response, Map.class);
        } catch (Exception e) {
            Log.i(TAG + " doDeleteContact", e.toString());
        }

        return data;
    }

    // delete contact group
    public static Map<String, Object> doDeleteContactGroup(String access_token, String group_id) {

        //Log.i(TAG, Boolean.toString(to_group_only));

        Map<String, Object> data = null;

        String url = BASE_URL + "deleteGroup";

        try {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("access_token", URLEncoder.encode(access_token, "UTF-8")));
            params.add(new BasicNameValuePair("groupId", group_id));

            String json_response = doPostRequest(url, params);
            Log.i(TAG + " RESPONSE", json_response);

            data = mapper.readValue(json_response, Map.class);
        } catch (Exception e) {
            Log.i(TAG + " doDeleteContactGroup", e.toString());
        }

        return data;
    }

    // toogle email notification
    public static Map<String, Object> doToggleEmailNotification(String access_token) throws UnsupportedEncodingException {

        Map<String, Object> data = null;

        HttpClient httpclient;
        HttpGet httpget;
        HttpResponse response;

        String url = BASE_URL + "toggleEmailNotification/";

        String target_url = url + URLEncoder.encode(access_token, "UTF-8");

        try {
            httpclient = new DefaultHttpClient();
            httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            httpclient.getParams().setBooleanParameter(CoreConnectionPNames.TCP_NODELAY, true);

            httpget = new HttpGet(target_url);
            response = httpclient.execute(httpget);
            HttpEntity entity = response.getEntity();

            String json_response = EntityUtils.toString(entity);
            Log.i(TAG + " RESPONSE", json_response);

            data = mapper.readValue(json_response, Map.class);
        } catch (Exception e) {
            Log.i(TAG + " doToggleEmailNotification", e.toString());
        }

        return data;
    }

    // get message list
    public static Map<String, Object> doGetMessageList(String access_token, String alert_id) throws UnsupportedEncodingException {

        Map<String, Object> data = null;

        HttpClient httpclient;
        HttpGet httpget;
        HttpResponse response;

        String url = BASE_URL + "getMessageList?";

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("access_token", URLEncoder.encode(access_token, "UTF-8")));
        params.add(new BasicNameValuePair("alertId", alert_id));

        String target_url = url + ThreadrAPIAdapter.getQuery(params);

        try {
            httpclient = new DefaultHttpClient();
            httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            httpclient.getParams().setBooleanParameter(CoreConnectionPNames.TCP_NODELAY, true);

            httpget = new HttpGet(target_url);
            response = httpclient.execute(httpget);
            HttpEntity entity = response.getEntity();

            String json_response = EntityUtils.toString(entity);
            Log.i(TAG + " RESPONSE", json_response);

            data = mapper.readValue(json_response, Map.class);
        } catch (Exception e) {
            Log.i(TAG + " doToggleEmailNotification", e.toString());
        }

        return data;
    }

    // get alert list
    public static Map<String, Object> doGetActiveAlertList(String access_token) throws UnsupportedEncodingException {

        Map<String, Object> data = null;

        HttpClient httpclient;
        HttpGet httpget;
        HttpResponse response;

        String url = BASE_URL + "alertList/";

        String target_url = url + URLEncoder.encode(access_token, "UTF-8");

        try {
            httpclient = new DefaultHttpClient();
            httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            httpclient.getParams().setBooleanParameter(CoreConnectionPNames.TCP_NODELAY, true);

            httpget = new HttpGet(target_url);
            response = httpclient.execute(httpget);
            HttpEntity entity = response.getEntity();

            String json_response = EntityUtils.toString(entity);
            Log.i(TAG + " RESPONSE", json_response);

            data = mapper.readValue(json_response, Map.class);

        } catch (Exception e) {
            Log.i(TAG + " doGetActiveAlertListq", e.toString());
        }

        return data;
    }

    // get alert list
    public static Map<String, Object> doGetFinishedAlertList(String access_token) throws UnsupportedEncodingException {

        Map<String, Object> data = null;

        HttpClient httpclient;
        HttpGet httpget;
        HttpResponse response;

        String url = BASE_URL + "alertList/";

        String target_url = url + URLEncoder.encode(access_token, "UTF-8") + "/1";

        try {
            httpclient = new DefaultHttpClient();
            httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            httpclient.getParams().setBooleanParameter(CoreConnectionPNames.TCP_NODELAY, true);

            httpget = new HttpGet(target_url);
            response = httpclient.execute(httpget);
            HttpEntity entity = response.getEntity();

            String json_response = EntityUtils.toString(entity);
            Log.i(TAG + " RESPONSE", json_response);

            data = mapper.readValue(json_response, Map.class);

        } catch (Exception e) {
            Log.i(TAG + " doGetFinishedAlertList", e.toString());
        }

        return data;
    }


    // get alert info
    public static Map<String, Object> doGetAlertInfo(String access_token, String alert_id) throws UnsupportedEncodingException {

        Map<String, Object> data = null;

        HttpClient httpclient;
        HttpGet httpget;
        HttpResponse response;

        String url = BASE_URL + "getAlertInfo/";

        String target_url = url + URLEncoder.encode(access_token, "UTF-8") + "/" + alert_id;

        try {
            httpclient = new DefaultHttpClient();
            httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            httpclient.getParams().setBooleanParameter(CoreConnectionPNames.TCP_NODELAY, true);

            httpget = new HttpGet(target_url);
            response = httpclient.execute(httpget);
            HttpEntity entity = response.getEntity();

            String json_response = EntityUtils.toString(entity);
            Log.i(TAG + " RESPONSE", json_response);

            data = mapper.readValue(json_response, Map.class);

        } catch (Exception e) {
            Log.i(TAG + " doGetAlertInfo", e.toString());
        }

        return data;
    }

    // delete alert
    public static Map<String, Object> doDeleteAlert(String access_token, String alert_id) throws UnsupportedEncodingException {

        Map<String, Object> data = null;

        HttpClient httpclient;
        HttpGet httpget;
        HttpResponse response;

        String url = BASE_URL + "deleteAlert?";

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("access_token", URLEncoder.encode(access_token, "UTF-8")));
        params.add(new BasicNameValuePair("alertId", alert_id));

        String target_url = url + ThreadrAPIAdapter.getQuery(params);

        try {
            httpclient = new DefaultHttpClient();
            httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            httpclient.getParams().setBooleanParameter(CoreConnectionPNames.TCP_NODELAY, true);

            httpget = new HttpGet(target_url);
            response = httpclient.execute(httpget);
            HttpEntity entity = response.getEntity();

            String json_response = EntityUtils.toString(entity);
            Log.i(TAG + " RESPONSE", json_response);

            data = mapper.readValue(json_response, Map.class);
        } catch (Exception e) {
            Log.i(TAG + " doDeleteAlert", e.toString());
        }

        return data;
    }

    // get user groups
    public static Map<String, Object> doGetUserGroups(String access_token) throws UnsupportedEncodingException {

        Map<String, Object> data = null;

        HttpClient httpclient;
        HttpGet httpget;
        HttpResponse response;

        String url = BASE_URL + "getGroupsOfUser?";

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("access_token", URLEncoder.encode(access_token, "UTF-8")));

        String target_url = url + ThreadrAPIAdapter.getQuery(params);

        try {
            httpclient = new DefaultHttpClient();
            httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            httpclient.getParams().setBooleanParameter(CoreConnectionPNames.TCP_NODELAY, true);

            httpget = new HttpGet(target_url);
            response = httpclient.execute(httpget);
            HttpEntity entity = response.getEntity();

            String json_response = EntityUtils.toString(entity);
            Log.i(TAG + " doGetUserGroups RESPONSE", json_response);

            data = mapper.readValue(json_response, Map.class);
        } catch (Exception e) {
            Log.i(TAG + " doGetUserGroups", e.toString());
        }

        return data;
    }

    // get all group contacts
    public static Map<String, Object> doGetAllGroupContacts(String access_token, String group_id) throws UnsupportedEncodingException {

        Map<String, Object> data = null;

        HttpClient httpclient;
        HttpGet httpget;
        HttpResponse response;

        String url = BASE_URL + "getAllContacts?";

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("access_token", URLEncoder.encode(access_token, "UTF-8")));
        params.add(new BasicNameValuePair("groupId", group_id));

        String target_url = url + ThreadrAPIAdapter.getQuery(params);

        try {
            httpclient = new DefaultHttpClient();
            httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            httpclient.getParams().setBooleanParameter(CoreConnectionPNames.TCP_NODELAY, true);

            httpget = new HttpGet(target_url);
            response = httpclient.execute(httpget);
            HttpEntity entity = response.getEntity();

            String json_response = EntityUtils.toString(entity);
            Log.i(TAG + " doGetAllGroupContacts RESPONSE", json_response);

            data = mapper.readValue(json_response, Map.class);
        } catch (Exception e) {
            Log.i(TAG + " doGetAllGroupContacts", e.toString());
        }

        return data;
    }

    // get contact info
    public static Map<String, Object> doGetContactInfo(String access_token, String contact_id) throws UnsupportedEncodingException {

        Map<String, Object> data = null;

        HttpClient httpclient;
        HttpGet httpget;
        HttpResponse response;

        String url = BASE_URL + "getContact?";

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("access_token", URLEncoder.encode(access_token, "UTF-8")));
        params.add(new BasicNameValuePair("userContactId", contact_id));

        String target_url = url + ThreadrAPIAdapter.getQuery(params);

        try {
            httpclient = new DefaultHttpClient();
            httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            httpclient.getParams().setBooleanParameter(CoreConnectionPNames.TCP_NODELAY, true);

            httpget = new HttpGet(target_url);
            response = httpclient.execute(httpget);
            HttpEntity entity = response.getEntity();

            String json_response = EntityUtils.toString(entity);
            Log.i(TAG + " RESPONSE", json_response);

            data = mapper.readValue(json_response, Map.class);
        } catch (Exception e) {
            Log.i(TAG + " doGetContactInfo", e.toString());
        }

        return data;
    }

    // get contact info
    public static Map<String, Object> doGetFacebookContacts(String access_token) throws UnsupportedEncodingException {

        Map<String, Object> data = null;

        HttpClient httpclient;
        HttpGet httpget;
        HttpResponse response;

        String url = BASE_URL + "getFbFriends/";

        String target_url = url + URLEncoder.encode(access_token, "UTF-8");


        try {
            httpclient = new DefaultHttpClient();
            httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            httpclient.getParams().setBooleanParameter(CoreConnectionPNames.TCP_NODELAY, true);

            httpget = new HttpGet(target_url);
            response = httpclient.execute(httpget);
            HttpEntity entity = response.getEntity();

            String json_response = EntityUtils.toString(entity);
            Log.i(TAG + " RESPONSE", json_response);

            data = mapper.readValue(json_response, Map.class);
        } catch (Exception e) {
            Log.i(TAG + " doGetFacebookContacts", e.toString());
        }

        return data;
    }

    // forgot password
    public static Map<String, Object> doForgotPassword(String email) {

        Map<String, Object> data = null;

        HttpClient httpclient;
        HttpGet httpget;
        HttpResponse response;

        String url = BASE_URL + "forgotPassword/";

        String target_url = url + email;

        try {
            httpclient = new DefaultHttpClient();
            httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            httpclient.getParams().setBooleanParameter(CoreConnectionPNames.TCP_NODELAY, true);

            httpget = new HttpGet(target_url);
            response = httpclient.execute(httpget);
            HttpEntity entity = response.getEntity();

            String json_response = EntityUtils.toString(entity);
            Log.i(TAG + " RESPONSE", json_response);

            data = mapper.readValue(json_response, Map.class);

        } catch (Exception e) {
            Log.i(TAG + " doForgotPassword", e.toString());
        }

        return data;
    }

    // forgot password
    public static Map<String, Object> doEditPassword(String access_token, String old_password, String new_password) throws UnsupportedEncodingException {

        Map<String, Object> data = null;

        HttpClient httpclient;
        HttpGet httpget;
        HttpResponse response;


        String url = BASE_URL + "changePassword/";

        String target_url = url + URLEncoder.encode(access_token, "UTF-8");

        try {

            MessageDigest md5 = MessageDigest.getInstance("MD5");
            md5.reset();
            md5.update(old_password.getBytes(Charset.forName("UTF8")));
            byte[] resultByte1 = md5.digest();
            String hex_old_password = getHex(resultByte1);

            md5.reset();
            md5.update(new_password.getBytes(Charset.forName("UTF8")));
            byte[] resultByte2 = md5.digest();
            String hex_new_password = getHex(resultByte2);

            target_url = target_url + "/" + URLEncoder.encode(hex_new_password, "UTF-8") + "/" + URLEncoder.encode(hex_old_password, "UTF-8");

            httpclient = new DefaultHttpClient();
            httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            httpclient.getParams().setBooleanParameter(CoreConnectionPNames.TCP_NODELAY, true);

            httpget = new HttpGet(target_url);
            response = httpclient.execute(httpget);
            HttpEntity entity = response.getEntity();

            String json_response = EntityUtils.toString(entity);
            Log.i(TAG + " RESPONSE", json_response);

            data = mapper.readValue(json_response, Map.class);

        } catch (Exception e) {
            Log.i(TAG + " doEditPassword", e.toString());
        }

        return data;
    }

    // delete contact group
    public static Map<String, Object> doAddDeviceToken(String access_token, String device_token) {

        Map<String, Object> data = null;

        String url = BASE_URL + "addDeviceToken";

        try {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("access_token", URLEncoder.encode(access_token, "UTF-8")));
            params.add(new BasicNameValuePair("deviceToken", URLEncoder.encode(device_token, "UTF-8")));
            params.add(new BasicNameValuePair("isAndroid", "1"));

            String json_response = doPostRequest(url, params);
            Log.i(TAG + "addDeviceToken" + " RESPONSE", json_response);

            data = mapper.readValue(json_response, Map.class);
        } catch (Exception e) {
            Log.i(TAG + " doAddDeviceToken", e.toString());
        }

        return data;
    }

    public static Map<String, Object> getLocation(double latitude, double longitude) throws UnknownHostException, UnsupportedEncodingException {

        Map<String, Object> data = null;

        HttpClient httpclient;
        HttpGet httpget;
        HttpResponse response;

        String base_url = "http://maps.googleapis.com/maps/api/geocode/json?";

        String target_url = base_url + "latlng=" + URLEncoder.encode(Double.toString(latitude), "utf-8") + "," + URLEncoder.encode(Double.toString(longitude), "utf-8") + "&sensor=false";
        Log.i(TAG + " maps fetch url ", target_url);

        try {
            httpclient = new DefaultHttpClient();
            httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            httpclient.getParams().setBooleanParameter(CoreConnectionPNames.TCP_NODELAY, true);

            httpget = new HttpGet(target_url);
            response = httpclient.execute(httpget);
            HttpEntity entity = response.getEntity();

            String json_response = EntityUtils.toString(entity);
            Log.i(TAG + " getLocation RESPONSE", json_response);

            data = mapper.readValue(json_response, Map.class);

        } catch (Exception e) {
            Log.i(TAG + " getLocation", e.toString());
        }

        return data;
    }
}
