package com.threadr.android.app;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

/**
 * Created by lilandra on 2/25/14.
 */
public class ThreadrBroadcastReceiver extends WakefulBroadcastReceiver {

    private static String TAG = "PollsFragment";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "message received");
        // Explicitly specify that GcmIntentService will handle the intent.
        ComponentName comp = new ComponentName(context.getPackageName(),
                ThreadrIntentService.class.getName());
        // Start the service, keeping the device awake while it is launching.
        startWakefulService(context, (intent.setComponent(comp)));
        setResultCode(Activity.RESULT_OK);
    }
}