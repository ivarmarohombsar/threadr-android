package com.threadr.android.app.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import com.threadr.android.app.AppConstants;

/**
 * Created by arthurlim on 1/29/14.
 */
public class PreferenceHelper {
    private static PreferenceHelper ourInstance;

    public static PreferenceHelper getInstance(Context context) {
        if (ourInstance == null)
            ourInstance = new PreferenceHelper(context);
        return ourInstance;
    }

    private Context context;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    private PreferenceHelper(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences(AppConstants.SHARED_DATA, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public String getString(String key, String default_value) {
        return sharedPreferences.getString(key, default_value);
    }

    public void setString(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }

    public boolean getBoolean(String key, boolean default_value) {
        return sharedPreferences.getBoolean(key, default_value);
    }

    public void setBoolean(String key, boolean value) {
        editor.putBoolean(key, value);
        editor.commit();
    }

    public void clear() {
        editor.clear();
        editor.commit();
    }
}
