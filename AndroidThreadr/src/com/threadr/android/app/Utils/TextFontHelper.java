package com.threadr.android.app.Utils;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

/**
 * Created by arthurlim on 2/13/14.
 */
public class TextFontHelper {
    /**
     *
     * @param textView - TextView to change the font
     * @param text - (optional) text for the TextView
     */
    public static void setFontAndText(Context context, TextView textView, String text){
        if(textView!= null){
            Typeface helveticaFont = Typeface.createFromAsset(context.getAssets(),"HelveticaNeueLTPro-Th.otf");
            textView.setTypeface(helveticaFont);
        }

        if(text != null){
            textView.setText(text);
        }
    }
}
