package com.threadr.android.app.Welcome;

import java.util.Locale;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.media.Image;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.threadr.android.app.AppConstants;
import com.threadr.android.app.R;
import com.threadr.android.app.Utils.PreferenceHelper;

public class WelcomeActivity extends Activity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v13.app.FragmentStatePagerAdapter}.
     */
    SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);



        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

    }
    

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            return 4;
        }

//        @Override
//        public CharSequence getPageTitle(int position) {
//            Locale l = Locale.getDefault();
//            switch (position) {
//                case 0:
//                    return getString(R.string.title_section1).toUpperCase(l);
//                case 1:
//                    return getString(R.string.title_section2).toUpperCase(l);
//                case 2:
//                    return getString(R.string.title_section3).toUpperCase(l);
//            }
//            return null;
//        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);

            ImageView welcomeImageView = (ImageView)rootView.findViewById(R.id.welcomeImageView);
            TextView messageTextView = (TextView) rootView.findViewById(R.id.messageTextView);
            ImageView selectionImageView = (ImageView)rootView.findViewById(R.id.selectionImageView);
            Button skipButton = (Button)rootView.findViewById(R.id.skipButton);

            int selection = getArguments().getInt(ARG_SECTION_NUMBER);
            if(selection == 1){
                String message = "<b>Threadr</b> is the best way to get instant feedback from friends... <b>ASAP!</b>";
                welcomeImageView.setImageResource(R.drawable.welcome1);
                messageTextView.setText(Html.fromHtml(message));
                selectionImageView.setImageResource(R.drawable.selection_1);
            }
            if(selection == 2){
                String message = "First select who you want to ask... if they are not on <b>Threadr</b> you can share by email";
                welcomeImageView.setImageResource(R.drawable.welcome2);
                messageTextView.setText(Html.fromHtml(message));
                selectionImageView.setImageResource(R.drawable.selection_2);
            }
            if(selection == 3){
                String message = "Add media to illustrate what outfits, art, music, or anything you would like to feedback on";
                welcomeImageView.setImageResource(R.drawable.welcome3);
                messageTextView.setText(Html.fromHtml(message));
                selectionImageView.setImageResource(R.drawable.selection_3);
            }
            if(selection == 4){
                String message = "Set a timer for how long your friends have to reply (could be minutes or days!) and send your <b>Threadr Poll</b>. <br><br>Your friends on <b>Threadr</b> would receive instant notifications, and all others will receive emails with a link to where they can vote and comment. Enjoy!";
                welcomeImageView.setImageResource(R.drawable.welcome4);
                messageTextView.setText(Html.fromHtml(message));
                selectionImageView.setImageResource(R.drawable.selection_4);
                skipButton.setText(getString(R.string.start_using_threadr));
            }

            skipButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    PreferenceHelper.getInstance(view.getContext()).setBoolean(AppConstants.SKIP_WELCOME, true);
                    getActivity().finish();
                }
            });

            return rootView;
        }
    }

}
