package com.threadr.android.app;

/**
 * Created by lilandra on 1/19/14.
 */
public class WhoToAskContactGroupInfo {
    public String name = "";
    public String id = "";
    public boolean is_selected = false;

    public WhoToAskContactGroupInfo(String group_name_param, String group_id_param, boolean is_selected_param) {
        name = group_name_param;
        id = group_id_param;
        is_selected = is_selected_param;
    }
}
