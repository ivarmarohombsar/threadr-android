package com.threadr.android.app;

/**
 * Created by lilandra on 1/23/14.
 */
public class WhoToAskContactInfo {
    public String first_name = "";
    public String last_name = "";
    public String id = "";
    public boolean is_selected = false;

    public WhoToAskContactInfo(String first_name_param, String last_name_param, String contact_id_param, boolean is_selected_param) {
        first_name = first_name_param;
        last_name = last_name_param;
        id = contact_id_param;
        is_selected = is_selected_param;
    }
}
